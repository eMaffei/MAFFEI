package Skate.Test;

import Skate.Panels.LoginPane;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static Scene scene;

    @Override
    public void start(Stage primaryStage) throws Exception{
        LoginPane LoginPane = new LoginPane();
        Main.scene = new Scene(LoginPane,500,500);

        // Parent root = FXMLLoader.load(getClass().getResource("sample.fxml")); */

        primaryStage.setTitle("JavaFX");
        primaryStage.setScene(scene);
        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}

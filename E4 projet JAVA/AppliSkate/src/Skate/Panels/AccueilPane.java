package Skate.Panels;

import Skate.Test.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class AccueilPane extends GridPane {

    private String ptext;
    //private String mtext;

    public AccueilPane(String pText/* String mtext*/) {
        super();
        this.ptext = pText;
       // this.mtext = mtext;
        this.InitPane();
    }

    private void InitPane() {
        this.setVgap(10);
        this.setVgap(10);

        //creation button
        Button playButton = new Button("deconnexion");

        //button colonne
        this.add(playButton,2,3);

        Text label = new Text("Vous etes connecté en tant que "+this.ptext/*" et le mdp est "+this.mtext*/);

        this.add(label,4,4);

        playButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.scene.setRoot(new LoginPane());
            }
        });
    }
}

package Skate.Panels;


import Skate.Test.Main;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class LoginPane extends GridPane {
    public LoginPane() {
        //recuperer Initpane
        super();
        this.InitPane();
    }

    private void InitPane() {

        PasswordField mdp = new PasswordField();


        this.setVgap(10);
        this.setVgap(10);

        Text label = new Text("Login");
        Text label1 = new Text("Utilisateur");
        Text label2 = new Text("mot de passe");
        Text labelmdp =  new Text("?");

        TextField p = new TextField();
        // TextField m = new TextField();

        //creation button
        Button playButton = new Button("envoyer");
        Button showMDP = new Button("montrer le mdp");
        Button clear = new Button("effacer");
        Button create = new Button("creer un compte");


        //colonne
        this.add(label,2,1);
        this.add(label1,1,2);
        this.add(label2,1,3);
        this.add(labelmdp,3,3);

        //textfield
        this.add(p,2,2);
        //this.add(m,2,3);
        this.add(mdp,2,3);


        //button
        GridPane.setHalignment(playButton, HPos.RIGHT);
        this.add(playButton,2,4);
        this.add(showMDP,2,5);
        this.add(clear,2,4);
        this.add(create,2,6);


        //action button
        playButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //vers l'autre scène
                Main.scene.setRoot(new AccueilPane(p.getText() /*m.getText()*/));
            }
        });

        showMDP.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String mdp1 = mdp.getText();
                labelmdp.setText(mdp1);
            }
        });

        clear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //clear les textfields
                p.clear();
                mdp.clear();
            }
        });

        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.scene.setRoot(new AccountPane());
            }
        });
    }

}
package Skate.DAO;

public class SkateException extends Exception{
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    private String className ;

    public SkateException(String message) {
        super(message);
    }

    public  SkateException(String message, Throwable cause) {
        super(message, cause);
    }

    public  SkateException(Throwable cause) {
        super(cause);
    }

    public  SkateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

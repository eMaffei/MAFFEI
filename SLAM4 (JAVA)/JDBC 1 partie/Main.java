import java.sql.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

            System.out.println("Driver O.K.");

            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/m2lfootball?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC&User=root&password=");
            PreparedStatement st = conn.prepareStatement("SELECT * FROM coach INNER JOIN licence ON coach.no_licence = licence.no_licence");
            PreparedStatement ct = conn.prepareStatement("SELECT * FROM licence");
            PreparedStatement mb = conn.prepareStatement("SELECT * FROM equipe INNER JOIN licence ON equipe.id_equipe = licence.no_licence");
            PreparedStatement gt = conn.prepareStatement("SELECT * FROM joueur");

            System.out.println("Connexion effective !");

            ResultSet result = st.executeQuery();
            ResultSet result1 = ct.executeQuery();
            ResultSet result2 = mb.executeQuery();
            ResultSet result3 = gt.executeQuery();

            ArrayList<Coach> tab = new ArrayList<Coach>();
            while (result.next()) {
                Coach c = new Coach(
                        result.getInt("no_licence"),
                        result.getString("datelic"),
                        result.getString("prenom"),
                        result.getString("nom"),
                        result.getString("adresse"),
                        result.getString("sexe"),
                        result.getString("datenaiss")
                        );
                        ; //appel du constructeur par defaut
                tab.add(c);
                for (int i = 0; i < tab.size(); i++) {
                    System.out.println(c);
                    //c n'affiche avec toute les info si la mathode toString
                }

            }

            ArrayList<Licencie> tabL = new ArrayList<Licencie>();
            while (result1.next()) {
                Licencie s = new Licencie();
                //appel du constructeur par defaut
                s.setNom(result1.getString("nom"));
                s.setPrenom(result1.getString("prenom"));
                tabL.add(s);
                for (int i = 0; i < tabL.size(); i++) {
                    System.out.println(s);
                }

            }
            ArrayList<Equipe> tabE = new ArrayList<Equipe>();
            while (result2.next()) {
                Equipe e = new Equipe(); //appel du constructeur par defaut
                e.setNom(result2.getString("nom"));
                tabE.add(e);
                for (int i = 0; i < tab.size(); i++) {
                    System.out.println(e);
                    //c n'affiche avec toute les info si la mathode toString
                }

            }
           ArrayList<Joueur> tabJ = new ArrayList<Joueur>();
            while (result3.next()) {
                Joueur j = new Joueur(); //appel du constructeur par defaut
                j.setNom(result3.getString("no_licence"));
                tabJ.add(j);
                for (int i = 0; i < tabJ.size(); i++) {
                    System.out.println(j);
                    //c n'affiche avec toute les info si la mathode toString
                }

            }
        }
        catch(Exception e)
                {

                    e.printStackTrace();

                }
            }
        }

public class Coach extends Licencie {
    public Coach(int noLicencie, String datelic, String prenom, String nom, String adresse, String sexe, String datenaiss) {
        super(noLicencie, datelic, prenom, nom, adresse, sexe, datenaiss);
    }

    public Coach() {
        super();
    }

    @Override
    public String toString() {
        return "Coach {" +super.toString()+ '}';
    }
}

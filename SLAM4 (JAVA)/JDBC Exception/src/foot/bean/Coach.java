package foot.bean;

import java.sql.Date;

public class Coach extends Licencie {
    public Coach(int noLicencie, Date datelic, String prenom, String nom, String adresse, String sexe, Date datenaiss) {
        super(noLicencie, datelic, prenom, nom, adresse, sexe, datenaiss);
    }

    public Coach() {
        super();
    }

    @Override
    public String toString() {
        return "foot.bean.Coach {" +super.toString()+ '}';
    }
}

package foot.bean;

import java.sql.Date;

public class Joueur extends Licencie{
    private int noLicencie;
    private Date datelic;
    private String prenom;
    private String nom;
    private String adresse;
    private String sexe;
    private String datenaiss;
    private String poste;

    public Joueur(int noLicencie, Date datelic, String prenom, String nom, String adresse, String sexe, Date datenaiss, String poste) {
        super(noLicencie, datelic, prenom, nom, adresse, sexe, datenaiss);
        this.setPoste(poste);
    }

    public Joueur() {}// permets de creer un joueur vide et cela permet de creer un (set)

    public Joueur(int no_licence, Date datelic, String prenom, String nom, String adresse, String sexe, Date datenaiss){ }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {

        this.poste = poste;
    }

    @Override
    public String toString() {
        return "Joueur{" + super.toString() + ", " +
                "poste='" + poste + '\'' +
                '}';
    }
}

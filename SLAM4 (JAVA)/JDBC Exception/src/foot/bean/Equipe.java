package foot.bean;

public class Equipe {
    private String nom;
    private String division;
    private String categorie;

    public Equipe(String nom, String division, String categorie) {
        this.setNom(nom);
        this.setDivision(division);
        this.setCategorie(categorie);
    }

    public Equipe(int no_licence, String datelic, String prenom, String nom, String adresse, String sexe, String datenaiss) {
        super();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "foot.bean.Equipe{" +
                "nom='" + nom + '\'' +
                ", division='" + division + '\'' +
                ", categorie='" + categorie + '\'' +
                '}';
    }

}

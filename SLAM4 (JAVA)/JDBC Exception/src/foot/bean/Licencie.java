package foot.bean;

import java.sql.Date;

public class Licencie {
    private int NoLicencie;
    private Date datelic;
    private String prenom;
    private String nom;
    private String adresse;
    private String sexe;
    private Date datenaiss;

    @Override
    public String toString() { // to string cela m'as permis d'afficher les données du arraylist
        return "Licencie{" +
                "NoLicencie=" + NoLicencie +
                ", datelic='" + datelic + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", sexe='" + sexe + '\'' +
                ", datenaiss='" + datenaiss + '\'' +
                '}';
    }
    public Licencie(int noLicencie, Date datelic, String prenom, String nom, String adresse, String sexe, Date datenaiss) {
        setNoLicencie(noLicencie);
        this.setDatelic(datelic);
        this.setPrenom(prenom);
        this.setNom(nom);
        this.setAdresse(adresse);
        this.setSexe(sexe);
        this.setDatenaiss(datenaiss);
    }

    public Licencie() {

    }

    public int getNoLicencie() {
        return NoLicencie;
    }

    public void setNoLicencie(int noLicencie) {
        NoLicencie = noLicencie;
    }

    public Date getDatelic() {
        return datelic;
    }

    public void setDatelic(Date datelic) {
        this.datelic = datelic;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Date getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(Date datenaiss) {
        this.datenaiss = datenaiss;
    }
}

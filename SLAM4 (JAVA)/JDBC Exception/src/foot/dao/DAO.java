package foot.dao;
import java.sql.*;
import java.sql.Connection;

public abstract class DAO {
    public Connection Getconnection() throws FootException {

        Connection conn = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/m2lfootball?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC&User=root&password=");
        }

        catch (SQLException ex){
            FootException fe = new FootException("erreur Connexion", ex);
            fe.setClassName("DAO");
            throw fe;
        }

        catch (ClassNotFoundException Exc){
            FootException f2 = new FootException("erreur driver", Exc);
            f2.setClassName("DAO");
            throw f2;
        }


        return conn;
    }

}
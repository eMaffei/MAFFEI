package foot.dao;
import foot.bean.Coach;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CoachDAO extends DAO {
    public ArrayList<Coach> RecupererToutLesCoach() throws  ClassNotFoundException, SQLException {
        ArrayList<Coach> tab = new ArrayList<>();
        Connection conn = this.Getconnection();

        try {
            PreparedStatement st = conn.prepareStatement("SELECT * FROM coach INNER JOIN licence ON coach.no_licence = licence.no_licence");

            ResultSet result = st.executeQuery();

            while (result.next()) {
                Coach c = new Coach(
                        result.getInt("no_licence"),
                        result.getDate("datelic"),
                        result.getString("prenom"),
                        result.getString("nom"),
                        result.getString("adresse"),
                        result.getString("sexe"),
                        result.getDate("datenaiss")
                );
                ; //appel du constructeur par defaut
                tab.add(c);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return tab;
    }
}

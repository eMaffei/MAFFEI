package foot.dao;
import foot.bean.Joueur;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class JoueurDAO extends DAO{

    public ArrayList<Joueur> Recuperertoutlesjoueurs() throws SQLException, ClassNotFoundException {

        ArrayList<Joueur> tab = new ArrayList<>();
        Connection conn = this.Getconnection();

        try {
            PreparedStatement st = conn.prepareStatement("SELECT * FROM joueur INNER JOIN licence ON joueur.no_licence = licence.no_licence");

            System.out.println("Connexion effective !");

            ResultSet result = st.executeQuery();

            while (result.next()) {
                Joueur j = new Joueur(
                        result.getInt("no_licence"),
                        result.getDate("datelic"),
                        result.getString("prenom"),
                        result.getString("nom"),
                        result.getString("adresse"),
                        result.getString("sexe"),
                        result.getDate("datenaiss"),
                        result.getString("poste")
                );
                //appel du constructeur par defaut
                tab.add(j);

            }
        } catch (Exception e) {
            System.err.println("Erreur JoueurDao select");
            e.printStackTrace();
        }
        return tab;
    }
    public boolean Ajouterlejoueur(Joueur j) throws SQLException, ClassNotFoundException {
        Connection conn2 = this.Getconnection();

        try {
            System.out.println("Connexion effective !");
            PreparedStatement st = conn2.prepareStatement("INSERT INTO licence(no_licence,datelic,prenom,nom,adresse,sexe,datenaiss,poste) VALUES(?,?,?,?,?,?,?)");

            st.setInt(1, j.getNoLicencie());
            st.setDate(2, j.getDatelic());
            st.setString(3,j.getPrenom());
            st.setString(4,j.getNom());
            st.setString(5,j.getAdresse());
            st.setString(6,j.getSexe());
            st.setString(7,j.getPoste());

            int result = st.executeUpdate();
        }
        catch (Exception e) {
            System.err.println("Erreur JoueurDao insert");
            e.printStackTrace();
        }

        return false;
    }
    public void UpdateJoueur(Joueur j) throws SQLException, ClassNotFoundException{
        Connection conn3 = this.Getconnection();

        try {
            System.out.println("Connexion effective !");
            // create our java preparedstatement using a sql update query
            PreparedStatement ps = conn3.prepareStatement("UPDATE joueur SET poste = ? WHERE no_licence = ? ");

            // set the preparedstatement parameters
            ps.setInt(1,j.getNoLicencie());
            ps.setString(2,j.getPoste());

            // call executeUpdate to execute our sql update statement
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            // log the exception
            System.err.println("Erreur JoueurDao update");
            e.printStackTrace();
        }
    }
    public void DeleteJoueur(String j) throws SQLException, ClassNotFoundException
    {
        Connection conn4 = this.Getconnection();
        try
        {
            System.out.println("Connexion effective !");
            PreparedStatement st = conn4.prepareStatement("DELETE FROM Table WHERE name = " + j + ";");
            st.executeUpdate();
        }
        catch(Exception e)
        {
            System.err.println("Erreur JoueurDao Delete");
            System.out.println(e);
        }
    }
}
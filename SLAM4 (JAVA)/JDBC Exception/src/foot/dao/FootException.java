package foot.dao;

public class FootException extends Exception{
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    private String className ;

    public FootException() {
    }

    public FootException(String message) {
        super(message);
    }

    public FootException(String message, Throwable cause) {
        super(message, cause);
    }

    public FootException(Throwable cause) {
        super(cause);
    }

    public FootException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

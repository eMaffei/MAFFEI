import java.sql.*;
import java.util.ArrayList;
import java.sql.Connection;

public class DAO {
    private Connection Getconnection() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/m2lfootball?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC&User=root&password=");

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return conn;
    }

    public ArrayList<Coach> recupererToutLesCaoch() {
        ArrayList<Coach> tab = new ArrayList<>();
        Connection conn = this.Getconnection();

        try {
            PreparedStatement st = conn.prepareStatement("SELECT * FROM coach INNER JOIN licence ON coach.no_licence = licence.no_licence");

            ResultSet result = st.executeQuery();

            while (result.next()) {
                Coach c = new Coach(
                        result.getInt("no_licence"),
                        result.getString("datelic"),
                        result.getString("prenom"),
                        result.getString("nom"),
                        result.getString("adresse"),
                        result.getString("sexe"),
                        result.getString("datenaiss")
                );
                ; //appel du constructeur par defaut
                tab.add(c);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return tab;
    }

    public ArrayList<Joueur> recuperertoutlesjoueurs() {
        ArrayList<Joueur> tab = new ArrayList<>();
        Connection conn = this.Getconnection();

        try {
            PreparedStatement st = conn.prepareStatement("SELECT * FROM joueur INNER JOIN licence ON joueur.no_licence = licence.no_licence");

            System.out.println("Connexion effective !");

            ResultSet result = st.executeQuery();

            while (result.next()) {
                Joueur j = new Joueur(
                        result.getInt("no_licence"),
                        result.getString("datelic"),
                        result.getString("prenom"),
                        result.getString("nom"),
                        result.getString("adresse"),
                        result.getString("sexe"),
                        result.getString("datenaiss"),
                        result.getString("poste")
                );
                //appel du constructeur par defaut
                tab.add(j);

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return tab;
    }
}



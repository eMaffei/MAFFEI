public class Licencie {
    private int NoLicencie;
    private String datelic;
    private String prenom;
    private String nom;
    private String adresse;
    private String sexe;
    private String datenaiss;

    @Override
    public String toString() {
        return "Licencie{" +
                "NoLicencie=" + NoLicencie +
                ", datelic='" + datelic + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", sexe='" + sexe + '\'' +
                ", datenaiss='" + datenaiss + '\'' +
                '}';
    }

    public Licencie(int noLicencie, String datelic, String prenom, String nom, String adresse, String sexe, String datenaiss) {
        setNoLicencie(noLicencie);
        this.setDatelic(datelic);
        this.setPrenom(prenom);
        this.setNom(nom);
        this.setAdresse(adresse);
        this.setSexe(sexe);
        this.setDatenaiss(datenaiss);
    }

    public Licencie() {

    }

    public int getNoLicencie() {
        return NoLicencie;
    }

    public void setNoLicencie(int noLicencie) {
        NoLicencie = noLicencie;
    }

    public String getDatelic() {
        return datelic;
    }

    public void setDatelic(String datelic) {
        this.datelic = datelic;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(String datenaiss) {
        this.datenaiss = datenaiss;
    }
}

public class Joueur extends Licencie{
    private String poste;

    public Joueur(int noLicencie, String datelic, String prenom, String nom, String adresse, String sexe, String datenaiss, String poste) {
        super(noLicencie, datelic, prenom, nom, adresse, sexe, datenaiss);
        this.setPoste(poste);
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }
}

public class Equipe {
    private String nom;
    private String division;
    private String categorie;

    @Override
    public String toString() {
        return "Equipe{" +
                "nom='" + nom + '\'' +
                ", division='" + division + '\'' +
                ", categorie='" + categorie + '\'' +
                '}';
    }

    public Equipe(String nom, String division, String categorie) {
        this.setNom(nom);
        this.setDivision(division);
        this.setCategorie(categorie);
    }

    public Equipe() {
        super();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
}

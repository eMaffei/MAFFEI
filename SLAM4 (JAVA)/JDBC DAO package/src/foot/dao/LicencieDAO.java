package foot.dao;
import foot.bean.Equipe;
import foot.bean.Licencie;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class LicencieDAO extends DAO {
    public ArrayList<Licencie> recupererToutsleslicences() {
        ArrayList<Licencie> tab = new ArrayList<>();
        Connection conn = this.Getconnection();

        try {
            PreparedStatement mb = conn.prepareStatement("SELECT * FROM licence");

            ResultSet result = mb.executeQuery();

            while (result.next()) {
                Licencie l = new Licencie(
                        result.getInt("no_licence"),
                        result.getDate("datelic"),
                        result.getString("prenom"),
                        result.getString("nom"),
                        result.getString("adresse"),
                        result.getString("sexe"),
                        result.getDate("datenaiss")
                );
                ; //appel du constructeur par defaut
                tab.add(l);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return tab;
    }
}

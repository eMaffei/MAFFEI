package foot.dao;
import foot.bean.Equipe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class EquipeDAO extends DAO {
    public ArrayList<Equipe> recupererToutslesequipes() {
        ArrayList<Equipe> tab = new ArrayList<>();
        Connection conn = this.Getconnection();

        try {
            PreparedStatement mb = conn.prepareStatement("SELECT * FROM equipe INNER JOIN licence ON equipe.id_equipe = licence.no_licence");

            ResultSet result = mb.executeQuery();

            while (result.next()) {
                Equipe e = new Equipe(
                        result.getInt("no_licence"),
                        result.getString("datelic"),
                        result.getString("prenom"),
                        result.getString("nom"),
                        result.getString("adresse"),
                        result.getString("sexe"),
                        result.getString("datenaiss")
                );
                ; //appel du constructeur par defaut
                tab.add(e);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return tab;
    }
}

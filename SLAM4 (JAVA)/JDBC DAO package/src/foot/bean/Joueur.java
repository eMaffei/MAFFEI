package foot.bean;

import java.sql.Date;

public class Joueur extends Licencie{
    private String poste;

    public Joueur(int noLicencie, Date datelic, String prenom, String nom, String adresse, String sexe, Date datenaiss, String poste) {
        super(noLicencie, datelic, prenom, nom, adresse, sexe, datenaiss);
        this.setPoste(poste);
    }

    public Joueur(int no_licence, Date datelic, String prenom, String nom, String adresse, String sexe, Date datenaiss){ }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {

        this.poste = poste;
    }

    @Override
    public String toString() {
        return "Joueur{" + super.toString() + ", " +
                "poste='" + poste + '\'' +
                '}';
    }
}

package sample;

import com.sun.xml.internal.ws.api.server.EndpointReferenceExtensionContributor;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.text.ParseException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws ParseException{
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        //VBox root = new VBox();
        //HBox namePane = new HBox();
        GridPane root = new GridPane();
        root.setHgap(10); // mettre un ecart entre les lignes
        root.setVgap(10); // mettre un ecart entre les collonnes

        primaryStage.setTitle("JavaFX");
        primaryStage.setScene(new Scene(root, 300, 275));


        //ajout d'un bouton
        Text label=new Text("Calculette");
        Text label1=new Text("nombre1");
        Text label2=new Text("nombre2");

        Button btnplus = new Button("+");
        Button btnmoin = new Button("-");
        Button btnClear = new Button("C");
        Button btndiviser = new Button("/");
        Button btnmultiplier = new Button("x");
        //Button btn =new Button( "Valider ");
        Text resultxt= new Text();
        TextField nombre1 = new TextField();
        TextField nombre2 = new TextField();



        //AJOUT D'un evenement
        btnplus.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                try{
                    double nb1 = Double.parseDouble(nombre1.getText());
                    double nb2 = Double.parseDouble(nombre2.getText());

                    resultxt.setText(Double.toString(nb1 + nb2));

                    System.out.println(resultxt.getText());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    resultxt.setText("ceci n'est pas un nombre");
                }
            }
        });

        btnmoin.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                try{
                    double nb1 = Double.parseDouble(nombre1.getText());
                    double nb2 = Double.parseDouble(nombre2.getText());

                    resultxt.setText(Double.toString(nb1 - nb2));

                    System.out.println(resultxt.getText());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    resultxt.setText("ceci n'est pas un nombre");
                }
            }
        });

        btndiviser.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                try{
                    double nb1 = Double.parseDouble(nombre1.getText());
                    double nb2 = Double.parseDouble(nombre2.getText());

                    resultxt.setText(Double.toString(nb1 / nb2));

                    System.out.println(resultxt.getText());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    resultxt.setText("ceci n'est pas un nombre");
                }
            }
        });

        btnmultiplier.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                try{
                    double nb1 = Double.parseDouble(nombre1.getText());
                    double nb2 = Double.parseDouble(nombre2.getText());

                    resultxt.setText(Double.toString(nb1 * nb2));

                    System.out.println(resultxt.getText());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    resultxt.setText("ceci n'est pas un nombre");
                }
            }
        });

        btnClear.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                try{
                    nombre1.clear();
                    nombre2.clear();
                    resultxt.setText(null); // sert a clear la donnée trouver comme résultat
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    resultxt.setText("ceci n'est pas un nombre");
                }
            }
        });
        root.setGridLinesVisible(false); // voir la grille true = pour voir la grille

        //root.getChildren().add(label);
        //root.getChildren().add(txt);
        //root.getChildren().add(btn);

        //namePane.getChildren().add(label);
        //namePane.getChildren().add(txt);
        //root.getChildren().addAll(namePane,btn, resultxt);


        label.setStyle("-fx-font: 22 arial; -fx-base: #b6e7c9;");// test de css
        btnClear.setStyle("-fx-font: 22 arial; -fx-base: #b6e7c9;");// de meme test de css dans javafx
        root.add(resultxt, 2 ,5);
        //root.add(btn, 1, 1);
        root.setAlignment(Pos.CENTER);
        root.add(label, 0, 0);
        root.add(label1, 0, 1);
        root.add(label2, 1, 1);

        root.add(nombre1,  0,2);
        root.add(nombre2,  1,2);

       //root.add(btn, 4, 4); // position 1 et 1

        root.add(btnplus, 1, 3);
        root.add(btnClear, 0, 3);
        root.add(btnmoin, 2 , 3);
        root.add(btndiviser, 3,3);
        root.add(btnmultiplier, 4,3);

        //btnplus.setMaxWidth(Double.MAX_VALUE); //sert a ce que le bouton prenne toute la lignes et collonnes ou il se situe
        primaryStage.show();
    }




    public static void main(String[] args) {
        launch(args);
    }
}
var tbl_region_2016 = [
{"reg_2016_code": "NR_01", "reg_2016_nom": "Apple"}, /* NR_01 = apple */
{"reg_2016_code": "NR_02", "reg_2016_nom": "Samsung"}, 
{"reg_2016_code": "NR_03", "reg_2016_nom": "Huawei"}, 
{"reg_2016_code": "NR_04", "reg_2016_nom": "Sony"}, 
{"reg_2016_code": "NR_05", "reg_2016_nom": "Wiko"},
{"reg_2016_code": "NR_06", "reg_2016_nom": "LG"}, 
{"reg_2016_code": "NR_07", "reg_2016_nom": "Honor"},
];

var tbl_old_region =[ 

/* Téléphone Iphone ici */

{"reg_code": "R_01", "reg_nom": "Iphone XR", "reg_2016_code": "NR_01"},
{"reg_code": "R_02", "reg_nom": "Iphone XS Max", "reg_2016_code": "NR_01"},
{"reg_code": "R_03", "reg_nom": "Iphone XS", "reg_2016_code": "NR_01"},  
{"reg_code": "R_04", "reg_nom": "Iphone X", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_05", "reg_nom": "Iphone 8 Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_06", "reg_nom": "Iphone 8", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_07", "reg_nom": "Iphone 7 Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_08", "reg_nom": "Iphone 7", "reg_2016_code": "NR_01"},  
{"reg_code": "R_09", "reg_nom": "Iphone 6s Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_10", "reg_nom": "Iphone 6s", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_11", "reg_nom": "Iphone 6 Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_12", "reg_nom": "Iphone 6", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_13", "reg_nom": "Iphone 5SE", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_14", "reg_nom": "Iphone 5S", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_15", "reg_nom": "Iphone 5C", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_16", "reg_nom": "Iphone 5", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_17", "reg_nom": "Iphone 4s", "reg_2016_code": "NR_01"},
{"reg_code": "R_18", "reg_nom": "Iphone 4", "reg_2016_code": "NR_01"},

/* Téléphone Samsung S */
{"reg_code": "R_19", "reg_nom": "Galaxy S10+", "reg_2016_code": "NR_02"},
{"reg_code": "R_20", "reg_nom": "Galaxy S10E", "reg_2016_code": "NR_02"},
{"reg_code": "R_21", "reg_nom": "Galaxy S10", "reg_2016_code": "NR_02"},
{"reg_code": "R_22", "reg_nom": "Galaxy S9+", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_23", "reg_nom": "Galaxy S9", "reg_2016_code": "NR_02"},
{"reg_code": "R_24", "reg_nom": "Galaxy S8+", "reg_2016_code": "NR_02"},
{"reg_code": "R_25", "reg_nom": "Galaxy S8", "reg_2016_code": "NR_02"},
{"reg_code": "R_26", "reg_nom": "Galaxy S7 Edge", "reg_2016_code": "NR_02"},
{"reg_code": "R_27", "reg_nom": "Galaxy S7", "reg_2016_code": "NR_02"},
{"reg_code": "R_28", "reg_nom": "Galaxy S6 Edge Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_29", "reg_nom": "Galaxy S6 Edge ", "reg_2016_code": "NR_02"},
{"reg_code": "R_30", "reg_nom": "Galaxy S6", "reg_2016_code": "NR_02"},
{"reg_code": "R_31", "reg_nom": "Galaxy S5 K Zoom", "reg_2016_code": "NR_02"},
{"reg_code": "R_32", "reg_nom": "Galaxy S5 Active", "reg_2016_code": "NR_02"},
{"reg_code": "R_33", "reg_nom": "Galaxy S5 4G+", "reg_2016_code": "NR_02"},
{"reg_code": "R_34", "reg_nom": "Galaxy S5", "reg_2016_code": "NR_02"},
{"reg_code": "R_35", "reg_nom": "Galaxy S4 Zoom", "reg_2016_code": "NR_02"},
{"reg_code": "R_36", "reg_nom": "Galaxy S4 Value Edition", "reg_2016_code": "NR_02"},
{"reg_code": "R_37", "reg_nom": "Galaxy S4 Advance 4G", "reg_2016_code": "NR_02"},
{"reg_code": "R_38", "reg_nom": "Galaxy S4 Active", "reg_2016_code": "NR_02"},
{"reg_code": "R_39", "reg_nom": "Galaxy S4 4G (19505)", "reg_2016_code": "NR_02"},
{"reg_code": "R_40", "reg_nom": "Galaxy S4", "reg_2016_code": "NR_02"},
{"reg_code": "R_41", "reg_nom": "Galaxy S3", "reg_2016_code": "NR_02"},
{"reg_code": "R_42", "reg_nom": "Galaxy S2 Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_43", "reg_nom": "Galaxy S2", "reg_2016_code": "NR_02"},
{"reg_code": "R_44", "reg_nom": "Galaxy S Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_45", "reg_nom": "Galaxy S Duos", "reg_2016_code": "NR_02"},
{"reg_code": "R_46", "reg_nom": "Galaxy S5 Mini", "reg_2016_code": "NR_02"},
{"reg_code": "R_47", "reg_nom": "Galaxy S4 Mini Duos", "reg_2016_code": "NR_02"},
{"reg_code": "R_48", "reg_nom": "Galaxy S4 Mini 4G", "reg_2016_code": "NR_02"},
{"reg_code": "R_49", "reg_nom": "Galaxy S4 Mini", "reg_2016_code": "NR_02"},
{"reg_code": "R_50", "reg_nom": "Galaxy S3 Mini", "reg_2016_code": "NR_02"},


/*Telephone Samsung Note */
{"reg_code": "R_51", "reg_nom": "Galaxy Note Edge", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_52", "reg_nom": "Galaxy Note 9", "reg_2016_code": "NR_02"},  
{"reg_code": "R_53", "reg_nom": "Galaxy Note 8", "reg_2016_code": "NR_02"},
{"reg_code": "R_54", "reg_nom": "Galaxy Note 7", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_55", "reg_nom": "Galaxy Note 5", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_56", "reg_nom": "Galaxy Note 4", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_57", "reg_nom": "Galaxy Note 3 Neo", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_58", "reg_nom": "Galaxy Note 3 Lte", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_59", "reg_nom": "Galaxy Note 3 Dual", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_60", "reg_nom": "Galaxy Note 2 Lte", "reg_2016_code": "NR_02"},
{"reg_code": "R_61", "reg_nom": "Galaxy Note 2", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_62", "reg_nom": "Galaxy Note", "reg_2016_code": "NR_02"},

{"reg_code": "R_63", "reg_nom": "Galaxy A9", "reg_2016_code": "NR_02"},
{"reg_code": "R_64", "reg_nom": "Galaxy A8 (2018)", "reg_2016_code": "NR_02"},
{"reg_code": "R_65", "reg_nom": "Galaxy A8", "reg_2016_code": "NR_02"},
{"reg_code": "R_66", "reg_nom": "Galaxy A70", "reg_2016_code": "NR_02"},
{"reg_code": "R_67", "reg_nom": "Galaxy A7 (2018)", "reg_2016_code": "NR_02"},
{"reg_code": "R_68", "reg_nom": "Galaxy A7 (2017)", "reg_2016_code": "NR_02"},
{"reg_code": "R_69", "reg_nom": "Galaxy A7 (2016)", "reg_2016_code": "NR_02"},
{"reg_code": "R_70", "reg_nom": "Galaxy A7", "reg_2016_code": "NR_02"},
{"reg_code": "R_71", "reg_nom": "Galaxy A6 Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_72", "reg_nom": "Galaxy A6", "reg_2016_code": "NR_02"},
{"reg_code": "R_73", "reg_nom": "Galaxy A50", "reg_2016_code": "NR_02"},
{"reg_code": "R_74", "reg_nom": "Galaxy A5 (2017)", "reg_2016_code": "NR_02"},
{"reg_code": "R_75", "reg_nom": "Galaxy A5 (2016)", "reg_2016_code": "NR_02"},
{"reg_code": "R_76", "reg_nom": "Galaxy A5 (2015)", "reg_2016_code": "NR_02"},
{"reg_code": "R_77", "reg_nom": "Galaxy A40", "reg_2016_code": "NR_02"},
{"reg_code": "R_78", "reg_nom": "Galaxy A30", "reg_2016_code": "NR_02"},
{"reg_code": "R_79", "reg_nom": "Galaxy A3 (2017)", "reg_2016_code": "NR_02"},
{"reg_code": "R_80", "reg_nom": "Galaxy A3 (2016)", "reg_2016_code": "NR_02"},
{"reg_code": "R_81", "reg_nom": "Galaxy A3", "reg_2016_code": "NR_02"},

{"reg_code": "R_82", "reg_nom": "Galaxy Ace 4", "reg_2016_code": "NR_02"},
{"reg_code": "R_83", "reg_nom": "Galaxy Ace 3 S7270", "reg_2016_code": "NR_02"},
{"reg_code": "R_84", "reg_nom": "Galaxy Ace 3", "reg_2016_code": "NR_02"},
{"reg_code": "R_85", "reg_nom": "Galaxy Ace 2", "reg_2016_code": "NR_02"},
{"reg_code": "R_86", "reg_nom": "Galaxy Ace", "reg_2016_code": "NR_02"},

{"reg_code": "R_87", "reg_nom": "Galaxy J7 2017", "reg_2016_code": "NR_02"},
{"reg_code": "R_88", "reg_nom": "Galaxy J7 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_89", "reg_nom": "Galaxy J6 2018", "reg_2016_code": "NR_02"},
{"reg_code": "R_90", "reg_nom": "Galaxy J5 2017", "reg_2016_code": "NR_02"},
{"reg_code": "R_91", "reg_nom": "Galaxy J5 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_92", "reg_nom": "Galaxy J4 2018", "reg_2016_code": "NR_02"},
{"reg_code": "R_93", "reg_nom": "Galaxy J3 2017", "reg_2016_code": "NR_02"},
{"reg_code": "R_94", "reg_nom": "Galaxy J3 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_95", "reg_nom": "Galaxy J1 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_96", "reg_nom": "Galaxy J1", "reg_2016_code": "NR_02"},

/* telephone huawei */
{"reg_code": "R_97", "reg_nom": "P9 Plus", "reg_2016_code": "NR_03"},
{"reg_code": "R_98", "reg_nom": "P9 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_99", "reg_nom": "P9", "reg_2016_code": "NR_03"},
{"reg_code": "R_100", "reg_nom": "P8 Lite (2017)", "reg_2016_code": "NR_03"},
{"reg_code": "R_101", "reg_nom": "P8", "reg_2016_code": "NR_03"},
{"reg_code": "R_102", "reg_nom": "P30 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_103", "reg_nom": "P30 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_104", "reg_nom": "P30", "reg_2016_code": "NR_03"},
{"reg_code": "R_105", "reg_nom": "P20 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_106", "reg_nom": "P20 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_107", "reg_nom": "P20", "reg_2016_code": "NR_03"},
{"reg_code": "R_108", "reg_nom": "P10 Plus", "reg_2016_code": "NR_03"},
{"reg_code": "R_109", "reg_nom": "P10 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_110", "reg_nom": "P10", "reg_2016_code": "NR_03"},
{"reg_code": "R_111", "reg_nom": "P Smart+", "reg_2016_code": "NR_03"},
{"reg_code": "R_112", "reg_nom": "P Smart", "reg_2016_code": "NR_03"},
{"reg_code": "R_113", "reg_nom": "Porsche Design Mate Rs", "reg_2016_code": "NR_03"},
{"reg_code": "R_114", "reg_nom": "Mate S", "reg_2016_code": "NR_03"},
{"reg_code": "R_115", "reg_nom": "Mate 9", "reg_2016_code": "NR_03"},
{"reg_code": "R_116", "reg_nom": "Mate 8", "reg_2016_code": "NR_03"},
{"reg_code": "R_117", "reg_nom": "Mate 7", "reg_2016_code": "NR_03"},
{"reg_code": "R_118", "reg_nom": "Mate 20X", "reg_2016_code": "NR_03"},
{"reg_code": "R_119", "reg_nom": "Mate 20 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_120", "reg_nom": "Mate 20 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_121", "reg_nom": "Mate 20", "reg_2016_code": "NR_03"},
{"reg_code": "R_122", "reg_nom": "Mate 10 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_123", "reg_nom": "Mate 10 Lite", "reg_2016_code": "NR_03"},

/* telephone Sony */
{"reg_code": "R_124", "reg_nom": "Xperia Zr", "reg_2016_code": "NR_04"},
{"reg_code": "R_125", "reg_nom": "Xperia Z5 Premium", "reg_2016_code": "NR_04"},
{"reg_code": "R_126", "reg_nom": "Xperia Z5 Dual", "reg_2016_code": "NR_04"},
{"reg_code": "R_127", "reg_nom": "Xperia Z5 Compact", "reg_2016_code": "NR_04"},
{"reg_code": "R_128", "reg_nom": "Xperia Z5 ", "reg_2016_code": "NR_04"},
{"reg_code": "R_129", "reg_nom": "Xperia Z3 Compact", "reg_2016_code": "NR_04"},
{"reg_code": "R_130", "reg_nom": "Xperia Z3", "reg_2016_code": "NR_04"},
{"reg_code": "R_131", "reg_nom": "Xperia Z2", "reg_2016_code": "NR_04"},
{"reg_code": "R_132", "reg_nom": "Xperia Z1 Compact", "reg_2016_code": "NR_04"},
{"reg_code": "R_133", "reg_nom": "Xperia Z1", "reg_2016_code": "NR_04"},
{"reg_code": "R_134", "reg_nom": "Xperia Z Ultra", "reg_2016_code": "NR_04"},
{"reg_code": "R_135", "reg_nom": "Xperia Z", "reg_2016_code": "NR_04"},

/* WIKO */
{"reg_code": "R_136", "reg_nom": "Ufeel", "reg_2016_code": "NR_05"},
{"reg_code": "R_137", "reg_nom": "Tommy", "reg_2016_code": "NR_05"},
{"reg_code": "R_138", "reg_nom": "Ridge", "reg_2016_code": "NR_05"},
{"reg_code": "R_139", "reg_nom": "Rainbow Lite Dual Sim", "reg_2016_code": "NR_05"},
{"reg_code": "R_140", "reg_nom": "Rainbow Jam", "reg_2016_code": "NR_05"},
{"reg_code": "R_141", "reg_nom": "Pulp 4G", "reg_2016_code": "NR_05"},
{"reg_code": "R_142", "reg_nom": "Lenny 3", "reg_2016_code": "NR_05"},
{"reg_code": "R_143", "reg_nom": "Lenny 2", "reg_2016_code": "NR_05"},
{"reg_code": "R_144", "reg_nom": "Highway 4G", "reg_2016_code": "NR_05"},

/* LG */
{"reg_code": "R_145", "reg_nom": "V30", "reg_2016_code": "NR_06"},
{"reg_code": "R_146", "reg_nom": "Optimus G Pro", "reg_2016_code": "NR_06"},
{"reg_code": "R_147", "reg_nom": "Optimus G E975", "reg_2016_code": "NR_06"},
{"reg_code": "R_148", "reg_nom": "Optimus G", "reg_2016_code": "NR_06"},
{"reg_code": "R_149", "reg_nom": "Optimus 4X Hd", "reg_2016_code": "NR_06"},
{"reg_code": "R_150", "reg_nom": "Nexus 5X", "reg_2016_code": "NR_06"},
{"reg_code": "R_151", "reg_nom": "Nexus 5", "reg_2016_code": "NR_06"},
{"reg_code": "R_152", "reg_nom": "Nexus 4", "reg_2016_code": "NR_06"},
{"reg_code": "R_153", "reg_nom": "G7", "reg_2016_code": "NR_06"},
{"reg_code": "R_154", "reg_nom": "G6", "reg_2016_code": "NR_06"},
{"reg_code": "R_155", "reg_nom": "G5", "reg_2016_code": "NR_06"},
{"reg_code": "R_156", "reg_nom": "G4", "reg_2016_code": "NR_06"},
{"reg_code": "R_157", "reg_nom": "G3S", "reg_2016_code": "NR_06"},
{"reg_code": "R_158", "reg_nom": "G3", "reg_2016_code": "NR_06"},
{"reg_code": "R_159", "reg_nom": "G2", "reg_2016_code": "NR_06"},
{"reg_code": "R_160", "reg_nom": "G Pro 2", "reg_2016_code": "NR_06"},
{"reg_code": "R_161", "reg_nom": "G Flex 2", "reg_2016_code": "NR_06"},
{"reg_code": "R_162", "reg_nom": "G Flex", "reg_2016_code": "NR_06"},

/*Honor */
{"reg_code": "R_163", "reg_nom": "View 20", "reg_2016_code": "NR_07"},
{"reg_code": "R_164", "reg_nom": "View 10", "reg_2016_code": "NR_07"},
{"reg_code": "R_165", "reg_nom": "Play", "reg_2016_code": "NR_07"},
{"reg_code": "R_166", "reg_nom": "Honor 10", "reg_2016_code": "NR_07"},
{"reg_code": "R_167", "reg_nom": "Honor 9 Lite", "reg_2016_code": "NR_07"},
{"reg_code": "R_168", "reg_nom": "Honor 9", "reg_2016_code": "NR_07"},
{"reg_code": "R_169", "reg_nom": "Honor 8X", "reg_2016_code": "NR_07"},
{"reg_code": "R_170", "reg_nom": "Honor 8", "reg_2016_code": "NR_07"},
{"reg_code": "R_171", "reg_nom": "Honor 7X", "reg_2016_code": "NR_07"},
{"reg_code": "R_172", "reg_nom": "Honor 7 Prenuim", "reg_2016_code": "NR_07"},
{"reg_code": "R_173", "reg_nom": "Honor 6X", "reg_2016_code": "NR_07"},
{"reg_code": "R_174", "reg_nom": "Honor 6C", "reg_2016_code": "NR_07"},
{"reg_code": "R_175", "reg_nom": "Honor 5X", "reg_2016_code": "NR_07"},
{"reg_code": "R_176", "reg_nom": "Honor 5C", "reg_2016_code": "NR_07"},
{"reg_code": "R_177", "reg_nom": "8 pro", "reg_2016_code": "NR_07"},
{"reg_code": "R_178", "reg_nom": "7A", "reg_2016_code": "NR_07"},

/* {"reg_code": "R_179", "reg_nom": "nom du telephone ici", "reg_2016_code": "NR_07"},*/
/*																			NR_07 = honor vu plus haut ^ */
];

var tbl_etat_telephone =[
{"tel_code": "E_01", "dep_nom": "Intact", "reg_code": "R_01"}, 
{"tel_code": "E_02", "dep_nom": "Micro-rayure", "reg_code": "R_01"},
{"tel_code": "E_03", "dep_nom": "Rayures", "reg_code": "R_01"}, 
{"tel_code": "E_04", "dep_nom": "Cassé", "reg_code": "R_01"},

{"tel_code": "E_05", "dep_nom": "Intact", "reg_code": "R_02"}, 
{"tel_code": "E_06", "dep_nom": "Micro-rayure", "reg_code": "R_02"},
{"tel_code": "E_07", "dep_nom": "Rayures", "reg_code": "R_02"}, 
{"tel_code": "E_08", "dep_nom": "Cassé", "reg_code": "R_02"},

{"tel_code": "E_09", "dep_nom": "Intact", "reg_code": "R_03"}, 
{"tel_code": "E_10", "dep_nom": "Micro-rayure", "reg_code": "R_03"},
{"tel_code": "E_11", "dep_nom": "Rayures", "reg_code": "R_03"}, 
{"tel_code": "E_12", "dep_nom": "Cassé", "reg_code": "R_03"},

{"tel_code": "E_13", "dep_nom": "Intact", "reg_code": "R_04"}, 
{"tel_code": "E_14", "dep_nom": "Micro-rayure", "reg_code": "R_04"},
{"tel_code": "E_15", "dep_nom": "Rayures", "reg_code": "R_04"}, 
{"tel_code": "E_16", "dep_nom": "Cassé", "reg_code": "R_04"},

{"tel_code": "E_17", "dep_nom": "Intact", "reg_code": "R_05"}, 
{"tel_code": "E_18", "dep_nom": "Micro-rayure", "reg_code": "R_05"},
{"tel_code": "E_19", "dep_nom": "Rayures", "reg_code": "R_05"}, 
{"tel_code": "E_20", "dep_nom": "Cassé", "reg_code": "R_05"},

{"tel_code": "E_21", "dep_nom": "Intact", "reg_code": "R_06"}, 
{"tel_code": "E_22", "dep_nom": "Micro-rayure", "reg_code": "R_06"},
{"tel_code": "E_23", "dep_nom": "Rayures", "reg_code": "R_06"}, 
{"tel_code": "E_24", "dep_nom": "Cassé", "reg_code": "R_06"},

{"tel_code": "E_25", "dep_nom": "Intact", "reg_code": "R_07"}, 
{"tel_code": "E_26", "dep_nom": "Micro-rayure", "reg_code": "R_07"},
{"tel_code": "E_27", "dep_nom": "Rayures", "reg_code": "R_07"}, 
{"tel_code": "E_28", "dep_nom": "Cassé", "reg_code": "R_07"},

{"tel_code": "E_29", "dep_nom": "Intact", "reg_code": "R_08"}, 
{"tel_code": "E_30", "dep_nom": "Micro-rayure", "reg_code": "R_08"},
{"tel_code": "E_31", "dep_nom": "Rayures", "reg_code": "R_08"}, 
{"tel_code": "E_32", "dep_nom": "Cassé", "reg_code": "R_08"},

{"tel_code": "E_33", "dep_nom": "Intact", "reg_code": "R_09"}, 
{"tel_code": "E_34", "dep_nom": "Micro-rayure", "reg_code": "R_09"},
{"tel_code": "E_35", "dep_nom": "Rayures", "reg_code": "R_09"}, 
{"tel_code": "E_36", "dep_nom": "Cassé", "reg_code": "R_09"},

{"tel_code": "E_37", "dep_nom": "Intact", "reg_code": "R_10"}, 
{"tel_code": "E_38", "dep_nom": "Micro-rayure", "reg_code": "R_10"},
{"tel_code": "E_39", "dep_nom": "Rayures", "reg_code": "R_10"}, 
{"tel_code": "E_40", "dep_nom": "Cassé", "reg_code": "R_10"},

{"tel_code": "E_41", "dep_nom": "Intact", "reg_code": "R_11"}, 
{"tel_code": "E_42", "dep_nom": "Micro-rayure", "reg_code": "R_11"},
{"tel_code": "E_43", "dep_nom": "Rayures", "reg_code": "R_11"}, 
{"tel_code": "E_44", "dep_nom": "Cassé", "reg_code": "R_11"},

{"tel_code": "E_45", "dep_nom": "Intact", "reg_code": "R_12"}, 
{"tel_code": "E_46", "dep_nom": "Micro-rayure", "reg_code": "R_12"},
{"tel_code": "E_47", "dep_nom": "Rayures", "reg_code": "R_12"}, 
{"tel_code": "E_48", "dep_nom": "Cassé", "reg_code": "R_12"},

{"tel_code": "E_49", "dep_nom": "Intact", "reg_code": "R_13"}, 
{"tel_code": "E_50", "dep_nom": "Micro-rayure", "reg_code": "R_13"},
{"tel_code": "E_51", "dep_nom": "Rayures", "reg_code": "R_13"}, 
{"tel_code": "E_52", "dep_nom": "Cassé", "reg_code": "R_13"},

{"tel_code": "E_53", "dep_nom": "Intact", "reg_code": "R_14"}, 
{"tel_code": "E_54", "dep_nom": "Micro-rayure", "reg_code": "R_14"},
{"tel_code": "E_55", "dep_nom": "Rayures", "reg_code": "R_14"}, 
{"tel_code": "E_56", "dep_nom": "Cassé", "reg_code": "R_14"},

{"tel_code": "E_57", "dep_nom": "Intact", "reg_code": "R_15"}, 
{"tel_code": "E_58", "dep_nom": "Micro-rayure", "reg_code": "R_15"},
{"tel_code": "E_59", "dep_nom": "Rayures", "reg_code": "R_15"}, 
{"tel_code": "E_60", "dep_nom": "Cassé", "reg_code": "R_15"},

{"tel_code": "E_61", "dep_nom": "Intact", "reg_code": "R_16"}, 
{"tel_code": "E_62", "dep_nom": "Micro-rayure", "reg_code": "R_16"},
{"tel_code": "E_63", "dep_nom": "Rayures", "reg_code": "R_16"}, 
{"tel_code": "E_64", "dep_nom": "Cassé", "reg_code": "R_16"},

{"tel_code": "E_65", "dep_nom": "Intact", "reg_code": "R_17"}, 
{"tel_code": "E_66", "dep_nom": "Micro-rayure", "reg_code": "R_17"},
{"tel_code": "E_67", "dep_nom": "Rayures", "reg_code": "R_17"}, 
{"tel_code": "E_68", "dep_nom": "Cassé", "reg_code": "R_17"},

{"tel_code": "E_69", "dep_nom": "Intact", "reg_code": "R_18"}, 
{"tel_code": "E_70", "dep_nom": "Micro-rayure", "reg_code": "R_18"},
{"tel_code": "E_71", "dep_nom": "Rayures", "reg_code": "R_18"}, 
{"tel_code": "E_72", "dep_nom": "Cassé", "reg_code": "R_18"},

{"tel_code": "E_73", "dep_nom": "Intact", "reg_code": "R_19"}, 
{"tel_code": "E_74", "dep_nom": "Micro-rayure", "reg_code": "R_19"},
{"tel_code": "E_75", "dep_nom": "Rayures", "reg_code": "R_19"}, 
{"tel_code": "E_76", "dep_nom": "Cassé", "reg_code": "R_19"},

{"tel_code": "E_77", "dep_nom": "Intact", "reg_code": "R_20"}, 
{"tel_code": "E_78", "dep_nom": "Micro-rayure", "reg_code": "R_20"},
{"tel_code": "E_79", "dep_nom": "Rayures", "reg_code": "R_20"}, 
{"tel_code": "E_80", "dep_nom": "Cassé", "reg_code": "R_20"},

{"tel_code": "E_81", "dep_nom": "Intact", "reg_code": "R_21"}, 
{"tel_code": "E_82", "dep_nom": "Micro-rayure", "reg_code": "R_21"},
{"tel_code": "E_83", "dep_nom": "Rayures", "reg_code": "R_21"}, 
{"tel_code": "E_84", "dep_nom": "Cassé", "reg_code": "R_21"},

{"tel_code": "E_85", "dep_nom": "Intact", "reg_code": "R_22"}, 
{"tel_code": "E_86", "dep_nom": "Micro-rayure", "reg_code": "R_22"},
{"tel_code": "E_87", "dep_nom": "Rayures", "reg_code": "R_22"}, 
{"tel_code": "E_88", "dep_nom": "Cassé", "reg_code": "R_22"},

{"tel_code": "E_89", "dep_nom": "Intact", "reg_code": "R_23"}, 
{"tel_code": "E_90", "dep_nom": "Micro-rayure", "reg_code": "R_23"},
{"tel_code": "E_91", "dep_nom": "Rayures", "reg_code": "R_23"}, 
{"tel_code": "E_92", "dep_nom": "Cassé", "reg_code": "R_23"},

{"tel_code": "E_93", "dep_nom": "Intact", "reg_code": "R_24"}, 
{"tel_code": "E_94", "dep_nom": "Micro-rayure", "reg_code": "R_24"},
{"tel_code": "E_95", "dep_nom": "Rayures", "reg_code": "R_24"}, 
{"tel_code": "E_96", "dep_nom": "Cassé", "reg_code": "R_24"},

{"tel_code": "E_97", "dep_nom": "Intact", "reg_code": "R_25"}, 
{"tel_code": "E_98", "dep_nom": "Micro-rayure", "reg_code": "R_25"},
{"tel_code": "E_99", "dep_nom": "Rayures", "reg_code": "R_25"}, 
{"tel_code": "E_100", "dep_nom": "Cassé", "reg_code": "R_25"},

{"tel_code": "E_101", "dep_nom": "Intact", "reg_code": "R_26"}, 
{"tel_code": "E_102", "dep_nom": "Micro-rayure", "reg_code": "R_26"},
{"tel_code": "E_103", "dep_nom": "Rayures", "reg_code": "R_26"}, 
{"tel_code": "E_104", "dep_nom": "Cassé", "reg_code": "R_26"},

{"tel_code": "E_105", "dep_nom": "Intact", "reg_code": "R_27"}, 
{"tel_code": "E_106", "dep_nom": "Micro-rayure", "reg_code": "R_27"},
{"tel_code": "E_107", "dep_nom": "Rayures", "reg_code": "R_27"}, 
{"tel_code": "E_108", "dep_nom": "Cassé", "reg_code": "R_27"},

{"tel_code": "E_109", "dep_nom": "Intact", "reg_code": "R_28"}, 
{"tel_code": "E_110", "dep_nom": "Micro-rayure", "reg_code": "R_28"},
{"tel_code": "E_111", "dep_nom": "Rayures", "reg_code": "R_28"}, 
{"tel_code": "E_112", "dep_nom": "Cassé", "reg_code": "R_28"},

{"tel_code": "E_113", "dep_nom": "Intact", "reg_code": "R_29"}, 
{"tel_code": "E_114", "dep_nom": "Micro-rayure", "reg_code": "R_29"},
{"tel_code": "E_115", "dep_nom": "Rayures", "reg_code": "R_29"}, 
{"tel_code": "E_116", "dep_nom": "Cassé", "reg_code": "R_29"},

{"tel_code": "E_117", "dep_nom": "Intact", "reg_code": "R_30"}, 
{"tel_code": "E_118", "dep_nom": "Micro-rayure", "reg_code": "R_30"},
{"tel_code": "E_119", "dep_nom": "Rayures", "reg_code": "R_30"}, 
{"tel_code": "E_120", "dep_nom": "Cassé", "reg_code": "R_30"},

{"tel_code": "E_121", "dep_nom": "Intact", "reg_code": "R_31"}, 
{"tel_code": "E_122", "dep_nom": "Micro-rayure", "reg_code": "R_31"},
{"tel_code": "E_123", "dep_nom": "Rayures", "reg_code": "R_31"}, 
{"tel_code": "E_124", "dep_nom": "Cassé", "reg_code": "R_31"},

{"tel_code": "E_125", "dep_nom": "Intact", "reg_code": "R_32"}, 
{"tel_code": "E_126", "dep_nom": "Micro-rayure", "reg_code": "R_32"},
{"tel_code": "E_127", "dep_nom": "Rayures", "reg_code": "R_32"}, 
{"tel_code": "E_128", "dep_nom": "Cassé", "reg_code": "R_32"},

{"tel_code": "E_129", "dep_nom": "Intact", "reg_code": "R_33"}, 
{"tel_code": "E_130", "dep_nom": "Micro-rayure", "reg_code": "R_33"},
{"tel_code": "E_131", "dep_nom": "Rayures", "reg_code": "R_33"}, 
{"tel_code": "E_132", "dep_nom": "Cassé", "reg_code": "R_33"},

{"tel_code": "E_133", "dep_nom": "Intact", "reg_code": "R_34"}, 
{"tel_code": "E_134", "dep_nom": "Micro-rayure", "reg_code": "R_34"},
{"tel_code": "E_135", "dep_nom": "Rayures", "reg_code": "R_34"}, 
{"tel_code": "E_136", "dep_nom": "Cassé", "reg_code": "R_34"},

{"tel_code": "E_137", "dep_nom": "Intact", "reg_code": "R_35"}, 
{"tel_code": "E_138", "dep_nom": "Micro-rayure", "reg_code": "R_35"},
{"tel_code": "E_139", "dep_nom": "Rayures", "reg_code": "R_35"}, 
{"tel_code": "E_140", "dep_nom": "Cassé", "reg_code": "R_35"},

{"tel_code": "E_141", "dep_nom": "Intact", "reg_code": "R_36"}, 
{"tel_code": "E_142", "dep_nom": "Micro-rayure", "reg_code": "R_36"},
{"tel_code": "E_143", "dep_nom": "Rayures", "reg_code": "R_36"}, 
{"tel_code": "E_144", "dep_nom": "Cassé", "reg_code": "R_36"},

{"tel_code": "E_145", "dep_nom": "Intact", "reg_code": "R_37"}, 
{"tel_code": "E_146", "dep_nom": "Micro-rayure", "reg_code": "R_37"},
{"tel_code": "E_147", "dep_nom": "Rayures", "reg_code": "R_37"}, 
{"tel_code": "E_148", "dep_nom": "Cassé", "reg_code": "R_37"},

{"tel_code": "E_149", "dep_nom": "Intact", "reg_code": "R_38"}, 
{"tel_code": "E_150", "dep_nom": "Micro-rayure", "reg_code": "R_38"},
{"tel_code": "E_151", "dep_nom": "Rayures", "reg_code": "R_38"}, 
{"tel_code": "E_152", "dep_nom": "Cassé", "reg_code": "R_38"},

{"tel_code": "E_153", "dep_nom": "Intact", "reg_code": "R_39"}, 
{"tel_code": "E_154", "dep_nom": "Micro-rayure", "reg_code": "R_39"},
{"tel_code": "E_155", "dep_nom": "Rayures", "reg_code": "R_39"}, 
{"tel_code": "E_156", "dep_nom": "Cassé", "reg_code": "R_39"},

{"tel_code": "E_157", "dep_nom": "Intact", "reg_code": "R_40"}, 
{"tel_code": "E_158", "dep_nom": "Micro-rayure", "reg_code": "R_40"},
{"tel_code": "E_159", "dep_nom": "Rayures", "reg_code": "R_40"}, 
{"tel_code": "E_160", "dep_nom": "Cassé", "reg_code": "R_40"},

{"tel_code": "E_161", "dep_nom": "Intact", "reg_code": "R_41"}, 
{"tel_code": "E_162", "dep_nom": "Micro-rayure", "reg_code": "R_41"},
{"tel_code": "E_163", "dep_nom": "Rayures", "reg_code": "R_41"}, 
{"tel_code": "E_164", "dep_nom": "Cassé", "reg_code": "R_41"},

{"tel_code": "E_165", "dep_nom": "Intact", "reg_code": "R_42"}, 
{"tel_code": "E_166", "dep_nom": "Micro-rayure", "reg_code": "R_42"},
{"tel_code": "E_167", "dep_nom": "Rayures", "reg_code": "R_42"}, 
{"tel_code": "E_168", "dep_nom": "Cassé", "reg_code": "R_42"},

{"tel_code": "E_169", "dep_nom": "Intact", "reg_code": "R_43"}, 
{"tel_code": "E_170", "dep_nom": "Micro-rayure", "reg_code": "R_43"},
{"tel_code": "E_171", "dep_nom": "Rayures", "reg_code": "R_43"}, 
{"tel_code": "E_172", "dep_nom": "Cassé", "reg_code": "R_43"},

{"tel_code": "E_173", "dep_nom": "Intact", "reg_code": "R_44"}, 
{"tel_code": "E_174", "dep_nom": "Micro-rayure", "reg_code": "R_44"},
{"tel_code": "E_175", "dep_nom": "Rayures", "reg_code": "R_44"}, 
{"tel_code": "E_176", "dep_nom": "Cassé", "reg_code": "R_44"},

{"tel_code": "E_177", "dep_nom": "Intact", "reg_code": "R_45"}, 
{"tel_code": "E_178", "dep_nom": "Micro-rayure", "reg_code": "R_45"},
{"tel_code": "E_179", "dep_nom": "Rayures", "reg_code": "R_45"}, 
{"tel_code": "E_180", "dep_nom": "Cassé", "reg_code": "R_45"},

{"tel_code": "E_181", "dep_nom": "Intact", "reg_code": "R_46"}, 
{"tel_code": "E_182", "dep_nom": "Micro-rayure", "reg_code": "R_46"},
{"tel_code": "E_183", "dep_nom": "Rayures", "reg_code": "R_46"}, 
{"tel_code": "E_184", "dep_nom": "Cassé", "reg_code": "R_46"},

{"tel_code": "E_185", "dep_nom": "Intact", "reg_code": "R_47"}, 
{"tel_code": "E_186", "dep_nom": "Micro-rayure", "reg_code": "R_47"},
{"tel_code": "E_187", "dep_nom": "Rayures", "reg_code": "R_47"}, 
{"tel_code": "E_188", "dep_nom": "Cassé", "reg_code": "R_47"},

{"tel_code": "E_189", "dep_nom": "Intact", "reg_code": "R_48"}, 
{"tel_code": "E_190", "dep_nom": "Micro-rayure", "reg_code": "R_48"},
{"tel_code": "E_191", "dep_nom": "Rayures", "reg_code": "R_48"}, 
{"tel_code": "E_192", "dep_nom": "Cassé", "reg_code": "R_48"},

{"tel_code": "E_193", "dep_nom": "Intact", "reg_code": "R_49"}, 
{"tel_code": "E_194", "dep_nom": "Micro-rayure", "reg_code": "R_49"},
{"tel_code": "E_195", "dep_nom": "Rayures", "reg_code": "R_49"}, 
{"tel_code": "E_196", "dep_nom": "Cassé", "reg_code": "R_49"},

{"tel_code": "E_197", "dep_nom": "Intact", "reg_code": "R_50"}, 
{"tel_code": "E_198", "dep_nom": "Micro-rayure", "reg_code": "R_50"},
{"tel_code": "E_199", "dep_nom": "Rayures", "reg_code": "R_50"}, 
{"tel_code": "E_200", "dep_nom": "Cassé", "reg_code": "R_50"},

{"tel_code": "E_201", "dep_nom": "Intact", "reg_code": "R_51"}, 
{"tel_code": "E_202", "dep_nom": "Micro-rayure", "reg_code": "R_51"},
{"tel_code": "E_203", "dep_nom": "Rayures", "reg_code": "R_51"}, 
{"tel_code": "E_204", "dep_nom": "Cassé", "reg_code": "R_51"},

{"tel_code": "E_205", "dep_nom": "Intact", "reg_code": "R_52"}, 
{"tel_code": "E_206", "dep_nom": "Micro-rayure", "reg_code": "R_52"},
{"tel_code": "E_207", "dep_nom": "Rayures", "reg_code": "R_52"}, 
{"tel_code": "E_208", "dep_nom": "Cassé", "reg_code": "R_52"},

{"tel_code": "E_209", "dep_nom": "Intact", "reg_code": "R_53"}, 
{"tel_code": "E_210", "dep_nom": "Micro-rayure", "reg_code": "R_53"},
{"tel_code": "E_211", "dep_nom": "Rayures", "reg_code": "R_53"}, 
{"tel_code": "E_212", "dep_nom": "Cassé", "reg_code": "R_53"},

{"tel_code": "E_213", "dep_nom": "Intact", "reg_code": "R_54"}, 
{"tel_code": "E_214", "dep_nom": "Micro-rayure", "reg_code": "R_54"},
{"tel_code": "E_215", "dep_nom": "Rayures", "reg_code": "R_54"}, 
{"tel_code": "E_216", "dep_nom": "Cassé", "reg_code": "R_54"},

{"tel_code": "E_217", "dep_nom": "Intact", "reg_code": "R_55"}, 
{"tel_code": "E_218", "dep_nom": "Micro-rayure", "reg_code": "R_55"},
{"tel_code": "E_219", "dep_nom": "Rayures", "reg_code": "R_55"}, 
{"tel_code": "E_220", "dep_nom": "Cassé", "reg_code": "R_55"},

{"tel_code": "E_221", "dep_nom": "Intact", "reg_code": "R_56"}, 
{"tel_code": "E_222", "dep_nom": "Micro-rayure", "reg_code": "R_56"},
{"tel_code": "E_223", "dep_nom": "Rayures", "reg_code": "R_56"}, 
{"tel_code": "E_224", "dep_nom": "Cassé", "reg_code": "R_56"},

{"tel_code": "E_225", "dep_nom": "Intact", "reg_code": "R_57"}, 
{"tel_code": "E_226", "dep_nom": "Micro-rayure", "reg_code": "R_57"},
{"tel_code": "E_227", "dep_nom": "Rayures", "reg_code": "R_57"}, 
{"tel_code": "E_228", "dep_nom": "Cassé", "reg_code": "R_57"},

{"tel_code": "E_229", "dep_nom": "Intact", "reg_code": "R_58"}, 
{"tel_code": "E_230", "dep_nom": "Micro-rayure", "reg_code": "R_58"},
{"tel_code": "E_231", "dep_nom": "Rayures", "reg_code": "R_58"}, 
{"tel_code": "E_232", "dep_nom": "Cassé", "reg_code": "R_58"},

{"tel_code": "E_233", "dep_nom": "Intact", "reg_code": "R_59"}, 
{"tel_code": "E_234", "dep_nom": "Micro-rayure", "reg_code": "R_59"},
{"tel_code": "E_235", "dep_nom": "Rayures", "reg_code": "R_59"}, 
{"tel_code": "E_236", "dep_nom": "Cassé", "reg_code": "R_59"},

{"tel_code": "E_237", "dep_nom": "Intact", "reg_code": "R_60"}, 
{"tel_code": "E_238", "dep_nom": "Micro-rayure", "reg_code": "R_60"},
{"tel_code": "E_239", "dep_nom": "Rayures", "reg_code": "R_60"}, 
{"tel_code": "E_240", "dep_nom": "Cassé", "reg_code": "R_60"},

{"tel_code": "E_241", "dep_nom": "Intact", "reg_code": "R_61"}, 
{"tel_code": "E_242", "dep_nom": "Micro-rayure", "reg_code": "R_61"},
{"tel_code": "E_243", "dep_nom": "Rayures", "reg_code": "R_61"}, 
{"tel_code": "E_244", "dep_nom": "Cassé", "reg_code": "R_61"},

{"tel_code": "E_245", "dep_nom": "Intact", "reg_code": "R_62"}, 
{"tel_code": "E_246", "dep_nom": "Micro-rayure", "reg_code": "R_62"},
{"tel_code": "E_247", "dep_nom": "Rayures", "reg_code": "R_62"}, 
{"tel_code": "E_248", "dep_nom": "Cassé", "reg_code": "R_62"},

{"tel_code": "E_249", "dep_nom": "Intact", "reg_code": "R_63"}, 
{"tel_code": "E_250", "dep_nom": "Micro-rayure", "reg_code": "R_63"},
{"tel_code": "E_251", "dep_nom": "Rayures", "reg_code": "R_63"}, 
{"tel_code": "E_252", "dep_nom": "Cassé", "reg_code": "R_63"},

{"tel_code": "E_253", "dep_nom": "Intact", "reg_code": "R_64"}, 
{"tel_code": "E_254", "dep_nom": "Micro-rayure", "reg_code": "R_64"},
{"tel_code": "E_255", "dep_nom": "Rayures", "reg_code": "R_64"}, 
{"tel_code": "E_256", "dep_nom": "Cassé", "reg_code": "R_64"},

{"tel_code": "E_257", "dep_nom": "Intact", "reg_code": "R_65"}, 
{"tel_code": "E_258", "dep_nom": "Micro-rayure", "reg_code": "R_65"},
{"tel_code": "E_259", "dep_nom": "Rayures", "reg_code": "R_65"}, 
{"tel_code": "E_260", "dep_nom": "Cassé", "reg_code": "R_65"},

{"tel_code": "E_261", "dep_nom": "Intact", "reg_code": "R_66"}, 
{"tel_code": "E_262", "dep_nom": "Micro-rayure", "reg_code": "R_66"},
{"tel_code": "E_263", "dep_nom": "Rayures", "reg_code": "R_66"}, 
{"tel_code": "E_264", "dep_nom": "Cassé", "reg_code": "R_66"},

{"tel_code": "E_265", "dep_nom": "Intact", "reg_code": "R_67"}, 
{"tel_code": "E_266", "dep_nom": "Micro-rayure", "reg_code": "R_67"},
{"tel_code": "E_267", "dep_nom": "Rayures", "reg_code": "R_67"}, 
{"tel_code": "E_268", "dep_nom": "Cassé", "reg_code": "R_67"},

{"tel_code": "E_269", "dep_nom": "Intact", "reg_code": "R_68"}, 
{"tel_code": "E_270", "dep_nom": "Micro-rayure", "reg_code": "R_68"},
{"tel_code": "E_271", "dep_nom": "Rayures", "reg_code": "R_68"}, 
{"tel_code": "E_272", "dep_nom": "Cassé", "reg_code": "R_68"},

{"tel_code": "E_273", "dep_nom": "Intact", "reg_code": "R_69"}, 
{"tel_code": "E_274", "dep_nom": "Micro-rayure", "reg_code": "R_69"},
{"tel_code": "E_275", "dep_nom": "Rayures", "reg_code": "R_69"}, 
{"tel_code": "E_276", "dep_nom": "Cassé", "reg_code": "R_69"},

{"tel_code": "E_277", "dep_nom": "Intact", "reg_code": "R_70"}, 
{"tel_code": "E_278", "dep_nom": "Micro-rayure", "reg_code": "R_70"},
{"tel_code": "E_279", "dep_nom": "Rayures", "reg_code": "R_70"}, 
{"tel_code": "E_280", "dep_nom": "Cassé", "reg_code": "R_70"},

{"tel_code": "E_281", "dep_nom": "Intact", "reg_code": "R_71"}, 
{"tel_code": "E_282", "dep_nom": "Micro-rayure", "reg_code": "R_71"},
{"tel_code": "E_283", "dep_nom": "Rayures", "reg_code": "R_71"}, 
{"tel_code": "E_284", "dep_nom": "Cassé", "reg_code": "R_71"},

{"tel_code": "E_285", "dep_nom": "Intact", "reg_code": "R_72"}, 
{"tel_code": "E_286", "dep_nom": "Micro-rayure", "reg_code": "R_72"},
{"tel_code": "E_287", "dep_nom": "Rayures", "reg_code": "R_72"}, 
{"tel_code": "E_288", "dep_nom": "Cassé", "reg_code": "R_72"},

{"tel_code": "E_289", "dep_nom": "Intact", "reg_code": "R_73"}, 
{"tel_code": "E_290", "dep_nom": "Micro-rayure", "reg_code": "R_73"},
{"tel_code": "E_291", "dep_nom": "Rayures", "reg_code": "R_73"}, 
{"tel_code": "E_292", "dep_nom": "Cassé", "reg_code": "R_73"},

{"tel_code": "E_293", "dep_nom": "Intact", "reg_code": "R_74"}, 
{"tel_code": "E_294", "dep_nom": "Micro-rayure", "reg_code": "R_74"},
{"tel_code": "E_295", "dep_nom": "Rayures", "reg_code": "R_74"}, 
{"tel_code": "E_296", "dep_nom": "Cassé", "reg_code": "R_74"},

{"tel_code": "E_297", "dep_nom": "Intact", "reg_code": "R_75"}, 
{"tel_code": "E_298", "dep_nom": "Micro-rayure", "reg_code": "R_75"},
{"tel_code": "E_299", "dep_nom": "Rayures", "reg_code": "R_75"}, 
{"tel_code": "E_300", "dep_nom": "Cassé", "reg_code": "R_75"},

{"tel_code": "E_301", "dep_nom": "Intact", "reg_code": "R_76"}, 
{"tel_code": "E_302", "dep_nom": "Micro-rayure", "reg_code": "R_76"},
{"tel_code": "E_303", "dep_nom": "Rayures", "reg_code": "R_76"}, 
{"tel_code": "E_304", "dep_nom": "Cassé", "reg_code": "R_76"},

{"tel_code": "E_305", "dep_nom": "Intact", "reg_code": "R_77"}, 
{"tel_code": "E_306", "dep_nom": "Micro-rayure", "reg_code": "R_77"},
{"tel_code": "E_307", "dep_nom": "Rayures", "reg_code": "R_77"}, 
{"tel_code": "E_308", "dep_nom": "Cassé", "reg_code": "R_77"},

{"tel_code": "E_309", "dep_nom": "Intact", "reg_code": "R_78"}, 
{"tel_code": "E_310", "dep_nom": "Micro-rayure", "reg_code": "R_78"},
{"tel_code": "E_311", "dep_nom": "Rayures", "reg_code": "R_78"}, 
{"tel_code": "E_312", "dep_nom": "Cassé", "reg_code": "R_78"},

{"tel_code": "E_313", "dep_nom": "Intact", "reg_code": "R_79"}, 
{"tel_code": "E_314", "dep_nom": "Micro-rayure", "reg_code": "R_79"},
{"tel_code": "E_315", "dep_nom": "Rayures", "reg_code": "R_79"}, 
{"tel_code": "E_316", "dep_nom": "Cassé", "reg_code": "R_79"},

{"tel_code": "E_317", "dep_nom": "Intact", "reg_code": "R_80"}, 
{"tel_code": "E_318", "dep_nom": "Micro-rayure", "reg_code": "R_80"},
{"tel_code": "E_319", "dep_nom": "Rayures", "reg_code": "R_80"}, 
{"tel_code": "E_320", "dep_nom": "Cassé", "reg_code": "R_80"},

{"tel_code": "E_321", "dep_nom": "Intact", "reg_code": "R_81"}, 
{"tel_code": "E_322", "dep_nom": "Micro-rayure", "reg_code": "R_81"},
{"tel_code": "E_323", "dep_nom": "Rayures", "reg_code": "R_81"}, 
{"tel_code": "E_324", "dep_nom": "Cassé", "reg_code": "R_81"},

{"tel_code": "E_325", "dep_nom": "Intact", "reg_code": "R_82"}, 
{"tel_code": "E_326", "dep_nom": "Micro-rayure", "reg_code": "R_82"},
{"tel_code": "E_327", "dep_nom": "Rayures", "reg_code": "R_82"}, 
{"tel_code": "E_328", "dep_nom": "Cassé", "reg_code": "R_82"},

{"tel_code": "E_329", "dep_nom": "Intact", "reg_code": "R_83"}, 
{"tel_code": "E_330", "dep_nom": "Micro-rayure", "reg_code": "R_83"},
{"tel_code": "E_331", "dep_nom": "Rayures", "reg_code": "R_83"}, 
{"tel_code": "E_332", "dep_nom": "Cassé", "reg_code": "R_83"},

{"tel_code": "E_333", "dep_nom": "Intact", "reg_code": "R_84"}, 
{"tel_code": "E_334", "dep_nom": "Micro-rayure", "reg_code": "R_84"},
{"tel_code": "E_335", "dep_nom": "Rayures", "reg_code": "R_84"}, 
{"tel_code": "E_336", "dep_nom": "Cassé", "reg_code": "R_84"},

{"tel_code": "E_337", "dep_nom": "Intact", "reg_code": "R_85"}, 
{"tel_code": "E_338", "dep_nom": "Micro-rayure", "reg_code": "R_85"},
{"tel_code": "E_339", "dep_nom": "Rayures", "reg_code": "R_85"}, 
{"tel_code": "E_340", "dep_nom": "Cassé", "reg_code": "R_85"},

{"tel_code": "E_341", "dep_nom": "Intact", "reg_code": "R_86"}, 
{"tel_code": "E_342", "dep_nom": "Micro-rayure", "reg_code": "R_86"},
{"tel_code": "E_343", "dep_nom": "Rayures", "reg_code": "R_86"}, 
{"tel_code": "E_344", "dep_nom": "Cassé", "reg_code": "R_86"},

{"tel_code": "E_345", "dep_nom": "Intact", "reg_code": "R_87"}, 
{"tel_code": "E_346", "dep_nom": "Micro-rayure", "reg_code": "R_87"},
{"tel_code": "E_347", "dep_nom": "Rayures", "reg_code": "R_87"}, 
{"tel_code": "E_348", "dep_nom": "Cassé", "reg_code": "R_87"},

{"tel_code": "E_349", "dep_nom": "Intact", "reg_code": "R_88"}, 
{"tel_code": "E_350", "dep_nom": "Micro-rayure", "reg_code": "R_88"},
{"tel_code": "E_351", "dep_nom": "Rayures", "reg_code": "R_88"}, 
{"tel_code": "E_352", "dep_nom": "Cassé", "reg_code": "R_88"},

{"tel_code": "E_353", "dep_nom": "Intact", "reg_code": "R_89"}, 
{"tel_code": "E_354", "dep_nom": "Micro-rayure", "reg_code": "R_89"},
{"tel_code": "E_355", "dep_nom": "Rayures", "reg_code": "R_89"}, 
{"tel_code": "E_356", "dep_nom": "Cassé", "reg_code": "R_89"},

{"tel_code": "E_357", "dep_nom": "Intact", "reg_code": "R_90"}, 
{"tel_code": "E_358", "dep_nom": "Micro-rayure", "reg_code": "R_90"},
{"tel_code": "E_359", "dep_nom": "Rayures", "reg_code": "R_90"}, 
{"tel_code": "E_360", "dep_nom": "Cassé", "reg_code": "R_90"},

{"tel_code": "E_361", "dep_nom": "Intact", "reg_code": "R_91"}, 
{"tel_code": "E_362", "dep_nom": "Micro-rayure", "reg_code": "R_91"},
{"tel_code": "E_363", "dep_nom": "Rayures", "reg_code": "R_91"}, 
{"tel_code": "E_364", "dep_nom": "Cassé", "reg_code": "R_91"},

{"tel_code": "E_365", "dep_nom": "Intact", "reg_code": "R_92"}, 
{"tel_code": "E_366", "dep_nom": "Micro-rayure", "reg_code": "R_92"},
{"tel_code": "E_367", "dep_nom": "Rayures", "reg_code": "R_92"}, 
{"tel_code": "E_368", "dep_nom": "Cassé", "reg_code": "R_92"},

{"tel_code": "E_369", "dep_nom": "Intact", "reg_code": "R_93"}, 
{"tel_code": "E_370", "dep_nom": "Micro-rayure", "reg_code": "R_93"},
{"tel_code": "E_371", "dep_nom": "Rayures", "reg_code": "R_93"}, 
{"tel_code": "E_372", "dep_nom": "Cassé", "reg_code": "R_93"},

{"tel_code": "E_373", "dep_nom": "Intact", "reg_code": "R_94"}, 
{"tel_code": "E_374", "dep_nom": "Micro-rayure", "reg_code": "R_94"},
{"tel_code": "E_375", "dep_nom": "Rayures", "reg_code": "R_94"}, 
{"tel_code": "E_376", "dep_nom": "Cassé", "reg_code": "R_94"},

{"tel_code": "E_377", "dep_nom": "Intact", "reg_code": "R_95"}, 
{"tel_code": "E_378", "dep_nom": "Micro-rayure", "reg_code": "R_95"},
{"tel_code": "E_379", "dep_nom": "Rayures", "reg_code": "R_95"}, 
{"tel_code": "E_380", "dep_nom": "Cassé", "reg_code": "R_95"},

{"tel_code": "E_381", "dep_nom": "Intact", "reg_code": "R_96"}, 
{"tel_code": "E_382", "dep_nom": "Micro-rayure", "reg_code": "R_96"},
{"tel_code": "E_383", "dep_nom": "Rayures", "reg_code": "R_96"}, 
{"tel_code": "E_384", "dep_nom": "Cassé", "reg_code": "R_96"},

{"tel_code": "E_385", "dep_nom": "Intact", "reg_code": "R_97"}, 
{"tel_code": "E_386", "dep_nom": "Micro-rayure", "reg_code": "R_97"},
{"tel_code": "E_387", "dep_nom": "Rayures", "reg_code": "R_97"}, 
{"tel_code": "E_388", "dep_nom": "Cassé", "reg_code": "R_97"},

{"tel_code": "E_389", "dep_nom": "Intact", "reg_code": "R_98"}, 
{"tel_code": "E_390", "dep_nom": "Micro-rayure", "reg_code": "R_98"},
{"tel_code": "E_391", "dep_nom": "Rayures", "reg_code": "R_98"}, 
{"tel_code": "E_392", "dep_nom": "Cassé", "reg_code": "R_98"},

{"tel_code": "E_393", "dep_nom": "Intact", "reg_code": "R_99"}, 
{"tel_code": "E_394", "dep_nom": "Micro-rayure", "reg_code": "R_99"},
{"tel_code": "E_395", "dep_nom": "Rayures", "reg_code": "R_99"}, 
{"tel_code": "E_396", "dep_nom": "Cassé", "reg_code": "R_99"},

{"tel_code": "E_397", "dep_nom": "Intact", "reg_code": "R_100"}, 
{"tel_code": "E_398", "dep_nom": "Micro-rayure", "reg_code": "R_100"},
{"tel_code": "E_399", "dep_nom": "Rayures", "reg_code": "R_100"}, 
{"tel_code": "E_400", "dep_nom": "Cassé", "reg_code": "R_100"},

{"tel_code": "E_401", "dep_nom": "Intact", "reg_code": "R_101"}, 
{"tel_code": "E_402", "dep_nom": "Micro-rayure", "reg_code": "R_101"},
{"tel_code": "E_403", "dep_nom": "Rayures", "reg_code": "R_101"}, 
{"tel_code": "E_404", "dep_nom": "Cassé", "reg_code": "R_101"},

{"tel_code": "E_405", "dep_nom": "Intact", "reg_code": "R_102"}, 
{"tel_code": "E_406", "dep_nom": "Micro-rayure", "reg_code":" R_102"},
{"tel_code": "E_407", "dep_nom": "Rayures", "reg_code": "R_102"}, 
{"tel_code": "E_408", "dep_nom": "Cassé", "reg_code": "R_102"},

{"tel_code": "E_409", "dep_nom": "Intact", "reg_code": "R_103"}, 
{"tel_code": "E_410", "dep_nom": "Micro-rayure", "reg_code": "R_103"},
{"tel_code": "E_411", "dep_nom": "Rayures", "reg_code": "R_103"}, 
{"tel_code": "E_412", "dep_nom": "Cassé", "reg_code": "R_103"},

{"tel_code": "E_413", "dep_nom": "Intact", "reg_code": "R_104"}, 
{"tel_code": "E_414", "dep_nom": "Micro-rayure", "reg_code": "R_104"},
{"tel_code": "E_415", "dep_nom": "Rayures", "reg_code": "R_104"}, 
{"tel_code": "E_416", "dep_nom": "Cassé", "reg_code": "R_104"},

{"tel_code": "E_417", "dep_nom": "Intact", "reg_code": "R_105"}, 
{"tel_code": "E_418", "dep_nom": "Micro-rayure", "reg_code": "R_105"},
{"tel_code": "E_419", "dep_nom": "Rayures", "reg_code": "R_105"}, 
{"tel_code": "E_420", "dep_nom": "Cassé", "reg_code": "R_105"},

{"tel_code": "E_421", "dep_nom": "Intact", "reg_code": "R_106"}, 
{"tel_code": "E_422", "dep_nom": "Micro-rayure", "reg_code": "R_106"},
{"tel_code": "E_423", "dep_nom": "Rayures", "reg_code": "R_106"}, 
{"tel_code": "E_424", "dep_nom": "Cassé", "reg_code": "R_106"},

{"tel_code": "E_425", "dep_nom": "Intact", "reg_code": "R_107"}, 
{"tel_code": "E_426", "dep_nom": "Micro-rayure", "reg_code": "R_107"},
{"tel_code": "E_427", "dep_nom": "Rayures", "reg_code": "R_107"}, 
{"tel_code": "E_428", "dep_nom": "Cassé", "reg_code": "R_107"},

{"tel_code": "E_429", "dep_nom": "Intact", "reg_code": "R_108"}, 
{"tel_code": "E_430", "dep_nom": "Micro-rayure", "reg_code": "R_108"},
{"tel_code": "E_431", "dep_nom": "Rayures", "reg_code": "R_108"}, 
{"tel_code": "E_432", "dep_nom": "Cassé", "reg_code": "R_108"},

{"tel_code": "E_433", "dep_nom": "Intact", "reg_code": "R_109"}, 
{"tel_code": "E_434", "dep_nom": "Micro-rayure", "reg_code": "R_109"},
{"tel_code": "E_435", "dep_nom": "Rayures", "reg_code": "R_109"}, 
{"tel_code": "E_436", "dep_nom": "Cassé", "reg_code": "R_109"},

{"tel_code": "E_437", "dep_nom": "Intact", "reg_code": "R_110"}, 
{"tel_code": "E_438", "dep_nom": "Micro-rayure", "reg_code": "R_110"},
{"tel_code": "E_439", "dep_nom": "Rayures", "reg_code": "R_110"}, 
{"tel_code": "E_440", "dep_nom": "Cassé", "reg_code": "R_110"},

{"tel_code": "E_441", "dep_nom": "Intact", "reg_code": "R_111"}, 
{"tel_code": "E_442", "dep_nom": "Micro-rayure", "reg_code": "R_111"},
{"tel_code": "E_443", "dep_nom": "Rayures", "reg_code": "R_111"}, 
{"tel_code": "E_444", "dep_nom": "Cassé", "reg_code": "R_111"},

{"tel_code": "E_445", "dep_nom": "Intact", "reg_code": "R_112"}, 
{"tel_code": "E_446", "dep_nom": "Micro-rayure", "reg_code": "R_112"},
{"tel_code": "E_447", "dep_nom": "Rayures", "reg_code": "R_112"}, 
{"tel_code": "E_448", "dep_nom": "Cassé", "reg_code": "R_112"},

{"tel_code": "E_449", "dep_nom": "Intact", "reg_code": "R_113"}, 
{"tel_code": "E_450", "dep_nom": "Micro-rayure", "reg_code": "R_113"},
{"tel_code": "E_451", "dep_nom": "Rayures", "reg_code": "R_113"}, 
{"tel_code": "E_452", "dep_nom": "Cassé", "reg_code": "R_113"},

{"tel_code": "E_453", "dep_nom": "Intact", "reg_code": "R_114"}, 
{"tel_code": "E_454", "dep_nom": "Micro-rayure", "reg_code": "R_114"},
{"tel_code": "E_455", "dep_nom": "Rayures", "reg_code": "R_114"}, 
{"tel_code": "E_456", "dep_nom": "Cassé", "reg_code": "R_114"},

{"tel_code": "E_457", "dep_nom": "Intact", "reg_code": "R_115"}, 
{"tel_code": "E_458", "dep_nom": "Micro-rayure", "reg_code": "R_115"},
{"tel_code": "E_459", "dep_nom": "Rayures", "reg_code": "R_115"}, 
{"tel_code": "E_460", "dep_nom": "Cassé", "reg_code": "R_115"},

{"tel_code": "E_461", "dep_nom": "Intact", "reg_code": "R_116"}, 
{"tel_code": "E_462", "dep_nom": "Micro-rayure", "reg_code": "R_116"},
{"tel_code": "E_463", "dep_nom": "Rayures", "reg_code": "R_116"}, 
{"tel_code": "E_464", "dep_nom": "Cassé", "reg_code": "R_116"},

{"tel_code": "E_465", "dep_nom": "Intact", "reg_code": "R_117"}, 
{"tel_code": "E_466", "dep_nom": "Micro-rayure", "reg_code": "R_117"},
{"tel_code": "E_467", "dep_nom": "Rayures", "reg_code": "R_117"}, 
{"tel_code": "E_468", "dep_nom": "Cassé", "reg_code": "R_117"},

{"tel_code": "E_469", "dep_nom": "Intact", "reg_code": "R_118"}, 
{"tel_code": "E_470", "dep_nom": "Micro-rayure", "reg_code": "R_118"},
{"tel_code": "E_471", "dep_nom": "Rayures", "reg_code": "R_118"}, 
{"tel_code": "E_472", "dep_nom": "Cassé", "reg_code": "R_118"},

{"tel_code": "E_473", "dep_nom": "Intact", "reg_code": "R_119"}, 
{"tel_code": "E_474", "dep_nom": "Micro-rayure", "reg_code": "R_119"},
{"tel_code": "E_475", "dep_nom": "Rayures", "reg_code": "R_119"}, 
{"tel_code": "E_476", "dep_nom": "Cassé", "reg_code": "R_119"},

{"tel_code": "E_477", "dep_nom": "Intact", "reg_code": "R_120"}, 
{"tel_code": "E_478", "dep_nom": "Micro-rayure", "reg_code": "R_120"},
{"tel_code": "E_479", "dep_nom": "Rayures", "reg_code": "R_120"}, 
{"tel_code": "E_480", "dep_nom": "Cassé", "reg_code": "R_120"},

{"tel_code": "E_481", "dep_nom": "Intact", "reg_code": "R_121"}, 
{"tel_code": "E_482", "dep_nom": "Micro-rayure", "reg_code": "R_121"},
{"tel_code": "E_483", "dep_nom": "Rayures", "reg_code": "R_121"}, 
{"tel_code": "E_484", "dep_nom": "Cassé", "reg_code": "R_121"},

{"tel_code": "E_485", "dep_nom": "Intact", "reg_code": "R_122"}, 
{"tel_code": "E_486", "dep_nom": "Micro-rayure", "reg_code": "R_122"},
{"tel_code": "E_487", "dep_nom": "Rayures", "reg_code": "R_122"}, 
{"tel_code": "E_488", "dep_nom": "Cassé", "reg_code": "R_122"},

{"tel_code": "E_489", "dep_nom": "Intact", "reg_code": "R_123"}, 
{"tel_code": "E_490", "dep_nom": "Micro-rayure", "reg_code": "R_123"},
{"tel_code": "E_491", "dep_nom": "Rayures", "reg_code": "R_123"}, 
{"tel_code": "E_492", "dep_nom": "Cassé", "reg_code": "R_123"},

{"tel_code": "E_493", "dep_nom": "Intact", "reg_code": "R_124"}, 
{"tel_code": "E_494", "dep_nom": "Micro-rayure", "reg_code": "R_124"},
{"tel_code": "E_495", "dep_nom": "Rayures", "reg_code": "R_124"}, 
{"tel_code": "E_496", "dep_nom": "Cassé", "reg_code": "R_124"},

{"tel_code": "E_497", "dep_nom": "Intact", "reg_code": "R_125"}, 
{"tel_code": "E_498", "dep_nom": "Micro-rayure", "reg_code": "R_125"},
{"tel_code": "E_499", "dep_nom": "Rayures", "reg_code": "R_125"}, 
{"tel_code": "E_500", "dep_nom": "Cassé", "reg_code": "R_125"},

{"tel_code": "E_501", "dep_nom": "Intact", "reg_code": "R_126"}, 
{"tel_code": "E_502", "dep_nom": "Micro-rayure", "reg_code": "R_126"},
{"tel_code": "E_503", "dep_nom": "Rayures", "reg_code": "R_126"}, 
{"tel_code": "E_504", "dep_nom": "Cassé", "reg_code": "R_126"},

{"tel_code": "E_505", "dep_nom": "Intact", "reg_code": "R_127"}, 
{"tel_code": "E_506", "dep_nom": "Micro-rayure", "reg_code": "R_127"},
{"tel_code": "E_507", "dep_nom": "Rayures", "reg_code": "R_127"}, 
{"tel_code": "E_508", "dep_nom": "Cassé", "reg_code": "R_127"},

{"tel_code": "E_509", "dep_nom": "Intact", "reg_code": "R_128"}, 
{"tel_code": "E_510", "dep_nom": "Micro-rayure", "reg_code": "R_128"},
{"tel_code": "E_511", "dep_nom": "Rayures", "reg_code": "R_128"}, 
{"tel_code": "E_512", "dep_nom": "Cassé", "reg_code": "R_128"},

{"tel_code": "E_513", "dep_nom": "Intact", "reg_code": "R_129"}, 
{"tel_code": "E_514", "dep_nom": "Micro-rayure", "reg_code": "R_129"},
{"tel_code": "E_515", "dep_nom": "Rayures", "reg_code": "R_129"}, 
{"tel_code": "E_516", "dep_nom": "Cassé", "reg_code": "R_129"},

{"tel_code": "E_517", "dep_nom": "Intact", "reg_code": "R_130"}, 
{"tel_code": "E_518", "dep_nom": "Micro-rayure", "reg_code": "R_130"},
{"tel_code": "E_519", "dep_nom": "Rayures", "reg_code": "R_130"}, 
{"tel_code": "E_520", "dep_nom": "Cassé", "reg_code": "R_130"},

{"tel_code": "E_521", "dep_nom": "Intact", "reg_code": "R_131"}, 
{"tel_code": "E_522", "dep_nom": "Micro-rayure", "reg_code": "R_131"},
{"tel_code": "E_523", "dep_nom": "Rayures", "reg_code": "R_131"}, 
{"tel_code": "E_524", "dep_nom": "Cassé", "reg_code": "R_131"},

{"tel_code": "E_525", "dep_nom": "Intact", "reg_code": "R_132"}, 
{"tel_code": "E_526", "dep_nom": "Micro-rayure", "reg_code": "R_132"},
{"tel_code": "E_527", "dep_nom": "Rayures", "reg_code": "R_132"}, 
{"tel_code": "E_528", "dep_nom": "Cassé", "reg_code": "R_132"},

{"tel_code": "E_529", "dep_nom": "Intact", "reg_code": "R_133"}, 
{"tel_code": "E_530", "dep_nom": "Micro-rayure", "reg_code": "R_133"},
{"tel_code": "E_531", "dep_nom": "Rayures", "reg_code": "R_133"}, 
{"tel_code": "E_532", "dep_nom": "Cassé", "reg_code": "R_133"},

{"tel_code": "E_533", "dep_nom": "Intact", "reg_code": "R_134"}, 
{"tel_code": "E_534", "dep_nom": "Micro-rayure", "reg_code": "R_134"},
{"tel_code": "E_535", "dep_nom": "Rayures", "reg_code": "R_134"}, 
{"tel_code": "E_536", "dep_nom": "Cassé", "reg_code": "R_134"},

{"tel_code": "E_537", "dep_nom": "Intact", "reg_code": "R_135"}, 
{"tel_code": "E_538", "dep_nom": "Micro-rayure", "reg_code": "R_135"},
{"tel_code": "E_539", "dep_nom": "Rayures", "reg_code": "R_135"}, 
{"tel_code": "E_540", "dep_nom": "Cassé", "reg_code": "R_135"},

{"tel_code": "E_541", "dep_nom": "Intact", "reg_code": "R_136"}, 
{"tel_code": "E_542", "dep_nom": "Micro-rayure", "reg_code": "R_136"},
{"tel_code": "E_543", "dep_nom": "Rayures", "reg_code": "R_136"}, 
{"tel_code": "E_544", "dep_nom": "Cassé", "reg_code": "R_136"},

{"tel_code": "E_545", "dep_nom": "Intact", "reg_code": "R_137"}, 
{"tel_code": "E_546", "dep_nom": "Micro-rayure", "reg_code": "R_137"},
{"tel_code": "E_547", "dep_nom": "Rayures", "reg_code": "R_137"}, 
{"tel_code": "E_548", "dep_nom": "Cassé", "reg_code": "R_137"},

{"tel_code": "E_549", "dep_nom": "Intact", "reg_code": "R_138"}, 
{"tel_code": "E_550", "dep_nom": "Micro-rayure", "reg_code": "R_138"},
{"tel_code": "E_551", "dep_nom": "Rayures", "reg_code": "R_138"}, 
{"tel_code": "E_552", "dep_nom": "Cassé", "reg_code": "R_138"},

{"tel_code": "E_553", "dep_nom": "Intact", "reg_code": "R_139"}, 
{"tel_code": "E_554", "dep_nom": "Micro-rayure", "reg_code": "R_139"},
{"tel_code": "E_555", "dep_nom": "Rayures", "reg_code": "R_139"}, 
{"tel_code": "E_556", "dep_nom": "Cassé", "reg_code": "R_139"},

{"tel_code": "E_557", "dep_nom": "Intact", "reg_code": "R_140"}, 
{"tel_code": "E_558", "dep_nom": "Micro-rayure", "reg_code": "R_140"},
{"tel_code": "E_559", "dep_nom": "Rayures", "reg_code": "R_140"}, 
{"tel_code": "E_560", "dep_nom": "Cassé", "reg_code": "R_140"},

{"tel_code": "E_561", "dep_nom": "Intact", "reg_code": "R_141"}, 
{"tel_code": "E_562", "dep_nom": "Micro-rayure", "reg_code": "R_141"},
{"tel_code": "E_563", "dep_nom": "Rayures", "reg_code": "R_141"}, 
{"tel_code": "E_564", "dep_nom": "Cassé", "reg_code": "R_141"},

{"tel_code": "E_565", "dep_nom": "Intact", "reg_code": "R_142"}, 
{"tel_code": "E_566", "dep_nom": "Micro-rayure", "reg_code": "R_142"},
{"tel_code": "E_567", "dep_nom": "Rayures", "reg_code": "R_142"}, 
{"tel_code": "E_568", "dep_nom": "Cassé", "reg_code": "R_142"},

{"tel_code": "E_569", "dep_nom": "Intact", "reg_code": "R_143"}, 
{"tel_code": "E_570", "dep_nom": "Micro-rayure", "reg_code": "R_143"},
{"tel_code": "E_571", "dep_nom": "Rayures", "reg_code": "R_143"}, 
{"tel_code": "E_572", "dep_nom": "Cassé", "reg_code": "R_143"},

{"tel_code": "E_573", "dep_nom": "Intact", "reg_code": "R_144"}, 
{"tel_code": "E_574", "dep_nom": "Micro-rayure", "reg_code": "R_144"},
{"tel_code": "E_575", "dep_nom": "Rayures", "reg_code": "R_144"}, 
{"tel_code": "E_576", "dep_nom": "Cassé", "reg_code": "R_144"},

{"tel_code": "E_577", "dep_nom": "Intact", "reg_code": "R_145"}, 
{"tel_code": "E_578", "dep_nom": "Micro-rayure", "reg_code": "R_145"},
{"tel_code": "E_579", "dep_nom": "Rayures", "reg_code": "R_145"}, 
{"tel_code": "E_580", "dep_nom": "Cassé", "reg_code": "R_145"},

{"tel_code": "E_581", "dep_nom": "Intact", "reg_code": "R_146"}, 
{"tel_code": "E_582", "dep_nom": "Micro-rayure", "reg_code": "R_146"},
{"tel_code": "E_583", "dep_nom": "Rayures", "reg_code": "R_146"}, 
{"tel_code": "E_584", "dep_nom": "Cassé", "reg_code": "R_146"},

{"tel_code": "E_585", "dep_nom": "Intact", "reg_code": "R_147"}, 
{"tel_code": "E_586", "dep_nom": "Micro-rayure", "reg_code": "R_147"},
{"tel_code": "E_587", "dep_nom": "Rayures", "reg_code": "R_147"}, 
{"tel_code": "E_588", "dep_nom": "Cassé", "reg_code": "R_147"},

{"tel_code": "E_589", "dep_nom": "Intact", "reg_code": "R_148"}, 
{"tel_code": "E_590", "dep_nom": "Micro-rayure", "reg_code": "R_148"},
{"tel_code": "E_591", "dep_nom": "Rayures", "reg_code": "R_148"}, 
{"tel_code": "E_592", "dep_nom": "Cassé", "reg_code": "R_148"},

{"tel_code": "E_593", "dep_nom": "Intact", "reg_code": "R_149"}, 
{"tel_code": "E_594", "dep_nom": "Micro-rayure", "reg_code": "R_149"},
{"tel_code": "E_595", "dep_nom": "Rayures", "reg_code": "R_149"}, 
{"tel_code": "E_596", "dep_nom": "Cassé", "reg_code": "R_149"},

{"tel_code": "E_597", "dep_nom": "Intact", "reg_code": "R_150"}, 
{"tel_code": "E_598", "dep_nom": "Micro-rayure", "reg_code": "R_150"},
{"tel_code": "E_599", "dep_nom": "Rayures", "reg_code": "R_150"}, 
{"tel_code": "E_600", "dep_nom": "Cassé", "reg_code": "R_150"},

{"tel_code": "E_601", "dep_nom": "Intact", "reg_code": "R_151"}, 
{"tel_code": "E_602", "dep_nom": "Micro-rayure", "reg_code": "R_151"},
{"tel_code": "E_603", "dep_nom": "Rayures", "reg_code": "R_151"}, 
{"tel_code": "E_604", "dep_nom": "Cassé", "reg_code": "R_151"},

{"tel_code": "E_605", "dep_nom": "Intact", "reg_code": "R_152"}, 
{"tel_code": "E_606", "dep_nom": "Micro-rayure", "reg_code": "R_152"},
{"tel_code": "E_607", "dep_nom": "Rayures", "reg_code": "R_152"}, 
{"tel_code": "E_608", "dep_nom": "Cassé", "reg_code": "R_152"},

{"tel_code": "E_609", "dep_nom": "Intact", "reg_code": "R_153"}, 
{"tel_code": "E_610", "dep_nom": "Micro-rayure", "reg_code": "R_153"},
{"tel_code": "E_611", "dep_nom": "Rayures", "reg_code": "R_153"}, 
{"tel_code": "E_612", "dep_nom": "Cassé", "reg_code": "R_153"},

{"tel_code": "E_613", "dep_nom": "Intact", "reg_code": "R_154"}, 
{"tel_code": "E_614", "dep_nom": "Micro-rayure", "reg_code": "R_154"},
{"tel_code": "E_615", "dep_nom": "Rayures", "reg_code": "R_154"}, 
{"tel_code": "E_616", "dep_nom": "Cassé", "reg_code": "R_154"},

{"tel_code": "E_617", "dep_nom": "Intact", "reg_code": "R_155"}, 
{"tel_code": "E_618", "dep_nom": "Micro-rayure", "reg_code": "R_155"},
{"tel_code": "E_619", "dep_nom": "Rayures", "reg_code": "R_155"}, 
{"tel_code": "E_620", "dep_nom": "Cassé", "reg_code": "R_155"},

{"tel_code": "E_621", "dep_nom": "Intact", "reg_code": "R_156"}, 
{"tel_code": "E_622", "dep_nom": "Micro-rayure", "reg_code": "R_156"},
{"tel_code": "E_623", "dep_nom": "Rayures", "reg_code": "R_156"}, 
{"tel_code": "E_624", "dep_nom": "Cassé", "reg_code": "R_156"},

{"tel_code": "E_625", "dep_nom": "Intact", "reg_code": "R_157"}, 
{"tel_code": "E_626", "dep_nom": "Micro-rayure", "reg_code": "R_157"},
{"tel_code": "E_627", "dep_nom": "Rayures", "reg_code": "R_157"}, 
{"tel_code": "E_628", "dep_nom": "Cassé", "reg_code": "R_157"},

{"tel_code": "E_629", "dep_nom": "Intact", "reg_code": "R_158"}, 
{"tel_code": "E_630", "dep_nom": "Micro-rayure", "reg_code": "R_158"},
{"tel_code": "E_631", "dep_nom": "Rayures", "reg_code": "R_158"}, 
{"tel_code": "E_632", "dep_nom": "Cassé", "reg_code": "R_158"},

{"tel_code": "E_633", "dep_nom": "Intact", "reg_code": "R_159"}, 
{"tel_code": "E_634", "dep_nom": "Micro-rayure", "reg_code": "R_159"},
{"tel_code": "E_635", "dep_nom": "Rayures", "reg_code": "R_159"}, 
{"tel_code": "E_636", "dep_nom": "Cassé", "reg_code": "R_159"},

{"tel_code": "E_637", "dep_nom": "Intact", "reg_code": "R_160"}, 
{"tel_code": "E_638", "dep_nom": "Micro-rayure", "reg_code": "R_160"},
{"tel_code": "E_639", "dep_nom": "Rayures", "reg_code": "R_160"}, 
{"tel_code": "E_640", "dep_nom": "Cassé", "reg_code": "R_160"},

{"tel_code": "E_641", "dep_nom": "Intact", "reg_code": "R_161"}, 
{"tel_code": "E_642", "dep_nom": "Micro-rayure", "reg_code": "R_161"},
{"tel_code": "E_643", "dep_nom": "Rayures", "reg_code": "R_161"}, 
{"tel_code": "E_644", "dep_nom": "Cassé", "reg_code": "R_161"},

{"tel_code": "E_645", "dep_nom": "Intact", "reg_code": "R_162"}, 
{"tel_code": "E_646", "dep_nom": "Micro-rayure", "reg_code": "R_162"},
{"tel_code": "E_647", "dep_nom": "Rayures", "reg_code": "R_162"}, 
{"tel_code": "E_648", "dep_nom": "Cassé", "reg_code": "R_162"},

{"tel_code": "E_649", "dep_nom": "Intact", "reg_code": "R_163"}, 
{"tel_code": "E_650", "dep_nom": "Micro-rayure", "reg_code": "R_163"},
{"tel_code": "E_651", "dep_nom": "Rayures", "reg_code": "R_163"}, 
{"tel_code": "E_652", "dep_nom": "Cassé", "reg_code": "R_163"},

{"tel_code": "E_653", "dep_nom": "Intact", "reg_code": "R_164"}, 
{"tel_code": "E_654", "dep_nom": "Micro-rayure", "reg_code": "R_164"},
{"tel_code": "E_655", "dep_nom": "Rayures", "reg_code": "R_164"}, 
{"tel_code": "E_656", "dep_nom": "Cassé", "reg_code": "R_164"},

{"tel_code": "E_657", "dep_nom": "Intact", "reg_code": "R_165"}, 
{"tel_code": "E_658", "dep_nom": "Micro-rayure", "reg_code": "R_165"},
{"tel_code": "E_659", "dep_nom": "Rayures", "reg_code": "R_165"}, 
{"tel_code": "E_660", "dep_nom": "Cassé", "reg_code": "R_165"},

{"tel_code": "E_661", "dep_nom": "Intact", "reg_code": "R_166"}, 
{"tel_code": "E_662", "dep_nom": "Micro-rayure", "reg_code": "R_166"},
{"tel_code": "E_663", "dep_nom": "Rayures", "reg_code": "R_166"}, 
{"tel_code": "E_664", "dep_nom": "Cassé", "reg_code": "R_166"},

{"tel_code": "E_665", "dep_nom": "Intact", "reg_code": "R_167"}, 
{"tel_code": "E_666", "dep_nom": "Micro-rayure", "reg_code": "R_167"},
{"tel_code": "E_667", "dep_nom": "Rayures", "reg_code": "R_167"}, 
{"tel_code": "E_668", "dep_nom": "Cassé", "reg_code": "R_167"},

{"tel_code": "E_669", "dep_nom": "Intact", "reg_code": "R_168"}, 
{"tel_code": "E_670", "dep_nom": "Micro-rayure", "reg_code": "R_168"},
{"tel_code": "E_671", "dep_nom": "Rayures", "reg_code": "R_168"}, 
{"tel_code": "E_672", "dep_nom": "Cassé", "reg_code": "R_168"},

{"tel_code": "E_673", "dep_nom": "Intact", "reg_code": "R_169"}, 
{"tel_code": "E_674", "dep_nom": "Micro-rayure", "reg_code": "R_169"},
{"tel_code": "E_675", "dep_nom": "Rayures", "reg_code": "R_169"}, 
{"tel_code": "E_676", "dep_nom": "Cassé", "reg_code": "R_169"},

{"tel_code": "E_677", "dep_nom": "Intact", "reg_code": "R_170"}, 
{"tel_code": "E_678", "dep_nom": "Micro-rayure", "reg_code": "R_170"},
{"tel_code": "E_679", "dep_nom": "Rayures", "reg_code": "R_170"}, 
{"tel_code": "E_680", "dep_nom": "Cassé", "reg_code": "R_170"},

{"tel_code": "E_681", "dep_nom": "Intact", "reg_code": "R_171"}, 
{"tel_code": "E_682", "dep_nom": "Micro-rayure", "reg_code": "R_171"},
{"tel_code": "E_683", "dep_nom": "Rayures", "reg_code": "R_171"}, 
{"tel_code": "E_684", "dep_nom": "Cassé", "reg_code": "R_171"},

{"tel_code": "E_685", "dep_nom": "Intact", "reg_code": "R_172"}, 
{"tel_code": "E_686", "dep_nom": "Micro-rayure", "reg_code": "R_172"},
{"tel_code": "E_687", "dep_nom": "Rayures", "reg_code": "R_172"}, 
{"tel_code": "E_688", "dep_nom": "Cassé", "reg_code": "R_172"},

{"tel_code": "E_689", "dep_nom": "Intact", "reg_code": "R_173"}, 
{"tel_code": "E_690", "dep_nom": "Micro-rayure", "reg_code": "R_173"},
{"tel_code": "E_691", "dep_nom": "Rayures", "reg_code": "R_173"}, 
{"tel_code": "E_692", "dep_nom": "Cassé", "reg_code": "R_173"},

{"tel_code": "E_693", "dep_nom": "Intact", "reg_code": "R_174"}, 
{"tel_code": "E_694", "dep_nom": "Micro-rayure", "reg_code": "R_174"},
{"tel_code": "E_695", "dep_nom": "Rayures", "reg_code": "R_174"}, 
{"tel_code": "E_696", "dep_nom": "Cassé", "reg_code": "R_174"},

{"tel_code": "E_697", "dep_nom": "Intact", "reg_code": "R_175"}, 
{"tel_code": "E_698", "dep_nom": "Micro-rayure", "reg_code": "R_175"},
{"tel_code": "E_699", "dep_nom": "Rayures", "reg_code": "R_175"}, 
{"tel_code": "E_700", "dep_nom": "Cassé", "reg_code": "R_175"},

{"tel_code": "E_701", "dep_nom": "Intact", "reg_code": "R_176"}, 
{"tel_code": "E_702", "dep_nom": "Micro-rayure", "reg_code": "R_176"},
{"tel_code": "E_703", "dep_nom": "Rayures", "reg_code": "R_176"}, 
{"tel_code": "E_704", "dep_nom": "Cassé", "reg_code": "R_176"},

{"tel_code": "E_705", "dep_nom": "Intact", "reg_code": "R_177"}, 
{"tel_code": "E_706", "dep_nom": "Micro-rayure", "reg_code": "R_177"},
{"tel_code": "E_707", "dep_nom": "Rayures", "reg_code": "R_177"}, 
{"tel_code": "E_708", "dep_nom": "Cassé", "reg_code": "R_177"},

{"tel_code": "E_709", "dep_nom": "Intact", "reg_code": "R_178"}, 
{"tel_code": "E_710", "dep_nom": "Micro-rayure", "reg_code": "R_178"},
{"tel_code": "E_711", "dep_nom": "Rayures", "reg_code": "R_178"}, 
{"tel_code": "E_712", "dep_nom": "Cassé", "reg_code": "R_178"},

{"tel_code": "E_713", "dep_nom": "Intact", "reg_code": "R_179"}, 
{"tel_code": "E_714", "dep_nom": "Micro-rayure", "reg_code": "R_179"},
{"tel_code": "E_715", "dep_nom": "Rayures", "reg_code": "R_179"}, 
{"tel_code": "E_716", "dep_nom": "Cassé", "reg_code": "R_179"},


/* pour rajouter un nouveau telephone */
/*
{"tel_code": "E_717", "dep_nom": "Intact", "reg_code": "R_180"}, 
{"tel_code": "E_718", "dep_nom": "Micro-rayure", "reg_code": "R_180"},
{"tel_code": "E_719", "dep_nom": "Rayures", "reg_code": "R_180"}, 
{"tel_code": "E_720", "dep_nom": "Cassé", "reg_code": "R_180"},

*/
];

var tbl_etat_coque =[


/* ---------------------------------------------------------------------------Changez prix ICI -------------------------------------------------------------------------------------------------------*/



/* telephone : Iphone XR | si l'etat telephone : intact*/ 
/* par exemple si le telephone est un iphone xr et que l etat du telephone est intact changez le prix ici */
/* exemple avec l iphone XR mais meme chose pour tout les autres appareils . Vous avez besoin de juste changer le prix dans les guillemets */


/* si la coque est intact */
{"dep_code": "D_01", "tel_code": "E_01", "tel_nom": "Intact", "etat_code": "528€ "},
/* si la coque est avec des micro rayure */
{"dep_code": "D_02", "tel_code": "E_01", "tel_nom": "Micro-rayure", "etat_code": "519€  "},
/* si la coque est avec des rayure */
{"dep_code": "D_03", "tel_code": "E_01", "tel_nom": "rayure", "etat_code": "371€ "},
/* si la coque est cassé */
{"dep_code": "D_04", "tel_code": "E_01", "tel_nom": "Cassé", "etat_code": "216€ "},


/* telephone : Iphone XR | etat telephone : Micro rayure*/ 
{"dep_code": "D_05", "tel_code": "E_02", "tel_nom": "Intact", "etat_code": "519€ "},
{"dep_code": "D_06", "tel_code": "E_02", "tel_nom": "Micro-rayure", "etat_code": "519€ "},
{"dep_code": "D_07", "tel_code": "E_02", "tel_nom": "rayure", "etat_code": "371€ "},
{"dep_code": "D_08", "tel_code": "E_02", "tel_nom": "Cassé", "etat_code": "216€"},


/* telephone : Iphone XR | etat telephone : rayure*/ 
{"dep_code": "D_09", "tel_code": "E_03", "tel_nom": "Intact", "etat_code": "371€ "},
{"dep_code": "D_10", "tel_code": "E_03", "tel_nom": "Micro-rayure", "etat_code": "371€"},
{"dep_code": "D_11", "tel_code": "E_03", "tel_nom": "rayure", "etat_code": "371€ "},
{"dep_code": "D_12", "tel_code": "E_03", "tel_nom": "Cassé", "etat_code": "216€ "},


/* telephone : Iphone XR | etat telephone : Cassé*/ 
{"dep_code": "D_13", "tel_code": "E_04", "tel_nom": "Intact", "etat_code": "216€  "},
{"dep_code": "D_14", "tel_code": "E_04", "tel_nom": "Micro-rayure", "etat_code": "216€  "},
{"dep_code": "D_15", "tel_code": "E_04", "tel_nom": "rayure", "etat_code": "216€  "},
{"dep_code": "D_16", "tel_code": "E_04", "tel_nom": "Cassé", "etat_code": "216€ "},


/* telephone : Iphone XS Max | etat telephone : intact*/ 
{"dep_code": "D_21", "tel_code": "E_05", "tel_nom": "Intact", "etat_code": "564€ "},
{"dep_code": "D_22", "tel_code": "E_05", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_23", "tel_code": "E_05", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_24", "tel_code": "E_05", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone XS Max | etat telephone : Micro rayure*/ 
{"dep_code": "D_25", "tel_code": "E_06", "tel_nom": "Intact", "etat_code": "558 €"},
{"dep_code": "D_26", "tel_code": "E_06", "tel_nom": "Micro-rayure", "etat_code": "531 €"},
{"dep_code": "D_27", "tel_code": "E_06", "tel_nom": "rayure", "etat_code": "485 €"},
{"dep_code": "D_28", "tel_code": "E_06", "tel_nom": "Cassé", "etat_code": "326 €"},


/* telephone : Iphone XS Max | etat telephone : rayure*/ 
{"dep_code": "D_29", "tel_code": "E_07", "tel_nom": "Intact", "etat_code": "531 €"},
{"dep_code": "D_30", "tel_code": "E_07", "tel_nom": "Micro-rayure", "etat_code": "531 €"},
{"dep_code": "D_31", "tel_code": "E_07", "tel_nom": "rayure", "etat_code": "485 €"},
{"dep_code": "D_32", "tel_code": "E_07", "tel_nom": "Cassé", "etat_code": "326 €"},


/* telephone : Iphone XS Max | etat telephone : Cassé*/ 
{"dep_code": "D_33", "tel_code": "E_08", "tel_nom": "Intact", "etat_code": " 326€"},
{"dep_code": "D_34", "tel_code": "E_08", "tel_nom": "Micro-rayure", "etat_code": "326 €"},
{"dep_code": "D_35", "tel_code": "E_08", "tel_nom": "rayure", "etat_code": "326 €"},
{"dep_code": "D_36", "tel_code": "E_08", "tel_nom": "Cassé", "etat_code": "326 €"},


/* telephone : Iphone XS  | etat telephone : Intact*/
{"dep_code": "D_37", "tel_code": "E_09", "tel_nom": "Intact", "etat_code": "580 €"},
{"dep_code": "D_38", "tel_code": "E_09", "tel_nom": "Micro-rayure", "etat_code": "549 €"},
{"dep_code": "D_39", "tel_code": "E_09", "tel_nom": "rayure", "etat_code": " 513€"},
{"dep_code": "D_40", "tel_code": "E_09", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone XS  | etat telephone : Micro rayure*/
{"dep_code": "D_41", "tel_code": "E_10", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_42", "tel_code": "E_10", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_43", "tel_code": "E_10", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_44", "tel_code": "E_10", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone XS  | etat telephone : rayure*/
{"dep_code": "D_45", "tel_code": "E_11", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_46", "tel_code": "E_11", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_47", "tel_code": "E_11", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_48", "tel_code": "E_11", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone XS  | etat telephone : Cassé*/
{"dep_code": "D_49", "tel_code": "E_12", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_50", "tel_code": "E_12", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_51", "tel_code": "E_12", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_52", "tel_code": "E_12", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone X | etat telephone : Intact*/
{"dep_code": "D_53", "tel_code": "E_13", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_54", "tel_code": "E_13", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_55", "tel_code": "E_13", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_56", "tel_code": "E_13", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone X | etat telephone : Micro rayure*/
{"dep_code": "D_57", "tel_code": "E_14", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_58", "tel_code": "E_14", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_59", "tel_code": "E_14", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_60", "tel_code": "E_14", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone X | etat telephone : Rayures*/
{"dep_code": "D_61", "tel_code": "E_15", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_62", "tel_code": "E_15", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_63", "tel_code": "E_15", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_64", "tel_code": "E_15", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone X | etat telephone : Cassé*/
{"dep_code": "D_65", "tel_code": "E_16", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_66", "tel_code": "E_16", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_67", "tel_code": "E_16", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_68", "tel_code": "E_16", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 Plus | etat telephone : Intact*/
{"dep_code": "D_69", "tel_code": "E_17", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_70", "tel_code": "E_17", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_71", "tel_code": "E_17", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_72", "tel_code": "E_17", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 Plus | etat telephone : Micro rayure*/
{"dep_code": "D_73", "tel_code": "E_18", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_74", "tel_code": "E_18", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_75", "tel_code": "E_18", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_76", "tel_code": "E_18", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 Plus | etat telephone : Rayures*/
{"dep_code": "D_77", "tel_code": "E_19", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_78", "tel_code": "E_19", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_79", "tel_code": "E_19", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_80", "tel_code": "E_19", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 Plus | etat telephone : Cassé*/
{"dep_code": "D_81", "tel_code": "E_20", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_82", "tel_code": "E_20", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_83", "tel_code": "E_20", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_84", "tel_code": "E_20", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 | etat telephone : Intact*/
{"dep_code": "D_85", "tel_code": "E_21", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_86", "tel_code": "E_21", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_87", "tel_code": "E_21", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_88", "tel_code": "E_21", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 | etat telephone : Micro rayure*/
{"dep_code": "D_89", "tel_code": "E_22", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_90", "tel_code": "E_22", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_91", "tel_code": "E_22", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_92", "tel_code": "E_22", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 | etat telephone : Rayure*/
{"dep_code": "D_93", "tel_code": "E_23", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_94", "tel_code": "E_23", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_95", "tel_code": "E_23", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_96", "tel_code": "E_23", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 8 | etat telephone : Casse*/
{"dep_code": "D_101", "tel_code": "E_24", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_102", "tel_code": "E_24", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_103", "tel_code": "E_24", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_104", "tel_code": "E_24", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 Plus | etat telephone : Intact*/
{"dep_code": "D_105", "tel_code": "E_25", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_106", "tel_code": "E_25", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_107", "tel_code": "E_25", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_108", "tel_code": "E_25", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 Plus | etat telephone : Micro rayure*/
{"dep_code": "D_109", "tel_code": "E_26", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_110", "tel_code": "E_26", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_111", "tel_code": "E_26", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_112", "tel_code": "E_26", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 Plus | etat telephone : Rayure*/
{"dep_code": "D_113", "tel_code": "E_27", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_114", "tel_code": "E_27", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_115", "tel_code": "E_27", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_116", "tel_code": "E_27", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 Plus | etat telephone : Casse*/
{"dep_code": "D_117", "tel_code": "E_28", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_118", "tel_code": "E_28", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_119", "tel_code": "E_28", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_120", "tel_code": "E_28", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 | etat telephone : Intact*/
{"dep_code": "D_121", "tel_code": "E_29", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_122", "tel_code": "E_29", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_123", "tel_code": "E_29", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_124", "tel_code": "E_29", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 | etat telephone : Micro rayure*/
{"dep_code": "D_125", "tel_code": "E_30", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_126", "tel_code": "E_30", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_127", "tel_code": "E_30", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_128", "tel_code": "E_30", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 | etat telephone : rayure*/
{"dep_code": "D_129", "tel_code": "E_31", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_130", "tel_code": "E_31", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_131", "tel_code": "E_31", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_132", "tel_code": "E_31", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 7 | etat telephone : Cassé*/
{"dep_code": "D_133", "tel_code": "E_32", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_134", "tel_code": "E_32", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_135", "tel_code": "E_32", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_136", "tel_code": "E_32", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6S Plus | etat telephone : Intact*/
{"dep_code": "D_137", "tel_code": "E_33", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_138", "tel_code": "E_33", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_139", "tel_code": "E_33", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_140", "tel_code": "E_33", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6S Plus | etat telephone : Micro Rayure*/
{"dep_code": "D_141", "tel_code": "E_34", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_142", "tel_code": "E_34", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_143", "tel_code": "E_34", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_144", "tel_code": "E_34", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6s Plus | etat telephone : Rayure*/
{"dep_code": "D_145", "tel_code": "E_35", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_146", "tel_code": "E_35", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_147", "tel_code": "E_35", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_148", "tel_code": "E_35", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6s Plus | etat telephone : Casse*/
{"dep_code": "D_149", "tel_code": "E_36", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_150", "tel_code": "E_36", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_151", "tel_code": "E_36", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_152", "tel_code": "E_36", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6s | etat telephone : Intact*/
{"dep_code": "D_153", "tel_code": "E_37", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_154", "tel_code": "E_37", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_155", "tel_code": "E_37", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_156", "tel_code": "E_37", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6s | etat telephone : Micro Rayure*/
{"dep_code": "D_157", "tel_code": "E_38", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_158", "tel_code": "E_38", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_159", "tel_code": "E_38", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_160", "tel_code": "E_38", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6s | etat telephone : Rayure*/
{"dep_code": "D_161", "tel_code": "E_39", "tel_nom": "Intact", "etat_code": "6 €"},
{"dep_code": "D_162", "tel_code": "E_39", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_163", "tel_code": "E_39", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_164", "tel_code": "E_39", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6s | etat telephone : Casse*/
{"dep_code": "D_165", "tel_code": "E_40", "tel_nom": "Intact", "etat_code": "x €"},
{"dep_code": "D_166", "tel_code": "E_40", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_167", "tel_code": "E_40", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_168", "tel_code": "E_40", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6 Plus | etat telephone : Intact*/
{"dep_code": "D_169", "tel_code": "E_41", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_170", "tel_code": "E_41", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_171", "tel_code": "E_41", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_172", "tel_code": "E_41", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6 Plus | etat telephone : Micro rayure*/
{"dep_code": "D_173", "tel_code": "E_42", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_174", "tel_code": "E_42", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_175", "tel_code": "E_42", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_176", "tel_code": "E_42", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6 Plus | etat telephone : Rayure*/
{"dep_code": "D_177", "tel_code": "E_43", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_178", "tel_code": "E_43", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_179", "tel_code": "E_43", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_180", "tel_code": "E_43", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6 Plus | etat telephone : Casse*/
{"dep_code": "D_181", "tel_code": "E_44", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_182", "tel_code": "E_44", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_183", "tel_code": "E_44", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_184", "tel_code": "E_44", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6 | etat telephone : Intact*/
{"dep_code": "D_185", "tel_code": "E_45", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_186", "tel_code": "E_45", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_187", "tel_code": "E_45", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_188", "tel_code": "E_45", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6 | etat telephone : Micro rayure*/
{"dep_code": "D_189", "tel_code": "E_46", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_190", "tel_code": "E_46", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_191", "tel_code": "E_46", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_192", "tel_code": "E_46", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Iphone 6 | etat telephone : Rayure*/
{"dep_code": "D_193", "tel_code": "E_47", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_194", "tel_code": "E_47", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_195", "tel_code": "E_47", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_196", "tel_code": "E_47", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 6 | etat telephone : Casse*/
{"dep_code": "D_197", "tel_code": "E_48", "tel_nom": "Intact", "etat_code": "y €"},
{"dep_code": "D_198", "tel_code": "E_48", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_199", "tel_code": "E_48", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_200", "tel_code": "E_48", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Iphone 5SE | etat telephone : Intact*/
{"dep_code": "D_201a", "tel_code": "E_49", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_202a", "tel_code": "E_49", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_203a", "tel_code": "E_49", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_204a", "tel_code": "E_49", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Iphone 5SE | etat telephone : Micro rayure*/
{"dep_code": "D_201", "tel_code": "E_50", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_202", "tel_code": "E_50", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_203", "tel_code": "E_50", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_204", "tel_code": "E_50", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5SE | etat telephone : rayure*/
{"dep_code": "D_205", "tel_code": "E_51", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_206", "tel_code": "E_51", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_207", "tel_code": "E_51", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_208", "tel_code": "E_51", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5SE | etat telephone : Cassé*/
{"dep_code": "D_209", "tel_code": "E_52", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_210", "tel_code": "E_52", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_211", "tel_code": "E_52", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_212", "tel_code": "E_52", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5s | etat telephone : Intact*/
{"dep_code": "D_213", "tel_code": "E_53", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_214", "tel_code": "E_53", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_215", "tel_code": "E_53", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_216", "tel_code": "E_53", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5s | etat telephone : Micro rayure*/
{"dep_code": "D_217", "tel_code": "E_54", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_218", "tel_code": "E_54", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_219", "tel_code": "E_54", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_220", "tel_code": "E_54", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5s | etat telephone : rayure*/
{"dep_code": "D_221", "tel_code": "E_55", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_222", "tel_code": "E_55", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_223", "tel_code": "E_55", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_224", "tel_code": "E_55", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5s | etat telephone : Cassé*/
{"dep_code": "D_225", "tel_code": "E_56", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_226", "tel_code": "E_56", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_227", "tel_code": "E_56", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_228", "tel_code": "E_56", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5c | etat telephone : Intact*/
{"dep_code": "D_229", "tel_code": "E_57", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_230", "tel_code": "E_57", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_231", "tel_code": "E_57", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_232", "tel_code": "E_57", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5c | etat telephone : Micro rayure*/
{"dep_code": "D_233", "tel_code": "E_58", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_234", "tel_code": "E_58", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_235", "tel_code": "E_58", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_236", "tel_code": "E_58", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5c | etat telephone : rayure*/
{"dep_code": "D_237", "tel_code": "E_59", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_238", "tel_code": "E_59", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_239", "tel_code": "E_59", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_240", "tel_code": "E_59", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5c | etat telephone : Cassé*/
{"dep_code": "D_241", "tel_code": "E_60", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_242", "tel_code": "E_60", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_243", "tel_code": "E_60", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_244", "tel_code": "E_60", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5 | etat telephone : Intact*/
{"dep_code": "D_245", "tel_code": "E_61", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_246", "tel_code": "E_61", "tel_nom": "Micro-rayure", "etat_code": "io €"},
{"dep_code": "D_247", "tel_code": "E_61", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_248", "tel_code": "E_61", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5 | etat telephone : Micro rayure*/
{"dep_code": "D_249", "tel_code": "E_62", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_250", "tel_code": "E_62", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_251", "tel_code": "E_62", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_252", "tel_code": "E_62", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5 | etat telephone : rayure*/
{"dep_code": "D_253", "tel_code": "E_63", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_254", "tel_code": "E_63", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_255", "tel_code": "E_63", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_256", "tel_code": "E_63", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 5 | etat telephone : Cassé*/
{"dep_code": "D_257", "tel_code": "E_64", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_258", "tel_code": "E_64", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_259", "tel_code": "E_64", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_260", "tel_code": "E_64", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 4s | etat telephone : Intact*/
{"dep_code": "D_261", "tel_code": "E_65", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_262", "tel_code": "E_65", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_263", "tel_code": "E_65", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_264", "tel_code": "E_65", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 4s | etat telephone : Micro rayure*/
{"dep_code": "D_265", "tel_code": "E_66", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_266", "tel_code": "E_66", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_267", "tel_code": "E_66", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_268", "tel_code": "E_66", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 4s | etat telephone : rayure*/
{"dep_code": "D_269", "tel_code": "E_67", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_270", "tel_code": "E_67", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_271", "tel_code": "E_67", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_272", "tel_code": "E_67", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 4s | etat telephone : Cassé*/
{"dep_code": "D_273", "tel_code": "E_68", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_274", "tel_code": "E_68", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_275", "tel_code": "E_68", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_276", "tel_code": "E_68", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 4 | etat telephone : Intact*/
{"dep_code": "D_277", "tel_code": "E_69", "tel_nom": "Intact", "etat_code": "p €"},
{"dep_code": "D_278", "tel_code": "E_69", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_279", "tel_code": "E_69", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_280", "tel_code": "E_69", "tel_nom": "Cassé", "etat_code": "550 €"},





/* telephone : Iphone 4 | etat telephone : Micro rayure*/
{"dep_code": "D_285", "tel_code": "E_70", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_286", "tel_code": "E_70", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_287", "tel_code": "E_70", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_288", "tel_code": "E_70", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 4 | etat telephone : rayure*/
{"dep_code": "D_289", "tel_code": "E_71", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_290", "tel_code": "E_71", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_291", "tel_code": "E_71", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_292", "tel_code": "E_71", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Iphone 4 | etat telephone : Casse*/
{"dep_code": "D_293", "tel_code": "E_72", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_294", "tel_code": "E_72", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_295", "tel_code": "E_72", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_296", "tel_code": "E_72", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10+ | etat telephone : Intact*/
{"dep_code": "D_297", "tel_code": "E_73", "tel_nom": "Intact", "etat_code": "lo €"},
{"dep_code": "D_298", "tel_code": "E_73", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_299", "tel_code": "E_73", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_300", "tel_code": "E_73", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10+ | etat telephone : Micro Rayure*/
{"dep_code": "D_301", "tel_code": "E_74", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_302", "tel_code": "E_74", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_303", "tel_code": "E_74", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_304", "tel_code": "E_74", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10+ | etat telephone : Rayure*/
{"dep_code": "D_305", "tel_code": "E_75", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_306", "tel_code": "E_75", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_307", "tel_code": "E_75", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_308", "tel_code": "E_75", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Samsung s10+ | etat telephone : Casse*/
{"dep_code": "D_309", "tel_code": "E_76", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_310", "tel_code": "E_76", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_311", "tel_code": "E_76", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_312", "tel_code": "E_76", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10e | etat telephone : Intact */
{"dep_code": "D_313", "tel_code": "E_77", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_314", "tel_code": "E_77", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_315", "tel_code": "E_77", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_316", "tel_code": "E_77", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10e | etat telephone : Micro rayure */
{"dep_code": "D_317", "tel_code": "E_78", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_318", "tel_code": "E_78", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_319", "tel_code": "E_78", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_320", "tel_code": "E_78", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10e | etat telephone : rayure*/
{"dep_code": "D_321", "tel_code": "E_79", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_322", "tel_code": "E_79", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_323", "tel_code": "E_79", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_324", "tel_code": "E_79", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10e | etat telephone : Casse*/
{"dep_code": "D_325", "tel_code": "E_80", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_326", "tel_code": "E_80", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_327", "tel_code": "E_80", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_328", "tel_code": "E_80", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10 | etat telephone : Intact*/
{"dep_code": "D_329", "tel_code": "E_81", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_330", "tel_code": "E_81", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_331", "tel_code": "E_81", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_332", "tel_code": "E_81", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10 | etat telephone : Micro rayure*/
{"dep_code": "D_333", "tel_code": "E_82", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_334", "tel_code": "E_82", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_335", "tel_code": "E_82", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_336", "tel_code": "E_82", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10 | etat telephone : rayure*/
{"dep_code": "D_337", "tel_code": "E_83", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_338", "tel_code": "E_83", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_339", "tel_code": "E_83", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_340", "tel_code": "E_83", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s10 | etat telephone : Casse*/
{"dep_code": "D_341", "tel_code": "E_84", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_342", "tel_code": "E_84", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_343", "tel_code": "E_84", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_344", "tel_code": "E_84", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9+ | etat telephone : Intact*/
{"dep_code": "D_345", "tel_code": "E_85", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_346", "tel_code": "E_85", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_347", "tel_code": "E_85", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_348", "tel_code": "E_85", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9+ | etat telephone : Micro rayure*/
{"dep_code": "D_349", "tel_code": "E_86", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_350", "tel_code": "E_86", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_351", "tel_code": "E_86", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_352", "tel_code": "E_86", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9+ | etat telephone : rayure*/
{"dep_code": "D_353", "tel_code": "E_87", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_354", "tel_code": "E_87", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_355", "tel_code": "E_87", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_356", "tel_code": "E_87", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9+ | etat telephone : Casse*/
{"dep_code": "D_357", "tel_code": "E_88", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_358", "tel_code": "E_88", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_359", "tel_code": "E_88", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_360", "tel_code": "E_88", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9 | etat telephone : Intact*/
{"dep_code": "D_361", "tel_code": "E_89", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_362", "tel_code": "E_89", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_363", "tel_code": "E_89", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_364", "tel_code": "E_89", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9 | etat telephone : micro rayure*/
{"dep_code": "D_365", "tel_code": "E_90", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_366", "tel_code": "E_90", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_367", "tel_code": "E_90", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_368", "tel_code": "E_90", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9 | etat telephone : rayure*/
{"dep_code": "D_369", "tel_code": "E_91", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_370", "tel_code": "E_91", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_371", "tel_code": "E_91", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_372", "tel_code": "E_91", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s9 | etat telephone : Cassé*/
{"dep_code": "D_373", "tel_code": "E_92", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_374", "tel_code": "E_92", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_375", "tel_code": "E_92", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_376", "tel_code": "E_92", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8+ | etat telephone : Intact*/
{"dep_code": "D_377", "tel_code": "E_93", "tel_nom": "Intact", "etat_code": "en €"},
{"dep_code": "D_378", "tel_code": "E_93", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_379", "tel_code": "E_93", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_380", "tel_code": "E_93", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8+ | etat telephone : Micro rayure*/
{"dep_code": "D_381", "tel_code": "E_94", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_382", "tel_code": "E_94", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_383", "tel_code": "E_94", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_384", "tel_code": "E_94", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8+ | etat telephone : rayure*/
{"dep_code": "D_385", "tel_code": "E_95", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_386", "tel_code": "E_95", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_387", "tel_code": "E_95", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_388", "tel_code": "E_95", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8+ | etat telephone : Cassé*/
{"dep_code": "D_389", "tel_code": "E_96", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_390", "tel_code": "E_96", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_391", "tel_code": "E_96", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_392", "tel_code": "E_96", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8 | etat telephone : Intact*/
{"dep_code": "D_393", "tel_code": "E_97", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_394", "tel_code": "E_97", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_395", "tel_code": "E_97", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_396", "tel_code": "E_97", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8 | etat telephone : Micro rayure*/
{"dep_code": "D_397", "tel_code": "E_98", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_398", "tel_code": "E_98", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_399", "tel_code": "E_98", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_400", "tel_code": "E_98", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8 | etat telephone : Rayure*/
{"dep_code": "D_401", "tel_code": "E_99", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_402", "tel_code": "E_99", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_403", "tel_code": "E_99", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_404", "tel_code": "E_99", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s8 | etat telephone : Casse*/
{"dep_code": "D_405", "tel_code": "E_100", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_406", "tel_code": "E_100", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_407", "tel_code": "E_100", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_408", "tel_code": "E_100", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 Edge | etat telephone : Intact*/
{"dep_code": "D_409", "tel_code": "E_101", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_410", "tel_code": "E_101", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_411", "tel_code": "E_101", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_412", "tel_code": "E_101", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 Edge | etat telephone : Micro rayure*/
{"dep_code": "D_413", "tel_code": "E_102", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_414", "tel_code": "E_102", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_415", "tel_code": "E_102", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_416", "tel_code": "E_102", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 edge  | etat telephone : rayure*/
{"dep_code": "D_417", "tel_code": "E_103", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_418", "tel_code": "E_103", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_419", "tel_code": "E_103", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_420", "tel_code": "E_103", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 Edge | etat telephone : Cassé*/
{"dep_code": "D_421", "tel_code": "E_104", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_422", "tel_code": "E_104", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_423", "tel_code": "E_104", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_424", "tel_code": "E_104", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 | etat telephone : Intact*/
{"dep_code": "D_425", "tel_code": "E_105", "tel_nom": "Intact", "etat_code": "60 €"},
{"dep_code": "D_426", "tel_code": "E_105", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_427", "tel_code": "E_105", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_428", "tel_code": "E_105", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 | etat telephone : Micro rayure*/
{"dep_code": "D_429", "tel_code": "E_106", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_430", "tel_code": "E_106", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_431", "tel_code": "E_106", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_432", "tel_code": "E_106", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 | etat telephone : rayure*/
{"dep_code": "D_433", "tel_code": "E_107", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_434", "tel_code": "E_107", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_435", "tel_code": "E_107", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_436", "tel_code": "E_107", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s7 | etat telephone : Casse*/
{"dep_code": "D_437", "tel_code": "E_108", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_438", "tel_code": "E_108", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_439", "tel_code": "E_108", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_440", "tel_code": "E_108", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 Edge Plus  | etat telephone : Intact*/
{"dep_code": "D_437b", "tel_code": "E_109", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_438b", "tel_code": "E_109", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_439b", "tel_code": "E_109", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_440b", "tel_code": "E_109", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 Edge Plus | etat telephone : Micro rayure*/
{"dep_code": "D_441", "tel_code": "E_110", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_442", "tel_code": "E_110", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_443", "tel_code": "E_110", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_444", "tel_code": "E_110", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 Edge Plus | etat telephone : Rayure*/
{"dep_code": "D_445", "tel_code": "E_111", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_446", "tel_code": "E_111", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_447", "tel_code": "E_111", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_448", "tel_code": "E_111", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6  edge Plus| etat telephone : Casse*/
{"dep_code": "D_449", "tel_code": "E_112", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_450", "tel_code": "E_112", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_451", "tel_code": "E_112", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_452", "tel_code": "E_112", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 edge  | etat telephone : Intact*/
{"dep_code": "D_453", "tel_code": "E_113", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_454", "tel_code": "E_113", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_455", "tel_code": "E_113", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_456", "tel_code": "E_113", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 Edge | etat telephone : Micro rayure*/
{"dep_code": "D_457", "tel_code": "E_114", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_458", "tel_code": "E_114", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_459", "tel_code": "E_114", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_460", "tel_code": "E_114", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 edge | etat telephone : rayure*/
{"dep_code": "D_461", "tel_code": "E_115", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_462", "tel_code": "E_115", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_463", "tel_code": "E_115", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_464", "tel_code": "E_115", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 edge | etat telephone : cassé*/
{"dep_code": "D_465", "tel_code": "E_116", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_466", "tel_code": "E_116", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_467", "tel_code": "E_116", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_468", "tel_code": "E_116", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6  | etat telephone : Intact*/
{"dep_code": "D_469", "tel_code": "E_117", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_470", "tel_code": "E_117", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_471", "tel_code": "E_117", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_472", "tel_code": "E_117", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 | etat telephone : Micro rayure */
{"dep_code": "D_473", "tel_code": "E_118", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_474", "tel_code": "E_118", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_475", "tel_code": "E_118", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_476", "tel_code": "E_118", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 | etat telephone : rayure*/
{"dep_code": "D_477", "tel_code": "E_119", "tel_nom": "Intact", "etat_code": "5 €"},
{"dep_code": "D_478", "tel_code": "E_119", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_479", "tel_code": "E_119", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_480", "tel_code": "E_119", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s6 | etat telephone : Cassé*/
{"dep_code": "D_481", "tel_code": "E_120", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_482", "tel_code": "E_120", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_483", "tel_code": "E_120", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_484", "tel_code": "E_120", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 k zoom | etat telephone : Intact*/
{"dep_code": "D_485", "tel_code": "E_121", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_486", "tel_code": "E_121", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_487", "tel_code": "E_121", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_488", "tel_code": "E_121", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 k zoom | etat telephone : Micro rayure*/
{"dep_code": "D_489", "tel_code": "E_122", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_490", "tel_code": "E_122", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_491", "tel_code": "E_122", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_492", "tel_code": "E_122", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 k zoom | etat telephone : rayure*/
{"dep_code": "D_493", "tel_code": "E_123", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_494", "tel_code": "E_123", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_495", "tel_code": "E_123", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_496", "tel_code": "E_123", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 k zoom | etat telephone : cassé*/
{"dep_code": "D_497", "tel_code": "E_124", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_498", "tel_code": "E_124", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_499", "tel_code": "E_124", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_500", "tel_code": "E_124", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 active | etat telephone : Intact*/
{"dep_code": "D_501", "tel_code": "E_125", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_502", "tel_code": "E_125", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_503", "tel_code": "E_125", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_504", "tel_code": "E_125", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 active | etat telephone : Micro rayure*/
{"dep_code": "D_505", "tel_code": "E_126", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_506", "tel_code": "E_126", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_507", "tel_code": "E_126", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_508", "tel_code": "E_126", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 active | etat telephone : Rayure*/
{"dep_code": "D_509", "tel_code": "E_127", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_510", "tel_code": "E_127", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_511", "tel_code": "E_127", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_512", "tel_code": "E_127", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 active | etat telephone : Casse*/
{"dep_code": "D_513", "tel_code": "E_128", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_514", "tel_code": "E_128", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_515", "tel_code": "E_128", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_516", "tel_code": "E_128", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 4g+ | etat telephone : Intact*/
{"dep_code": "D_517", "tel_code": "E_129", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_518", "tel_code": "E_129", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_519", "tel_code": "E_129", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_520", "tel_code": "E_129", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Samsung s5 4g+ | etat telephone : Micro rayure*/
{"dep_code": "D_521", "tel_code": "E_130", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_522", "tel_code": "E_130", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_523", "tel_code": "E_130", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_524", "tel_code": "E_130", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 4g+ | etat telephone : rayure*/
{"dep_code": "D_525", "tel_code": "E_131", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_526", "tel_code": "E_131", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_527", "tel_code": "E_131", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_528", "tel_code": "E_131", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 4g+ | etat telephone : Casse*/
{"dep_code": "D_529", "tel_code": "E_132", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_530", "tel_code": "E_132", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_531", "tel_code": "E_132", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_532", "tel_code": "E_132", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 | etat telephone : Intact*/
{"dep_code": "D_533", "tel_code": "E_133", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_534", "tel_code": "E_133", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_535", "tel_code": "E_133", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_536", "tel_code": "E_133", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 | etat telephone : Micro rayure*/
{"dep_code": "D_537", "tel_code": "E_134", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_538", "tel_code": "E_134", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_539", "tel_code": "E_134", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_540", "tel_code": "E_134", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 | etat telephone : rayure*/
{"dep_code": "D_541", "tel_code": "E_135", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_542", "tel_code": "E_135", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_543", "tel_code": "E_135", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_544", "tel_code": "E_135", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 | etat telephone : Casse*/
{"dep_code": "D_545", "tel_code": "E_136", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_546", "tel_code": "E_136", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_547", "tel_code": "E_136", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_548", "tel_code": "E_136", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 zoom | etat telephone : Intact*/
{"dep_code": "D_549", "tel_code": "E_137", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_550", "tel_code": "E_137", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_551", "tel_code": "E_137", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_552", "tel_code": "E_137", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 zoom | etat telephone : Micro rayure*/
{"dep_code": "D_553", "tel_code": "E_138", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_554", "tel_code": "E_138", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_555", "tel_code": "E_138", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_556", "tel_code": "E_138", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 zoom | etat telephone : rayure*/
{"dep_code": "D_557", "tel_code": "E_139", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_558", "tel_code": "E_139", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_559", "tel_code": "E_139", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_560", "tel_code": "E_139", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 zoom | etat telephone : Casse*/
{"dep_code": "D_561", "tel_code": "E_140", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_562", "tel_code": "E_140", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_563", "tel_code": "E_140", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_564", "tel_code": "E_140", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 value edition | etat telephone : Intact*/
{"dep_code": "D_565", "tel_code": "E_141", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_566", "tel_code": "E_141", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_567", "tel_code": "E_141", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_568", "tel_code": "E_141", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 value edition | etat telephone : Micro rayure*/
{"dep_code": "D_569", "tel_code": "E_142", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_570", "tel_code": "E_142", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_571", "tel_code": "E_142", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_572", "tel_code": "E_142", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 value edition | etat telephone : rayure*/
{"dep_code": "D_573", "tel_code": "E_143", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_574", "tel_code": "E_143", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_575", "tel_code": "E_143", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_576", "tel_code": "E_143", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 value edition | etat telephone : Casse*/
{"dep_code": "D_577", "tel_code": "E_144", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_578", "tel_code": "E_144", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_579", "tel_code": "E_144", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_580", "tel_code": "E_144", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 advance 4g | etat telephone : Intact*/
{"dep_code": "D_581", "tel_code": "E_145", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_582", "tel_code": "E_145", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_583", "tel_code": "E_145", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_584", "tel_code": "E_145", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 advance 4g | etat telephone : Micro rayure*/
{"dep_code": "D_585", "tel_code": "E_146", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_586", "tel_code": "E_146", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_587", "tel_code": "E_146", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_588", "tel_code": "E_146", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 advance 4g | etat telephone : rayure*/
{"dep_code": "D_589", "tel_code": "E_147", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_590", "tel_code": "E_147", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_591", "tel_code": "E_147", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_592", "tel_code": "E_147", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 advance 4g | etat telephone : Casse*/
{"dep_code": "D_593", "tel_code": "E_148", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_594", "tel_code": "E_148", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_595", "tel_code": "E_148", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_596", "tel_code": "E_148", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 active | etat telephone : Intact*/
{"dep_code": "D_597", "tel_code": "E_149", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_598", "tel_code": "E_149", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_599", "tel_code": "E_149", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_600", "tel_code": "E_149", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 active | etat telephone : micro rayure*/
{"dep_code": "D_601", "tel_code": "E_150", "tel_nom": "Intact", "etat_code": "60 €"},
{"dep_code": "D_602", "tel_code": "E_150", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_603", "tel_code": "E_150", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_604", "tel_code": "E_150", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 active | etat telephone : rayure*/
{"dep_code": "D_605", "tel_code": "E_151", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_606", "tel_code": "E_151", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_607", "tel_code": "E_151", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_608", "tel_code": "E_151", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 active | etat telephone : Casse*/
{"dep_code": "D_609", "tel_code": "E_152", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_610", "tel_code": "E_152", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_611", "tel_code": "E_152", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_612", "tel_code": "E_152", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 4g (19505) | etat telephone : Intact*/
{"dep_code": "D_613", "tel_code": "E_153", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_614", "tel_code": "E_153", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_615", "tel_code": "E_153", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_616", "tel_code": "E_153", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 4g (19505) | etat telephone : Micro rayure*/
{"dep_code": "D_617", "tel_code": "E_154", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_618", "tel_code": "E_154", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_619", "tel_code": "E_154", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_620", "tel_code": "E_154", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 4g (19505) | etat telephone : rayure*/
{"dep_code": "D_621", "tel_code": "E_155", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_622", "tel_code": "E_155", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_623", "tel_code": "E_155", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_624", "tel_code": "E_155", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 4g (19505) | etat telephone : Casse*/
{"dep_code": "D_625", "tel_code": "E_156", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_626", "tel_code": "E_156", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_627", "tel_code": "E_156", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_628", "tel_code": "E_156", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 | etat telephone : Intact*/
{"dep_code": "D_629", "tel_code": "E_157", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_630", "tel_code": "E_157", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_631", "tel_code": "E_157", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_632", "tel_code": "E_157", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 | etat telephone : Micro rayure*/
{"dep_code": "D_633", "tel_code": "E_158", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_634", "tel_code": "E_158", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_635", "tel_code": "E_158", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_636", "tel_code": "E_158", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 | etat telephone : rayure*/
{"dep_code": "D_637", "tel_code": "E_159", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_638", "tel_code": "E_159", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_639", "tel_code": "E_159", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_640", "tel_code": "E_159", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 | etat telephone : Casse*/
{"dep_code": "D_641", "tel_code": "E_160", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_642", "tel_code": "E_160", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_643", "tel_code": "E_160", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_644", "tel_code": "E_160", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3 | etat telephone : Intact*/
{"dep_code": "D_645", "tel_code": "E_161", "tel_nom": "Intact", "etat_code": "x €"},
{"dep_code": "D_646", "tel_code": "E_161", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_647", "tel_code": "E_161", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_648", "tel_code": "E_161", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3 | etat telephone : Micro rayure*/
{"dep_code": "D_649", "tel_code": "E_162", "tel_nom": "Intact", "etat_code": "7 €"},
{"dep_code": "D_650", "tel_code": "E_162", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_651", "tel_code": "E_162", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_652", "tel_code": "E_162", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3| etat telephone : Rayure*/
{"dep_code": "D_653", "tel_code": "E_163", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_654", "tel_code": "E_163", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_655", "tel_code": "E_163", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_656", "tel_code": "E_163", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3 | etat telephone : Casse*/
{"dep_code": "D_657", "tel_code": "E_164", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_658", "tel_code": "E_164", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_659", "tel_code": "E_164", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_670", "tel_code": "E_164", "tel_nom": "Cassé", "etat_code": "550 €"},



/* telephone : Samsung s2 Plus | etat telephone : Intact*/
{"dep_code": "D_671", "tel_code": "E_165", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_672", "tel_code": "E_165", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_673", "tel_code": "E_165", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_674", "tel_code": "E_165", "tel_nom": "Cassé", "etat_code": "550 €"},



/* telephone : Samsung s2  Plus | etat telephone :Micro rayure */
{"dep_code": "D_675", "tel_code": "E_166", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_676", "tel_code": "E_166", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_677", "tel_code": "E_166", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_678", "tel_code": "E_166", "tel_nom": "Cassé", "etat_code": "550 €"},



/* telephone : Samsung s2  Plus | etat telephone : rayure */
{"dep_code": "D_679", "tel_code": "E_167", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_680", "tel_code": "E_167", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_681", "tel_code": "E_167", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_682", "tel_code": "E_167", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s2  Plus | etat telephone : Casse */
{"dep_code": "D_683", "tel_code": "E_168", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_684", "tel_code": "E_168", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_685", "tel_code": "E_168", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_686", "tel_code": "E_168", "tel_nom": "Cassé", "etat_code": "550 €"},



/* telephone : Samsung s2 | etat telephone : Intact*/
{"dep_code": "D_687", "tel_code": "E_169", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_688", "tel_code": "E_169", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_689", "tel_code": "E_169", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_690", "tel_code": "E_169", "tel_nom": "Cassé", "etat_code": "550 €"},



/* telephone : Samsung s2 | etat telephone : Micro rayure*/
{"dep_code": "D_691", "tel_code": "E_170", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_692", "tel_code": "E_170", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_693", "tel_code": "E_170", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_694", "tel_code": "E_170", "tel_nom": "Cassé", "etat_code": "550 €"},



/* telephone : Samsung s2 | etat telephone : rayure*/
{"dep_code": "D_695", "tel_code": "E_171", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_696", "tel_code": "E_171", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_697", "tel_code": "E_171", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_698", "tel_code": "E_171", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s2 | etat telephone : Casse*/
{"dep_code": "D_699", "tel_code": "E_172", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_700", "tel_code": "E_172", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_701", "tel_code": "E_172", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_702", "tel_code": "E_172", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Plus | etat telephone : Intact*/
{"dep_code": "D_703", "tel_code": "E_173", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_704", "tel_code": "E_173", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_705", "tel_code": "E_173", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_706", "tel_code": "E_173", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Plus | etat telephone : Micro Rayure*/
{"dep_code": "D_707", "tel_code": "E_174", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_708", "tel_code": "E_174", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_709", "tel_code": "E_174", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_710", "tel_code": "E_174", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Plus | etat telephone : Rayure*/
{"dep_code": "D_711", "tel_code": "E_175", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_712", "tel_code": "E_175", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_713", "tel_code": "E_175", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_714", "tel_code": "E_175", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Plus | etat telephone : Casse*/
{"dep_code": "D_715", "tel_code": "E_176", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_716", "tel_code": "E_176", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_717", "tel_code": "E_176", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_718", "tel_code": "E_176", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Duos | etat telephone : Intact*/
{"dep_code": "D_719", "tel_code": "E_177", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_720", "tel_code": "E_177", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_721", "tel_code": "E_177", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_722", "tel_code": "E_177", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Duos | etat telephone : Micro rayure*/
{"dep_code": "D_723", "tel_code": "E_178", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_724", "tel_code": "E_178", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_725", "tel_code": "E_178", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_726", "tel_code": "E_178", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Duos | etat telephone : Rayure*/
{"dep_code": "D_727", "tel_code": "E_179", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_728", "tel_code": "E_179", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_729", "tel_code": "E_179", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_730", "tel_code": "E_179", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s Duos | etat telephone : Casse*/
{"dep_code": "D_731", "tel_code": "E_180", "tel_nom": "Intact", "etat_code": "6000 €"},
{"dep_code": "D_732", "tel_code": "E_180", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_733", "tel_code": "E_180", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_734", "tel_code": "E_180", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 Mini | etat telephone : Intact*/
{"dep_code": "D_735", "tel_code": "E_181", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_736", "tel_code": "E_181", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_737", "tel_code": "E_181", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_738", "tel_code": "E_181", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 Mini | etat telephone : Micro rayure*/
{"dep_code": "D_739", "tel_code": "E_182", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_740", "tel_code": "E_182", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_741", "tel_code": "E_182", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_742", "tel_code": "E_182", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 Mini | etat telephone : Rayure*/
{"dep_code": "D_743", "tel_code": "E_183", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_744", "tel_code": "E_183", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_745", "tel_code": "E_183", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_746", "tel_code": "E_183", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s5 Mini | etat telephone : Casse*/
{"dep_code": "D_747", "tel_code": "E_184", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_748", "tel_code": "E_184", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_749", "tel_code": "E_184", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_750", "tel_code": "E_184", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini Duos | etat telephone : Intact*/
{"dep_code": "D_751", "tel_code": "E_185", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_752", "tel_code": "E_185", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_753", "tel_code": "E_185", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_754", "tel_code": "E_185", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini Duos | etat telephone : Micro rayure*/
{"dep_code": "D_755", "tel_code": "E_186", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_756", "tel_code": "E_186", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_757", "tel_code": "E_186", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_758", "tel_code": "E_186", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini Duos | etat telephone : Rayure*/
{"dep_code": "D_759", "tel_code": "E_187", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_760", "tel_code": "E_187", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_761", "tel_code": "E_187", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_762", "tel_code": "E_187", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini Duos | etat telephone : Casse*/
{"dep_code": "D_763", "tel_code": "E_188", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_764", "tel_code": "E_188", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_765", "tel_code": "E_188", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_766", "tel_code": "E_188", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini 4G | etat telephone : Intact*/
{"dep_code": "D_767", "tel_code": "E_189", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_768", "tel_code": "E_189", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_769", "tel_code": "E_189", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_770", "tel_code": "E_189", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini 4G | etat telephone : Micro rayure*/
{"dep_code": "D_771", "tel_code": "E_190", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_772", "tel_code": "E_190", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_773", "tel_code": "E_190", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_774", "tel_code": "E_190", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini 4G | etat telephone : Rayure*/
{"dep_code": "D_775", "tel_code": "E_191", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_776", "tel_code": "E_191", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_777", "tel_code": "E_191", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_778", "tel_code": "E_191", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini 4G | etat telephone : Casse*/
{"dep_code": "D_779", "tel_code": "E_192", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_780", "tel_code": "E_192", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_781", "tel_code": "E_192", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_782", "tel_code": "E_192", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini| etat telephone : Intact*/
{"dep_code": "D_783", "tel_code": "E_193", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_784", "tel_code": "E_193", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_785", "tel_code": "E_193", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_786", "tel_code": "E_193", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini| etat telephone : Micro Rayure*/
{"dep_code": "D_787", "tel_code": "E_194", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_788", "tel_code": "E_194", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_789", "tel_code": "E_194", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_790", "tel_code": "E_194", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini| etat telephone : Rayure*/
{"dep_code": "D_791", "tel_code": "E_195", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_792", "tel_code": "E_195", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_793", "tel_code": "E_195", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_794", "tel_code": "E_195", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s4 Mini| etat telephone : Casse*/
{"dep_code": "D_795", "tel_code": "E_196", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_796", "tel_code": "E_196", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_797", "tel_code": "E_196", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_798", "tel_code": "E_196", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3 Mini | etat telephone : Intact*/
{"dep_code": "D_799", "tel_code": "E_197", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_800", "tel_code": "E_197", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_801", "tel_code": "E_197", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_802", "tel_code": "E_197", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3 Mini | etat telephone : Micro rayure*/
{"dep_code": "D_799a", "tel_code": "E_198", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_800a", "tel_code": "E_198", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_801a", "tel_code": "E_198", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_802a", "tel_code": "E_198", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3 Mini | etat telephone : Rayure*/
{"dep_code": "D_799b", "tel_code": "E_199", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_800b", "tel_code": "E_199", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_801b", "tel_code": "E_199", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_802b", "tel_code": "E_199", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung s3 Mini | etat telephone : Casse*/
{"dep_code": "D_799c", "tel_code": "E_200", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_800c", "tel_code": "E_200", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_801c", "tel_code": "E_200", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_802c", "tel_code": "E_200", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note Edge| etat telephone : Intact*/
{"dep_code": "D_803", "tel_code": "E_201", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_804", "tel_code": "E_201", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_805", "tel_code": "E_201", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_806", "tel_code": "E_201", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note Edge| etat telephone : Micro rayure*/
{"dep_code": "D_807", "tel_code": "E_202", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_808", "tel_code": "E_202", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_809", "tel_code": "E_202", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_810", "tel_code": "E_202", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note Edge| etat telephone : Rayure*/
{"dep_code": "D_811", "tel_code": "E_203", "tel_nom": "Intact", "etat_code": "x €"},
{"dep_code": "D_812", "tel_code": "E_203", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_813", "tel_code": "E_203", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_814", "tel_code": "E_203", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note Edge| etat telephone : Casse*/
{"dep_code": "D_815", "tel_code": "E_204", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_816", "tel_code": "E_204", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_817", "tel_code": "E_204", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_818", "tel_code": "E_204", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 9| etat telephone : Intact*/
{"dep_code": "D_819", "tel_code": "E_205", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_820", "tel_code": "E_205", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_821", "tel_code": "E_205", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_822", "tel_code": "E_205", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 9| etat telephone : Micro rayure*/
{"dep_code": "D_823", "tel_code": "E_206", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_824", "tel_code": "E_206", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_825", "tel_code": "E_206", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_826", "tel_code": "E_206", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 9| etat telephone : rayure*/
{"dep_code": "D_827", "tel_code": "E_207", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_828", "tel_code": "E_207", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_829", "tel_code": "E_207", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_830", "tel_code": "E_207", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 9| etat telephone : Casse*/
{"dep_code": "D_831", "tel_code": "E_208", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_832", "tel_code": "E_208", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_833", "tel_code": "E_208", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_834", "tel_code": "E_208", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 8| etat telephone : Intact*/
{"dep_code": "D_835", "tel_code": "E_209", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_836", "tel_code": "E_209", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_837", "tel_code": "E_209", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_838", "tel_code": "E_209", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 8| etat telephone : Micro rayure*/
{"dep_code": "D_839", "tel_code": "E_210", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_840", "tel_code": "E_210", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_841", "tel_code": "E_210", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_842", "tel_code": "E_210", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 8| etat telephone : rayure*/
{"dep_code": "D_843", "tel_code": "E_211", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_844", "tel_code": "E_211", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_845", "tel_code": "E_211", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_846", "tel_code": "E_211", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 8| etat telephone : Casse*/
{"dep_code": "D_847", "tel_code": "E_212", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_848", "tel_code": "E_212", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_849", "tel_code": "E_212", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_850", "tel_code": "E_212", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 7| etat telephone : Intact*/
{"dep_code": "D_851", "tel_code": "E_213", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_852", "tel_code": "E_213", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_853", "tel_code": "E_213", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_854", "tel_code": "E_213", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 7| etat telephone : Micro rayure*/
{"dep_code": "D_855", "tel_code": "E_214", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_856", "tel_code": "E_214", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_857", "tel_code": "E_214", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_858", "tel_code": "E_214", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 7| etat telephone : rayure*/
{"dep_code": "D_859", "tel_code": "E_215", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_860", "tel_code": "E_215", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_861", "tel_code": "E_215", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_862", "tel_code": "E_215", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 7| etat telephone : Casse*/
{"dep_code": "D_863", "tel_code": "E_216", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_864", "tel_code": "E_216", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_865", "tel_code": "E_216", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_866", "tel_code": "E_216", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 5| etat telephone : Intact*/
{"dep_code": "D_867", "tel_code": "E_217", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_868", "tel_code": "E_217", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_869", "tel_code": "E_217", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_870", "tel_code": "E_217", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 5| etat telephone : Micro rayure*/
{"dep_code": "D_871", "tel_code": "E_218", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_872", "tel_code": "E_218", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_873", "tel_code": "E_218", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_874", "tel_code": "E_218", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 5| etat telephone : rayure*/
{"dep_code": "D_875", "tel_code": "E_219", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_876", "tel_code": "E_219", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_877", "tel_code": "E_219", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_878", "tel_code": "E_219", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 5| etat telephone : Casse*/
{"dep_code": "D_879", "tel_code": "E_220", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_880", "tel_code": "E_220", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_881", "tel_code": "E_220", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_882", "tel_code": "E_220", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 4| etat telephone : Intact*/
{"dep_code": "D_883", "tel_code": "E_221", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_884", "tel_code": "E_221", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_885", "tel_code": "E_221", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_886", "tel_code": "E_221", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 4| etat telephone : Micro rayure*/
{"dep_code": "D_887", "tel_code": "E_222", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_222", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_222", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_222", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 4| etat telephone : rayure*/
{"dep_code": "D_891", "tel_code": "E_223", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_223", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_223", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_223", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 4| etat telephone : Casse*/
{"dep_code": "D_895", "tel_code": "E_224", "tel_nom": "Intact", "etat_code": "60 €"},
{"dep_code": "D_896", "tel_code": "E_224", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_224", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_224", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Neo| etat telephone : Intact*/
{"dep_code": "D_899", "tel_code": "E_225", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_225", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_225", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_225", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Neo| etat telephone : Micro rayure*/
{"dep_code": "D_903", "tel_code": "E_226", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_226", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_226", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_226", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Neo| etat telephone : rayure*/
{"dep_code": "D_907", "tel_code": "E_227", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_227", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_227", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_227", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Neo| etat telephone : Casse*/
{"dep_code": "D_911", "tel_code": "E_228", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_228", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_228", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_228", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Lite| etat telephone : Intact*/
{"dep_code": "D_915", "tel_code": "E_229", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_229", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_229", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_229", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Lite| etat telephone : Micro rayure*/
{"dep_code": "D_919", "tel_code": "E_230", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_230", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_230", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_230", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Lite| etat telephone : rayure*/
{"dep_code": "D_887", "tel_code": "E_231", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_231", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_231", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_231", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Lite| etat telephone : Casse*/
{"dep_code": "D_891", "tel_code": "E_232", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_232", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_232", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_232", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Dual| etat telephone : Intact*/
{"dep_code": "D_895", "tel_code": "E_233", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_896", "tel_code": "E_233", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_233", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_233", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Dual| etat telephone : Micro rayure*/
{"dep_code": "D_899", "tel_code": "E_234", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_234", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_234", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_234", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Dual| etat telephone : rayure*/
{"dep_code": "D_903", "tel_code": "E_235", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_235", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_235", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_235", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 3 Dual| etat telephone : Casse*/
{"dep_code": "D_907", "tel_code": "E_236", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_236", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_236", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_236", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2 Lite| etat telephone : Intact*/
{"dep_code": "D_911", "tel_code": "E_237", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_237", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_237", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_237", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2 Lite| etat telephone : Micro rayure*/
{"dep_code": "D_915", "tel_code": "E_238", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_238", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_238", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_238", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2 Lite| etat telephone : rayure*/
{"dep_code": "D_919", "tel_code": "E_239", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_239", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_239", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_239", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2 Lite| etat telephone : Casse*/
{"dep_code": "D_887", "tel_code": "E_240", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_240", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_240", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_240", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2| etat telephone : Intact*/
{"dep_code": "D_891", "tel_code": "E_241", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_241", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_241", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_241", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2| etat telephone : Micro rayure*/
{"dep_code": "D_895", "tel_code": "E_242", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_896", "tel_code": "E_242", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_242", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_242", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2| etat telephone : rayure*/
{"dep_code": "D_899", "tel_code": "E_243", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_243", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_243", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_243", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note 2| etat telephone : Casse*/
{"dep_code": "D_903", "tel_code": "E_244", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_244", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_244", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_244", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note | etat telephone : Intact*/
{"dep_code": "D_907", "tel_code": "E_245", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_245", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_245", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_245", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note | etat telephone : Micro rayure*/
{"dep_code": "D_911", "tel_code": "E_246", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_246", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_246", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_246", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note | etat telephone : rayure*/
{"dep_code": "D_915", "tel_code": "E_247", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_247", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_247", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_247", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Note | etat telephone : Casse*/
{"dep_code": "D_919", "tel_code": "E_248", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_248", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_248", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_248", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A9| etat telephone : Intact*/
{"dep_code": "D_887", "tel_code": "E_249", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_249", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_249", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_249", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A9| etat telephone : Micro rayure*/
{"dep_code": "D_891", "tel_code": "E_250", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_250", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_250", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_250", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A9| etat telephone : rayure*/
{"dep_code": "D_895", "tel_code": "E_251", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_896", "tel_code": "E_251", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_251", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_251", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A9| etat telephone : Casse*/
{"dep_code": "D_899", "tel_code": "E_252", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_252", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_252", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_252", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8 (2018)| etat telephone : Intact*/
{"dep_code": "D_903", "tel_code": "E_253", "tel_nom": "Intact", "etat_code": "60 €"},
{"dep_code": "D_904", "tel_code": "E_253", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_253", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_253", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8 (2018)| etat telephone : Micro rayure*/
{"dep_code": "D_907", "tel_code": "E_254", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_254", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_254", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_254", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8 (2018)| etat telephone : rayure*/
{"dep_code": "D_911", "tel_code": "E_255", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_255", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_255", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_255", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8 (2018)| etat telephone : Casse*/
{"dep_code": "D_915", "tel_code": "E_256", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_256", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_256", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_256", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8| etat telephone : Intact*/
{"dep_code": "D_919", "tel_code": "E_257", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_257", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_257", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_257", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8| etat telephone : Micro rayure*/
{"dep_code": "D_887", "tel_code": "E_258", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_258", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_258", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_258", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8| etat telephone : rayure*/
{"dep_code": "D_891", "tel_code": "E_259", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_259", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_259", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_259", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A8| etat telephone : Casse*/
{"dep_code": "D_895", "tel_code": "E_260", "tel_nom": "Intact", "etat_code": "60y0 €"},
{"dep_code": "D_896", "tel_code": "E_260", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_260", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_260", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A70 | etat telephone : Intact*/
{"dep_code": "D_899", "tel_code": "E_261", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_261", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_261", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_261", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A70| etat telephone : Micro rayure*/
{"dep_code": "D_903", "tel_code": "E_262", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_262", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_262", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_262", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A70| etat telephone :  rayure*/
{"dep_code": "D_907", "tel_code": "E_263", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_263", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_263", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_263", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A70| etat telephone : Casse*/
{"dep_code": "D_911", "tel_code": "E_264", "tel_nom": "Intact", "etat_code": "600i €"},
{"dep_code": "D_912", "tel_code": "E_264", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_264", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_264", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Samsung Galaxy A7 (2018)| etat telephone : Intact*/
{"dep_code": "D_915", "tel_code": "E_265", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_265", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_265", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_265", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2018)| etat telephone : Micro rayure*/
{"dep_code": "D_919", "tel_code": "E_266", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_266", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_266", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_266", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Samsung Galaxy A7 (2018)| etat telephone : rayure*/
{"dep_code": "D_887", "tel_code": "E_267", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_267", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_267", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_267", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2018)| etat telephone : Casse*/
{"dep_code": "D_891", "tel_code": "E_268", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_268", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_268", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_268", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2017)| etat telephone : Intact*/
{"dep_code": "D_895", "tel_code": "E_269", "tel_nom": "Intact", "etat_code": "600o €"},
{"dep_code": "D_896", "tel_code": "E_269", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_269", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_269", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2017)| etat telephone : Micro rayure*/
{"dep_code": "D_899", "tel_code": "E_270", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_270", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_270", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_270", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2017)| etat telephone : rayure*/
{"dep_code": "D_903", "tel_code": "E_271", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_271", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_271", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_271", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2017)| etat telephone : Casse*/
{"dep_code": "D_907", "tel_code": "E_272", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_272", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_272", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_272", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2016)| etat telephone : Intact*/
{"dep_code": "D_911", "tel_code": "E_273", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_273", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_273", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_273", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2016)| etat telephone : Micro rayure*/
{"dep_code": "D_915", "tel_code": "E_274", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_274", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_274", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_274", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2016)| etat telephone : rayure*/
{"dep_code": "D_919", "tel_code": "E_275", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_275", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_275", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_275", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 (2016)| etat telephone : Casse*/
{"dep_code": "D_887", "tel_code": "E_276", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_276", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_276", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_276", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 | etat telephone : Intact*/
{"dep_code": "D_891", "tel_code": "E_277", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_277", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_277", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_277", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 | etat telephone : Micro rayure*/
{"dep_code": "D_895", "tel_code": "E_278", "tel_nom": "Intact", "etat_code": "600m €"},
{"dep_code": "D_896", "tel_code": "E_278", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_278", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_278", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 | etat telephone : rayure*/
{"dep_code": "D_899", "tel_code": "E_279", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_279", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_279", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_279", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A7 | etat telephone : Casse*/
{"dep_code": "D_903", "tel_code": "E_280", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_280", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_280", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_280", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 Plus | etat telephone : Intact*/
{"dep_code": "D_907", "tel_code": "E_281", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_281", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_281", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_281", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 Plus | etat telephone : Micro rayure*/
{"dep_code": "D_911", "tel_code": "E_282", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_282", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_282", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_282", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 Plus | etat telephone : rayure*/
{"dep_code": "D_915", "tel_code": "E_283", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_283", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_283", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_283", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 Plus | etat telephone : Casse*/
{"dep_code": "D_919", "tel_code": "E_284", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_284", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_284", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_284", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 | etat telephone : Intact*/
{"dep_code": "D_887", "tel_code": "E_285", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_285", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_285", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_285", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 | etat telephone : Micro rayure*/
{"dep_code": "D_891", "tel_code": "E_286", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_286", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_286", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_286", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 | etat telephone : rayure*/
{"dep_code": "D_895", "tel_code": "E_287", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_896", "tel_code": "E_287", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_287", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_287", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A6 | etat telephone : Casse*/
{"dep_code": "D_899", "tel_code": "E_288", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_288", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_288", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_288", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A50 | etat telephone : Intact*/
{"dep_code": "D_903", "tel_code": "E_289", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_289", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_289", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_289", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A50 | etat telephone : Micro rayure*/
{"dep_code": "D_907", "tel_code": "E_290", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_290", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_290", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_290", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A50 | etat telephone : Rayure*/
{"dep_code": "D_911", "tel_code": "E_291", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_291", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_291", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_291", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A50 | etat telephone : Casse*/
{"dep_code": "D_915", "tel_code": "E_292", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_292", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_292", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_292", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2017) | etat telephone : Intact*/
{"dep_code": "D_919", "tel_code": "E_293", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_293", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_293", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_293", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2017) | etat telephone : Micro rayure*/
{"dep_code": "D_887", "tel_code": "E_294", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_294", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_294", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_294", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2017) | etat telephone : rayure*/
{"dep_code": "D_891", "tel_code": "E_295", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_295", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_295", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_295", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2017) | etat telephone : Casse*/
{"dep_code": "D_895", "tel_code": "E_296", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_896", "tel_code": "E_296", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_296", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_296", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2016) | etat telephone : Intact*/
{"dep_code": "D_899", "tel_code": "E_297", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_297", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_297", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_297", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2016) | etat telephone : Micro rayure*/
{"dep_code": "D_903", "tel_code": "E_298", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_298", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_298", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_298", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2016) | etat telephone : rayure*/
{"dep_code": "D_907", "tel_code": "E_299", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_299", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_299", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_299", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2016) | etat telephone : Casse*/
{"dep_code": "D_911", "tel_code": "E_300", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_300", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_300", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_300", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2015) | etat telephone : Intact*/
{"dep_code": "D_915", "tel_code": "E_301", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_301", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_301", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_301", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2015) | etat telephone : Micro rayure*/
{"dep_code": "D_919", "tel_code": "E_302", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_302", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_302", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_302", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2015) | etat telephone : Rayure*/
{"dep_code": "D_887", "tel_code": "E_303", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_888", "tel_code": "E_303", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_889", "tel_code": "E_303", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_890", "tel_code": "E_303", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A5 (2015) | etat telephone : Casse*/
{"dep_code": "D_891", "tel_code": "E_304", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_892", "tel_code": "E_304", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_893", "tel_code": "E_304", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_894", "tel_code": "E_304", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A40 | etat telephone : Intact*/
{"dep_code": "D_895", "tel_code": "E_305", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_896", "tel_code": "E_305", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_897", "tel_code": "E_305", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_898", "tel_code": "E_305", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : Samsung Galaxy A40 | etat telephone : Micro rayure*/
{"dep_code": "D_899", "tel_code": "E_306", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_900", "tel_code": "E_306", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_901", "tel_code": "E_306", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_902", "tel_code": "E_306", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A40 | etat telephone : Rayure*/
{"dep_code": "D_903", "tel_code": "E_307", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_904", "tel_code": "E_307", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_905", "tel_code": "E_307", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_906", "tel_code": "E_307", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A40 | etat telephone : Casse*/
{"dep_code": "D_907", "tel_code": "E_308", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_908", "tel_code": "E_308", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_909", "tel_code": "E_308", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_910", "tel_code": "E_308", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A30 | etat telephone : Intact*/
{"dep_code": "D_911", "tel_code": "E_309", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_912", "tel_code": "E_309", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_913", "tel_code": "E_309", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_914", "tel_code": "E_309", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A30 | etat telephone : Micro rayure*/
{"dep_code": "D_915", "tel_code": "E_310", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_916", "tel_code": "E_310", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_917", "tel_code": "E_310", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_918", "tel_code": "E_310", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A30 | etat telephone : Rayure*/
{"dep_code": "D_919", "tel_code": "E_311", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_920", "tel_code": "E_311", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_921", "tel_code": "E_311", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_922", "tel_code": "E_311", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A30 | etat telephone : Casse*/
{"dep_code": "D_923", "tel_code": "E_312", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_924", "tel_code": "E_312", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_925", "tel_code": "E_312", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_926", "tel_code": "E_312", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2017) | etat telephone : Intact*/
{"dep_code": "D_927", "tel_code": "E_313", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_928", "tel_code": "E_313", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_929", "tel_code": "E_313", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_930", "tel_code": "E_313", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2017) | etat telephone : Micro rayure*/
{"dep_code": "D_931", "tel_code": "E_314", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_932", "tel_code": "E_314", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_933", "tel_code": "E_314", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_934", "tel_code": "E_314", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2017) | etat telephone : Rayure*/
{"dep_code": "D_935", "tel_code": "E_315", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_936", "tel_code": "E_315", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_937", "tel_code": "E_315", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_938", "tel_code": "E_315", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2017) | etat telephone : Casse*/
{"dep_code": "D_939", "tel_code": "E_316", "tel_nom": "Intact", "etat_code": "600p €"},
{"dep_code": "D_940", "tel_code": "E_316", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_941", "tel_code": "E_316", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_942", "tel_code": "E_316", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2016) | etat telephone : Intact*/
{"dep_code": "D_943", "tel_code": "E_317", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_944", "tel_code": "E_317", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_945", "tel_code": "E_317", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_946", "tel_code": "E_317", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2016) | etat telephone : Micro rayure*/
{"dep_code": "D_947", "tel_code": "E_318", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_948", "tel_code": "E_318", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_949", "tel_code": "E_318", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_950", "tel_code": "E_318", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2016) | etat telephone : rayure*/
{"dep_code": "D_951", "tel_code": "E_319", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_952", "tel_code": "E_319", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_953", "tel_code": "E_319", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_954", "tel_code": "E_319", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3 (2016) | etat telephone : Casse*/
{"dep_code": "D_955", "tel_code": "E_320", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_956", "tel_code": "E_320", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_957", "tel_code": "E_320", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_958", "tel_code": "E_320", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3  | etat telephone : Intact*/
{"dep_code": "D_959", "tel_code": "E_321", "tel_nom": "Intact", "etat_code": "600i €"},
{"dep_code": "D_960", "tel_code": "E_321", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_961", "tel_code": "E_321", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_962", "tel_code": "E_321", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3  | etat telephone : Micro rayure*/
{"dep_code": "D_963", "tel_code": "E_322", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_964", "tel_code": "E_322", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_965", "tel_code": "E_322", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_966", "tel_code": "E_322", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3  | etat telephone : Rayure*/
{"dep_code": "D_967", "tel_code": "E_323", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_968", "tel_code": "E_323", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_969", "tel_code": "E_323", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_970", "tel_code": "E_323", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy A3  | etat telephone : Casse*/
{"dep_code": "D_971", "tel_code": "E_324", "tel_nom": "Intact", "etat_code": "600k €"},
{"dep_code": "D_972", "tel_code": "E_324", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_973", "tel_code": "E_324", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_974", "tel_code": "E_324", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 4  | etat telephone : Intact*/
{"dep_code": "D_975", "tel_code": "E_325", "tel_nom": "Intact", "etat_code": "600p €"},
{"dep_code": "D_976", "tel_code": "E_325", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_977", "tel_code": "E_325", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_978", "tel_code": "E_325", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 4  | etat telephone : Micro rayure*/
{"dep_code": "D_979", "tel_code": "E_326", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_980", "tel_code": "E_326", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_981", "tel_code": "E_326", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_982", "tel_code": "E_326", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 4  | etat telephone : Rayure*/
{"dep_code": "D_983", "tel_code": "E_327", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_984", "tel_code": "E_327", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_985", "tel_code": "E_327", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_986", "tel_code": "E_327", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 4  | etat telephone : Casse*/
{"dep_code": "D_987", "tel_code": "E_328", "tel_nom": "Intact", "etat_code": "60 €"},
{"dep_code": "D_988", "tel_code": "E_328", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_989", "tel_code": "E_328", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_990", "tel_code": "E_328", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3 S7270  | etat telephone : Intact*/
{"dep_code": "D_991", "tel_code": "E_329", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_992", "tel_code": "E_329", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_993", "tel_code": "E_329", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_994", "tel_code": "E_329", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3 S7270  | etat telephone : Micro rayure*/
{"dep_code": "D_995", "tel_code": "E_330", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_996", "tel_code": "E_330", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_997", "tel_code": "E_330", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_998", "tel_code": "E_330", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3 S7270  | etat telephone : rayure*/
{"dep_code": "D_999", "tel_code": "E_331", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1000", "tel_code": "E_331", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1001", "tel_code": "E_331", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1002", "tel_code": "E_331", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3 S7270  | etat telephone : Casse*/
{"dep_code": "D_1003", "tel_code": "E_332", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1004", "tel_code": "E_332", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1005", "tel_code": "E_332", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1006", "tel_code": "E_332", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3  | etat telephone : Intact*/
{"dep_code": "D_1007", "tel_code": "E_333", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1008", "tel_code": "E_333", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1009", "tel_code": "E_333", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1010", "tel_code": "E_333", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3 | etat telephone : Micro rayure*/
{"dep_code": "D_1011", "tel_code": "E_334", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1012", "tel_code": "E_334", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1013", "tel_code": "E_334", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1014", "tel_code": "E_334", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3 | etat telephone : rayure*/
{"dep_code": "D_1015", "tel_code": "E_335", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1016", "tel_code": "E_335", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1017", "tel_code": "E_335", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1018", "tel_code": "E_335", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 3 | etat telephone : Casse*/
{"dep_code": "D_1019", "tel_code": "E_336", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1020", "tel_code": "E_336", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1021", "tel_code": "E_336", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1022", "tel_code": "E_336", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 2 | etat telephone : Intact*/
{"dep_code": "D_1023", "tel_code": "E_337", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1024", "tel_code": "E_337", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1025", "tel_code": "E_337", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1026", "tel_code": "E_337", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 2 | etat telephone : Micro Rayure*/
{"dep_code": "D_1027", "tel_code": "E_338", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1028", "tel_code": "E_338", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1029", "tel_code": "E_338", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1030", "tel_code": "E_338", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 2 | etat telephone : Rayure*/
{"dep_code": "D_1031", "tel_code": "E_339", "tel_nom": "Intact", "etat_code": "600t €"},
{"dep_code": "D_1032", "tel_code": "E_339", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1033", "tel_code": "E_339", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1034", "tel_code": "E_339", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace 2 | etat telephone : Casse*/
{"dep_code": "D_1035", "tel_code": "E_340", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1036", "tel_code": "E_340", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1037", "tel_code": "E_340", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1038", "tel_code": "E_340", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace | etat telephone : Intact*/
{"dep_code": "D_1039", "tel_code": "E_341", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1040", "tel_code": "E_341", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1041", "tel_code": "E_341", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1042", "tel_code": "E_341", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace | etat telephone : Micro rayure*/
{"dep_code": "D_1043", "tel_code": "E_342", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1044", "tel_code": "E_342", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1045", "tel_code": "E_342", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1046", "tel_code": "E_342", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace | etat telephone : Rayure*/
{"dep_code": "D_1047", "tel_code": "E_343", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1048", "tel_code": "E_343", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1049", "tel_code": "E_343", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1050", "tel_code": "E_343", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy Ace | etat telephone : Casse*/
{"dep_code": "D_1051", "tel_code": "E_344", "tel_nom": "Intact", "etat_code": "600^ €"},
{"dep_code": "D_1052", "tel_code": "E_344", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1053", "tel_code": "E_344", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1054", "tel_code": "E_344", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2017 | etat telephone : Intact*/
{"dep_code": "D_1055", "tel_code": "E_345", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1056", "tel_code": "E_345", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1057", "tel_code": "E_345", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1058", "tel_code": "E_345", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2017 | etat telephone : Micro rayure*/
{"dep_code": "D_1059", "tel_code": "E_346", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1060", "tel_code": "E_346", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1061", "tel_code": "E_346", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1062", "tel_code": "E_346", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2017 | etat telephone : rayure*/
{"dep_code": "D_1063", "tel_code": "E_347", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1064", "tel_code": "E_347", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1065", "tel_code": "E_347", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1066", "tel_code": "E_347", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2017 | etat telephone : Casse*/
{"dep_code": "D_1067", "tel_code": "E_348", "tel_nom": "Intact", "etat_code": "600ip €"},
{"dep_code": "D_1068", "tel_code": "E_348", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1069", "tel_code": "E_348", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1070", "tel_code": "E_348", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2016 | etat telephone : Intact*/
{"dep_code": "D_1071", "tel_code": "E_349", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1072", "tel_code": "E_349", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1073", "tel_code": "E_349", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1074", "tel_code": "E_349", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2016 | etat telephone : Micro rayure*/
{"dep_code": "D_1075", "tel_code": "E_350", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1076", "tel_code": "E_350", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1077", "tel_code": "E_350", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1078", "tel_code": "E_350", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2016 | etat telephone : Rayure*/
{"dep_code": "D_1079", "tel_code": "E_351", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1080", "tel_code": "E_351", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1081", "tel_code": "E_351", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1082", "tel_code": "E_351", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J7 2016 | etat telephone : Casse*/
{"dep_code": "D_1083", "tel_code": "E_352", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1084", "tel_code": "E_352", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1085", "tel_code": "E_352", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1086", "tel_code": "E_352", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J6 2018 | etat telephone : Intact*/
{"dep_code": "D_1087", "tel_code": "E_353", "tel_nom": "Intact", "etat_code": "6 €"},
{"dep_code": "D_1088", "tel_code": "E_353", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1089", "tel_code": "E_353", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1090", "tel_code": "E_353", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J6 2018 | etat telephone : Micro rayure*/
{"dep_code": "D_1091", "tel_code": "E_354", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1092", "tel_code": "E_354", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1093", "tel_code": "E_354", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1096", "tel_code": "E_354", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J6 2018 | etat telephone : Rayure*/
{"dep_code": "D_1097", "tel_code": "E_355", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1098", "tel_code": "E_355", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1099", "tel_code": "E_355", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1100", "tel_code": "E_355", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J6 2018 | etat telephone : Casse*/
{"dep_code": "D_1101", "tel_code": "E_356", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1102", "tel_code": "E_356", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1103", "tel_code": "E_356", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1104", "tel_code": "E_356", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2017 | etat telephone : Intact*/
{"dep_code": "D_1105", "tel_code": "E_357", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1106", "tel_code": "E_357", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1107", "tel_code": "E_357", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1108", "tel_code": "E_357", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2017 | etat telephone : Micro rayure*/
{"dep_code": "D_1109", "tel_code": "E_358", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1110", "tel_code": "E_358", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1111", "tel_code": "E_358", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1112", "tel_code": "E_358", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2017 | etat telephone : Rayure*/
{"dep_code": "D_1113", "tel_code": "E_359", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1114", "tel_code": "E_359", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1115", "tel_code": "E_359", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1116", "tel_code": "E_359", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2017 | etat telephone : Casse*/
{"dep_code": "D_1117", "tel_code": "E_360", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1118", "tel_code": "E_360", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1119", "tel_code": "E_360", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1120", "tel_code": "E_360", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2016 | etat telephone : Intact*/
{"dep_code": "D_1121", "tel_code": "E_361", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1122", "tel_code": "E_361", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1123", "tel_code": "E_361", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1124", "tel_code": "E_361", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2016 | etat telephone : Micro rayure*/
{"dep_code": "D_1125", "tel_code": "E_362", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1126", "tel_code": "E_362", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1127", "tel_code": "E_362", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1128", "tel_code": "E_362", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2016 | etat telephone : rayure*/
{"dep_code": "D_1129", "tel_code": "E_363", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1130", "tel_code": "E_363", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1131", "tel_code": "E_363", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1132", "tel_code": "E_363", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J5 2016 | etat telephone : Casse*/
{"dep_code": "D_1133", "tel_code": "E_364", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1134", "tel_code": "E_364", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1135", "tel_code": "E_364", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1136", "tel_code": "E_364", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J4 2018 | etat telephone : Intact*/
{"dep_code": "D_1137", "tel_code": "E_365", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1138", "tel_code": "E_365", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1139", "tel_code": "E_365", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1140", "tel_code": "E_365", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J4 2018 | etat telephone : Micro rayure*/
{"dep_code": "D_1141", "tel_code": "E_366", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1142", "tel_code": "E_366", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1143", "tel_code": "E_366", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1144", "tel_code": "E_366", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J4 2018 | etat telephone : rayure*/
{"dep_code": "D_1145", "tel_code": "E_367", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1146", "tel_code": "E_367", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1147", "tel_code": "E_367", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1148", "tel_code": "E_367", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J4 2018 | etat telephone : Casse*/
{"dep_code": "D_1149", "tel_code": "E_368", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1150", "tel_code": "E_368", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1151", "tel_code": "E_368", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1152", "tel_code": "E_368", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2017 | etat telephone : Intact*/
{"dep_code": "D_1153", "tel_code": "E_369", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1154", "tel_code": "E_369", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1155", "tel_code": "E_369", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1156", "tel_code": "E_369", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2017 | etat telephone : Micro rayure*/
{"dep_code": "D_1157", "tel_code": "E_370", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1158", "tel_code": "E_370", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1159", "tel_code": "E_370", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1160", "tel_code": "E_370", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2017 | etat telephone : Rayure*/
{"dep_code": "D_1161", "tel_code": "E_371", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1162", "tel_code": "E_371", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1163", "tel_code": "E_371", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1164", "tel_code": "E_371", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2017 | etat telephone : Casse*/
{"dep_code": "D_1165", "tel_code": "E_372", "tel_nom": "Intact", "etat_code": "600r €"},
{"dep_code": "D_1166", "tel_code": "E_372", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1167", "tel_code": "E_372", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1168", "tel_code": "E_372", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2016 | etat telephone : Intact*/
{"dep_code": "D_1169", "tel_code": "E_373", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1170", "tel_code": "E_373", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1171", "tel_code": "E_373", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1172", "tel_code": "E_373", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2016 | etat telephone : Micro rayure*/
{"dep_code": "D_1173", "tel_code": "E_374", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1174", "tel_code": "E_374", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1175", "tel_code": "E_374", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1176", "tel_code": "E_374", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2016 | etat telephone : rayure*/
{"dep_code": "D_1177", "tel_code": "E_375", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1178", "tel_code": "E_375", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1179", "tel_code": "E_375", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1180", "tel_code": "E_375", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J3 2016 | etat telephone : Casse*/
{"dep_code": "D_1181", "tel_code": "E_376", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1182", "tel_code": "E_376", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1183", "tel_code": "E_376", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1184", "tel_code": "E_376", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1 2016 | etat telephone : Intact*/
{"dep_code": "D_1185", "tel_code": "E_377", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1186", "tel_code": "E_377", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1187", "tel_code": "E_377", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1188", "tel_code": "E_377", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1 2016 | etat telephone : Micro rayure*/
{"dep_code": "D_1189", "tel_code": "E_378", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1190", "tel_code": "E_378", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1191", "tel_code": "E_378", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1192", "tel_code": "E_378", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1 2016 | etat telephone : Rayure*/
{"dep_code": "D_1193", "tel_code": "E_379", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1194", "tel_code": "E_379", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1195", "tel_code": "E_379", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1196", "tel_code": "E_379", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1 2016 | etat telephone : Casse*/
{"dep_code": "D_1197", "tel_code": "E_380", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1198", "tel_code": "E_380", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1199", "tel_code": "E_380", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1200", "tel_code": "E_380", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1  | etat telephone : Intact*/
{"dep_code": "D_1201", "tel_code": "E_381", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1202", "tel_code": "E_381", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1203", "tel_code": "E_381", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1204", "tel_code": "E_381", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1  | etat telephone : Micro rayure */
{"dep_code": "D_1205", "tel_code": "E_382", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1206", "tel_code": "E_382", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1207", "tel_code": "E_382", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1208", "tel_code": "E_382", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1  | etat telephone : Rayure*/
{"dep_code": "D_1209", "tel_code": "E_383", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1210", "tel_code": "E_383", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1211", "tel_code": "E_383", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1212", "tel_code": "E_383", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Samsung Galaxy J1  | etat telephone : Casse*/
{"dep_code": "D_1213", "tel_code": "E_384", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1214", "tel_code": "E_384", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1215", "tel_code": "E_384", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1216", "tel_code": "E_384", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Plus | etat telephone : Intact*/
{"dep_code": "D_1217", "tel_code": "E_385", "tel_nom": "Intact", "etat_code": "600hu €"},
{"dep_code": "D_1218", "tel_code": "E_385", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1219", "tel_code": "E_385", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1220", "tel_code": "E_385", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Plus | etat telephone : Micro rayure*/
{"dep_code": "D_1221", "tel_code": "E_386", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1222", "tel_code": "E_386", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1223", "tel_code": "E_386", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1224", "tel_code": "E_386", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Plus | etat telephone : Rayure*/
{"dep_code": "D_1225", "tel_code": "E_387", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1226", "tel_code": "E_387", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1227", "tel_code": "E_387", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1228", "tel_code": "E_387", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Plus | etat telephone : Casse*/
{"dep_code": "D_1229", "tel_code": "E_388", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1230", "tel_code": "E_388", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1231", "tel_code": "E_388", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1232", "tel_code": "E_388", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Lite | etat telephone : Intact*/
{"dep_code": "D_1233", "tel_code": "E_389", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1234", "tel_code": "E_389", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1235", "tel_code": "E_389", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1236", "tel_code": "E_389", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Lite | etat telephone : Micro rayure*/
{"dep_code": "D_1237", "tel_code": "E_390", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1238", "tel_code": "E_390", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1239", "tel_code": "E_390", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1240", "tel_code": "E_390", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Lite | etat telephone : Rayure*/
{"dep_code": "D_1241", "tel_code": "E_391", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1242", "tel_code": "E_391", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1243", "tel_code": "E_391", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1244", "tel_code": "E_391", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9 Lite | etat telephone : Casse*/
{"dep_code": "D_1245", "tel_code": "E_392", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1246", "tel_code": "E_392", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1247", "tel_code": "E_392", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1248", "tel_code": "E_392", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9  | etat telephone : Intact*/
{"dep_code": "D_1249", "tel_code": "E_393", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1250", "tel_code": "E_393", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1251", "tel_code": "E_393", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1252", "tel_code": "E_393", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9  | etat telephone : Micro rayure*/
{"dep_code": "D_1253", "tel_code": "E_394", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1254", "tel_code": "E_394", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1255", "tel_code": "E_394", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1256", "tel_code": "E_394", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9  | etat telephone : Rayure*/
{"dep_code": "D_1257", "tel_code": "E_395", "tel_nom": "Intact", "etat_code": "600ui €"},
{"dep_code": "D_1258", "tel_code": "E_395", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1259", "tel_code": "E_395", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1260", "tel_code": "E_395", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p9  | etat telephone : Casse*/
{"dep_code": "D_1261", "tel_code": "E_396", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1262", "tel_code": "E_396", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1263", "tel_code": "E_396", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1264", "tel_code": "E_396", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p8 lite (2017) | etat telephone : Intact*/
{"dep_code": "D_1265", "tel_code": "E_397", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1266", "tel_code": "E_397", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1267", "tel_code": "E_397", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1268", "tel_code": "E_397", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p8 lite (2017) | etat telephone : Micro rayure*/
{"dep_code": "D_1269", "tel_code": "E_398", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1270", "tel_code": "E_398", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1271", "tel_code": "E_398", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1272", "tel_code": "E_398", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p8 lite (2017) | etat telephone : rayure*/
{"dep_code": "D_1273", "tel_code": "E_399", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1274", "tel_code": "E_399", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1275", "tel_code": "E_399", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1276", "tel_code": "E_399", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p8 lite (2017) | etat telephone : Casse*/
{"dep_code": "D_1277", "tel_code": "E_400", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1278", "tel_code": "E_400", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1279", "tel_code": "E_400", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1280", "tel_code": "E_400", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p8  | etat telephone : Intact*/
{"dep_code": "D_1281", "tel_code": "E_401", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1282", "tel_code": "E_401", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1283", "tel_code": "E_401", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1284", "tel_code": "E_401", "tel_nom": "Cassé", "etat_code": "550 €"},

/* telephone : huawei p8  | etat telephone : Micro rayure*/
{"dep_code": "D_1285", "tel_code": "E_402", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1286", "tel_code": "E_402", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1287", "tel_code": "E_402", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1288", "tel_code": "E_402", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p8  | etat telephone : rayure*/
{"dep_code": "D_1289", "tel_code": "E_403", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1290", "tel_code": "E_403", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1291", "tel_code": "E_403", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1292", "tel_code": "E_403", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p8  | etat telephone : Casse*/
{"dep_code": "D_1293", "tel_code": "E_404", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_1294", "tel_code": "E_404", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1295", "tel_code": "E_404", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1296", "tel_code": "E_404", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Pro  | etat telephone : Intact*/
{"dep_code": "D_1297", "tel_code": "E_405", "tel_nom": "Intact", "etat_code": "600ty €"},
{"dep_code": "D_1298", "tel_code": "E_405", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1299", "tel_code": "E_405", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1300", "tel_code": "E_405", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Pro  | etat telephone : Micro rayure*/
{"dep_code": "D_1301", "tel_code": "E_406", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1302", "tel_code": "E_406", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1303", "tel_code": "E_406", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1304", "tel_code": "E_406", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Pro  | etat telephone : Rayure*/
{"dep_code": "D_1305", "tel_code": "E_407", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1306", "tel_code": "E_407", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1307", "tel_code": "E_407", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1308", "tel_code": "E_407", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Pro  | etat telephone : Casse*/
{"dep_code": "D_1309", "tel_code": "E_408", "tel_nom": "Intact", "etat_code": "600t €"},
{"dep_code": "D_1310", "tel_code": "E_408", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1311", "tel_code": "E_408", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1312", "tel_code": "E_408", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Lite  | etat telephone : Intact*/
{"dep_code": "D_1313", "tel_code": "E_409", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1314", "tel_code": "E_409", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1315", "tel_code": "E_409", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1316", "tel_code": "E_409", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Lite | etat telephone : Micro rayure*/
{"dep_code": "D_1317", "tel_code": "E_410", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1318", "tel_code": "E_410", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1319", "tel_code": "E_410", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1320", "tel_code": "E_410", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Lite | etat telephone : Rayure*/
{"dep_code": "D_1321", "tel_code": "E_411", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1322", "tel_code": "E_411", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1323", "tel_code": "E_411", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1324", "tel_code": "E_411", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 Lite| etat telephone : Casse*/
{"dep_code": "D_1325", "tel_code": "E_412", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1226", "tel_code": "E_412", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1227", "tel_code": "E_412", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1228", "tel_code": "E_412", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30  | etat telephone : Intact*/
{"dep_code": "D_1229", "tel_code": "E_413", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1230", "tel_code": "E_413", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1231", "tel_code": "E_413", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1232", "tel_code": "E_413", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 | etat telephone : Micro rayure*/
{"dep_code": "D_1233", "tel_code": "E_414", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1234", "tel_code": "E_414", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1235", "tel_code": "E_414", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1236", "tel_code": "E_414", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 | etat telephone : rayure*/
{"dep_code": "D_1237", "tel_code": "E_415", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1238", "tel_code": "E_415", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1239", "tel_code": "E_415", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1240", "tel_code": "E_415", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p30 | etat telephone : Casse*/
{"dep_code": "D_1241", "tel_code": "E_416", "tel_nom": "Intact", "etat_code": "600t €"},
{"dep_code": "D_1242", "tel_code": "E_416", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1243", "tel_code": "E_416", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1244", "tel_code": "E_416", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Pro  | etat telephone : Intact*/
{"dep_code": "D_1245", "tel_code": "E_417", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1246", "tel_code": "E_417", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1247", "tel_code": "E_417", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1248", "tel_code": "E_417", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Pro  | etat telephone : Micro rayure*/
{"dep_code": "D_1249", "tel_code": "E_418", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1250", "tel_code": "E_418", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1251", "tel_code": "E_418", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1252", "tel_code": "E_418", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Pro  | etat telephone : rayure*/
{"dep_code": "D_1253", "tel_code": "E_419", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1254", "tel_code": "E_419", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1255", "tel_code": "E_419", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1256", "tel_code": "E_419", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Pro  | etat telephone : Casse*/
{"dep_code": "D_1257", "tel_code": "E_420", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1258", "tel_code": "E_420", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1259", "tel_code": "E_420", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1260", "tel_code": "E_420", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Lite | etat telephone : Intact*/
{"dep_code": "D_1261", "tel_code": "E_421", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1262", "tel_code": "E_421", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1263", "tel_code": "E_421", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1264", "tel_code": "E_421", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Lite | etat telephone : Micro rayure*/
{"dep_code": "D_1265", "tel_code": "E_422", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1266", "tel_code": "E_422", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1267", "tel_code": "E_422", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1268", "tel_code": "E_422", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Lite | etat telephone : Rayure*/
{"dep_code": "D_1269", "tel_code": "E_423", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1270", "tel_code": "E_423", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1271", "tel_code": "E_423", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1272", "tel_code": "E_423", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 Lite | etat telephone : Casse*/
{"dep_code": "D_1273", "tel_code": "E_424", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1274", "tel_code": "E_424", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1275", "tel_code": "E_424", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1276", "tel_code": "E_424", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 | etat telephone : Intact*/
{"dep_code": "D_1277", "tel_code": "E_425", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1278", "tel_code": "E_425", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1279", "tel_code": "E_425", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1280", "tel_code": "E_425", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 | etat telephone : Micro rayure*/
{"dep_code": "D_1281", "tel_code": "E_426", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1282", "tel_code": "E_426", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1283", "tel_code": "E_426", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1284", "tel_code": "E_426", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 | etat telephone : Rayure*/
{"dep_code": "D_1285", "tel_code": "E_427", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1286", "tel_code": "E_427", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1287", "tel_code": "E_427", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1288", "tel_code": "E_427", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p20 | etat telephone : Casse*/
{"dep_code": "D_1289", "tel_code": "E_428", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1290", "tel_code": "E_428", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1291", "tel_code": "E_428", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1292", "tel_code": "E_428", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Plus | etat telephone : Intact*/
{"dep_code": "D_1293", "tel_code": "E_429", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1294", "tel_code": "E_429", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1295", "tel_code": "E_429", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1296", "tel_code": "E_429", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Plus | etat telephone : Micro rayure*/
{"dep_code": "D_1297", "tel_code": "E_430", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1298", "tel_code": "E_430", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1299", "tel_code": "E_430", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1300", "tel_code": "E_430", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Plus | etat telephone : rayure*/
{"dep_code": "D_1301", "tel_code": "E_431", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1302", "tel_code": "E_431", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1303", "tel_code": "E_431", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1304", "tel_code": "E_431", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Plus | etat telephone : Casse*/
{"dep_code": "D_1305", "tel_code": "E_432", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1306", "tel_code": "E_432", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1307", "tel_code": "E_432", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1308", "tel_code": "E_432", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Lite | etat telephone : Intact*/
{"dep_code": "D_1309", "tel_code": "E_433", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1310", "tel_code": "E_433", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1311", "tel_code": "E_433", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1312", "tel_code": "E_433", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Lite | etat telephone : Micro rayure*/
{"dep_code": "D_1313", "tel_code": "E_434", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1314", "tel_code": "E_434", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1315", "tel_code": "E_434", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1316", "tel_code": "E_434", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Lite | etat telephone : Rayure*/
{"dep_code": "D_1317", "tel_code": "E_435", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1318", "tel_code": "E_435", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1319", "tel_code": "E_435", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1320", "tel_code": "E_435", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 Lite | etat telephone : Casse*/
{"dep_code": "D_1321", "tel_code": "E_436", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1322", "tel_code": "E_436", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1323", "tel_code": "E_436", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1324", "tel_code": "E_436", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 | etat telephone : Intact*/
{"dep_code": "D_1325", "tel_code": "E_437", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1326", "tel_code": "E_437", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1327", "tel_code": "E_437", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1328", "tel_code": "E_437", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 | etat telephone : Micro rayure*/
{"dep_code": "D_1329", "tel_code": "E_438", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1330", "tel_code": "E_438", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1331", "tel_code": "E_438", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1332", "tel_code": "E_438", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 | etat telephone : Rayure*/
{"dep_code": "D_1333", "tel_code": "E_439", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1334", "tel_code": "E_439", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1335", "tel_code": "E_439", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1336", "tel_code": "E_439", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p10 | etat telephone : Casse*/
{"dep_code": "D_1337", "tel_code": "E_440", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1338", "tel_code": "E_440", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1339", "tel_code": "E_440", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1340", "tel_code": "E_440", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart+ | etat telephone : Intact*/
{"dep_code": "D_1341", "tel_code": "E_441", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1342", "tel_code": "E_441", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1343", "tel_code": "E_441", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1344", "tel_code": "E_441", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart+ | etat telephone : Micro rayure*/
{"dep_code": "D_1345", "tel_code": "E_442", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1346", "tel_code": "E_442", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1347", "tel_code": "E_442", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1348", "tel_code": "E_442", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart+ | etat telephone : rayure*/
{"dep_code": "D_1349", "tel_code": "E_443", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1350", "tel_code": "E_443", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1351", "tel_code": "E_443", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1352", "tel_code": "E_443", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart+ | etat telephone : Casse*/
{"dep_code": "D_1353", "tel_code": "E_444", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1354", "tel_code": "E_444", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1355", "tel_code": "E_444", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1356", "tel_code": "E_444", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart | etat telephone : Intact*/
{"dep_code": "D_1357", "tel_code": "E_445", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1358", "tel_code": "E_445", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1359", "tel_code": "E_445", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1360", "tel_code": "E_445", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart | etat telephone : Micro rayure*/
{"dep_code": "D_1361", "tel_code": "E_446", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1362", "tel_code": "E_446", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1363", "tel_code": "E_446", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1364", "tel_code": "E_446", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart | etat telephone : rayure*/
{"dep_code": "D_1365", "tel_code": "E_447", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1366", "tel_code": "E_447", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1367", "tel_code": "E_447", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1368", "tel_code": "E_447", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei p smart | etat telephone : Casse*/
{"dep_code": "D_1369", "tel_code": "E_448", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1370", "tel_code": "E_448", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1371", "tel_code": "E_448", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1372", "tel_code": "E_448", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei porsche Design Mate Rs | etat telephone : Intact*/
{"dep_code": "D_1373", "tel_code": "E_449", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1374", "tel_code": "E_449", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1375", "tel_code": "E_449", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1376", "tel_code": "E_449", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei porsche Design Mate Rs | etat telephone : Micro rayure*/
{"dep_code": "D_1377", "tel_code": "E_450", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1378", "tel_code": "E_450", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1379", "tel_code": "E_450", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1380", "tel_code": "E_450", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei porsche Design Mate Rs | etat telephone : Rayure*/
{"dep_code": "D_1381", "tel_code": "E_451", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1382", "tel_code": "E_451", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1383", "tel_code": "E_451", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1384", "tel_code": "E_451", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei porsche Design Mate Rs | etat telephone : Cassé*/
{"dep_code": "D_1385", "tel_code": "E_452", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1386", "tel_code": "E_452", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1387", "tel_code": "E_452", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1388", "tel_code": "E_452", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate S | etat telephone : Intact*/
{"dep_code": "D_1389", "tel_code": "E_453", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1390", "tel_code": "E_453", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1391", "tel_code": "E_453", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1392", "tel_code": "E_453", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate S | etat telephone : Micro rayure*/
{"dep_code": "D_1393", "tel_code": "E_454", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1394", "tel_code": "E_454", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1395", "tel_code": "E_454", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1396", "tel_code": "E_454", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate S | etat telephone : rayure*/
{"dep_code": "D_1397", "tel_code": "E_455", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1398", "tel_code": "E_455", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1399", "tel_code": "E_455", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1400", "tel_code": "E_455", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate S | etat telephone : Casse*/
{"dep_code": "D_1401", "tel_code": "E_456", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1402", "tel_code": "E_456", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1403", "tel_code": "E_456", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1404", "tel_code": "E_456", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 9 | etat telephone : Intact*/
{"dep_code": "D_1405", "tel_code": "E_457", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1406", "tel_code": "E_457", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1407", "tel_code": "E_457", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1408", "tel_code": "E_457", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 9 | etat telephone : Micro rayure*/
{"dep_code": "D_1409", "tel_code": "E_458", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1410", "tel_code": "E_458", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1411", "tel_code": "E_458", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1412", "tel_code": "E_458", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 9 | etat telephone : rayure*/
{"dep_code": "D_1413", "tel_code": "E_459", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1414", "tel_code": "E_459", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1415", "tel_code": "E_459", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1416", "tel_code": "E_459", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 9 | etat telephone : Cassé*/
{"dep_code": "D_1417", "tel_code": "E_460", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1418", "tel_code": "E_460", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1419", "tel_code": "E_460", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1420", "tel_code": "E_460", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 8 | etat telephone : Intact*/
{"dep_code": "D_1421", "tel_code": "E_461", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1422", "tel_code": "E_461", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1423", "tel_code": "E_461", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1424", "tel_code": "E_461", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 8 | etat telephone : Micro rayure*/
{"dep_code": "D_1425", "tel_code": "E_462", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1426", "tel_code": "E_462", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1427", "tel_code": "E_462", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1428", "tel_code": "E_462", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 8 | etat telephone : rayure*/
{"dep_code": "D_1429", "tel_code": "E_463", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1430", "tel_code": "E_463", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1431", "tel_code": "E_463", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1432", "tel_code": "E_463", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 8 | etat telephone : Casse*/
{"dep_code": "D_1433", "tel_code": "E_464", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1434", "tel_code": "E_464", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1435", "tel_code": "E_464", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1436", "tel_code": "E_464", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 7 | etat telephone : Intact*/
{"dep_code": "D_1437", "tel_code": "E_465", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1438", "tel_code": "E_465", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1439", "tel_code": "E_465", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1440", "tel_code": "E_465", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 7 | etat telephone : rayure*/
{"dep_code": "D_1441", "tel_code": "E_466", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1442", "tel_code": "E_466", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1443", "tel_code": "E_466", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1444", "tel_code": "E_466", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 7 | etat telephone : rayure*/
{"dep_code": "D_1445", "tel_code": "E_467", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1446", "tel_code": "E_467", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1447", "tel_code": "E_467", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1448", "tel_code": "E_467", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 7 | etat telephone : Cassé*/
{"dep_code": "D_1449", "tel_code": "E_468", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1450", "tel_code": "E_468", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1451", "tel_code": "E_468", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1452", "tel_code": "E_468", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20X | etat telephone : Intact*/
{"dep_code": "D_1453", "tel_code": "E_469", "tel_nom": "Intact", "etat_code": "6008 €"},
{"dep_code": "D_1454", "tel_code": "E_469", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1455", "tel_code": "E_469", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1456", "tel_code": "E_469", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20X | etat telephone : Micro rayure*/
{"dep_code": "D_1457", "tel_code": "E_470", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1458", "tel_code": "E_470", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1459", "tel_code": "E_470", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1460", "tel_code": "E_470", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20X | etat telephone : rayure*/
{"dep_code": "D_1461", "tel_code": "E_471", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1462", "tel_code": "E_471", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1463", "tel_code": "E_471", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1464", "tel_code": "E_471", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20X | etat telephone : Casse*/
{"dep_code": "D_1465", "tel_code": "E_472", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1466", "tel_code": "E_472", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1467", "tel_code": "E_472", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1468", "tel_code": "E_472", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Pro | etat telephone : Intact*/
{"dep_code": "D_1469", "tel_code": "E_473", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1470", "tel_code": "E_473", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1471", "tel_code": "E_473", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1472", "tel_code": "E_473", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Pro | etat telephone : Micro rayure*/
{"dep_code": "D_1473", "tel_code": "E_474", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1474", "tel_code": "E_474", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1475", "tel_code": "E_474", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1476", "tel_code": "E_474", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Pro | etat telephone : rayure*/
{"dep_code": "D_1477", "tel_code": "E_475", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1478", "tel_code": "E_475", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1479", "tel_code": "E_475", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1480", "tel_code": "E_475", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Pro | etat telephone : Casse*/
{"dep_code": "D_1481", "tel_code": "E_476", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1482", "tel_code": "E_476", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1483", "tel_code": "E_476", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1484", "tel_code": "E_476", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Lite | etat telephone : Intact*/
{"dep_code": "D_1485", "tel_code": "E_477", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1486", "tel_code": "E_477", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1487", "tel_code": "E_477", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1488", "tel_code": "E_477", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Lite | etat telephone : Micro rayure*/
{"dep_code": "D_1489", "tel_code": "E_478", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1490", "tel_code": "E_478", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1491", "tel_code": "E_478", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1492", "tel_code": "E_478", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Lite | etat telephone : rayure*/
{"dep_code": "D_1493", "tel_code": "E_479", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1494", "tel_code": "E_479", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1495", "tel_code": "E_479", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1496", "tel_code": "E_479", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 Lite | etat telephone : Casse*/
{"dep_code": "D_1497", "tel_code": "E_480", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1498", "tel_code": "E_480", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1499", "tel_code": "E_480", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1500", "tel_code": "E_480", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 | etat telephone : Intact*/
{"dep_code": "D_1501", "tel_code": "E_481", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1502", "tel_code": "E_481", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1503", "tel_code": "E_481", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1504", "tel_code": "E_481", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 | etat telephone : Micro rayure*/
{"dep_code": "D_1505", "tel_code": "E_482", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1506", "tel_code": "E_482", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1507", "tel_code": "E_482", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1508", "tel_code": "E_482", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 | etat telephone : rayure*/
{"dep_code": "D_1509", "tel_code": "E_483", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1510", "tel_code": "E_483", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1511", "tel_code": "E_483", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1512", "tel_code": "E_483", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 20 | etat telephone : Casse*/
{"dep_code": "D_1513", "tel_code": "E_484", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1514", "tel_code": "E_484", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1515", "tel_code": "E_484", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1516", "tel_code": "E_484", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Pro | etat telephone : Intact*/
{"dep_code": "D_1517", "tel_code": "E_485", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1518", "tel_code": "E_485", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1519", "tel_code": "E_485", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1520", "tel_code": "E_485", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Pro | etat telephone : Micro rayure*/
{"dep_code": "D_1521", "tel_code": "E_486", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1522", "tel_code": "E_486", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1523", "tel_code": "E_486", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1524", "tel_code": "E_486", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Pro | etat telephone : rayure*/
{"dep_code": "D_1525", "tel_code": "E_487", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1526", "tel_code": "E_487", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1527", "tel_code": "E_487", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1528", "tel_code": "E_487", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Pro | etat telephone : Casse*/
{"dep_code": "D_1529", "tel_code": "E_488", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1530", "tel_code": "E_488", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1531", "tel_code": "E_488", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1532", "tel_code": "E_488", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Lite  | etat telephone : Intact*/
{"dep_code": "D_1533", "tel_code": "E_489", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1534", "tel_code": "E_489", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1535", "tel_code": "E_489", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1536", "tel_code": "E_489", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Lite  | etat telephone : Micro rayure*/
{"dep_code": "D_1537", "tel_code": "E_490", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1538", "tel_code": "E_490", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1539", "tel_code": "E_490", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1540", "tel_code": "E_490", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Lite  | etat telephone : rayure*/
{"dep_code": "D_1541", "tel_code": "E_491", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1542", "tel_code": "E_491", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1543", "tel_code": "E_491", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1544", "tel_code": "E_491", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : huawei Mate 10 Lite  | etat telephone : Casse*/
{"dep_code": "D_1545", "tel_code": "E_492", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1546", "tel_code": "E_492", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1547", "tel_code": "E_492", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1548", "tel_code": "E_492", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Zr | etat telephone : Intact*/
{"dep_code": "D_1549", "tel_code": "E_493", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1550", "tel_code": "E_493", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1551", "tel_code": "E_493", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1552", "tel_code": "E_493", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Zr | etat telephone : Micro rayure*/
{"dep_code": "D_1553", "tel_code": "E_494", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1554", "tel_code": "E_494", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1555", "tel_code": "E_494", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1556", "tel_code": "E_494", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Zr | etat telephone : Rayure*/
{"dep_code": "D_1557", "tel_code": "E_495", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1558", "tel_code": "E_495", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1559", "tel_code": "E_495", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1560", "tel_code": "E_495", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Zr | etat telephone : Casse*/
{"dep_code": "D_1561", "tel_code": "E_496", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1562", "tel_code": "E_496", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1563", "tel_code": "E_496", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1564", "tel_code": "E_496", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Prenuim | etat telephone : Intact*/
{"dep_code": "D_1565", "tel_code": "E_497", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1566", "tel_code": "E_497", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1567", "tel_code": "E_497", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1568", "tel_code": "E_497", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Prenuim | etat telephone : Micro rayure*/
{"dep_code": "D_1569", "tel_code": "E_498", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1570", "tel_code": "E_498", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1571", "tel_code": "E_498", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1572", "tel_code": "E_498", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Prenuim | etat telephone : rayure*/
{"dep_code": "D_1573", "tel_code": "E_499", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1574", "tel_code": "E_499", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1575", "tel_code": "E_499", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1576", "tel_code": "E_499", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Prenuim | etat telephone : Casse*/
{"dep_code": "D_1577", "tel_code": "E_500", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1578", "tel_code": "E_500", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1579", "tel_code": "E_500", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1580", "tel_code": "E_500", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Dual | etat telephone : Intact*/
{"dep_code": "D_1581", "tel_code": "E_501", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1582", "tel_code": "E_501", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1583", "tel_code": "E_501", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1584", "tel_code": "E_501", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Dual | etat telephone : Micro rayure*/
{"dep_code": "D_1585", "tel_code": "E_502", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1586", "tel_code": "E_502", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1587", "tel_code": "E_502", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1588", "tel_code": "E_502", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Dual | etat telephone : rayure*/
{"dep_code": "D_1589", "tel_code": "E_503", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1590", "tel_code": "E_503", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1591", "tel_code": "E_503", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1592", "tel_code": "E_503", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Dual | etat telephone : Casse*/
{"dep_code": "D_1593", "tel_code": "E_504", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1594", "tel_code": "E_504", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1595", "tel_code": "E_504", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1596", "tel_code": "E_504", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Compact | etat telephone : Intact*/
{"dep_code": "D_1597", "tel_code": "E_505", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1598", "tel_code": "E_505", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1599", "tel_code": "E_505", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1600", "tel_code": "E_505", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Compact | etat telephone : Micro rayure*/
{"dep_code": "D_1601", "tel_code": "E_506", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1602", "tel_code": "E_506", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1603", "tel_code": "E_506", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1604", "tel_code": "E_506", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Compact | etat telephone : rayure*/
{"dep_code": "D_1605", "tel_code": "E_507", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1606", "tel_code": "E_507", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1607", "tel_code": "E_507", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1608", "tel_code": "E_507", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 Compact | etat telephone : Casse*/
{"dep_code": "D_1609", "tel_code": "E_508", "tel_nom": "Intact", "etat_code": "600k €"},
{"dep_code": "D_1610", "tel_code": "E_508", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1611", "tel_code": "E_508", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1612", "tel_code": "E_508", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 | etat telephone : Intact*/
{"dep_code": "D_1613", "tel_code": "E_509", "tel_nom": "Intact", "etat_code": "600ù €"},
{"dep_code": "D_1614", "tel_code": "E_509", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1615", "tel_code": "E_509", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1616", "tel_code": "E_509", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 | etat telephone : Micro rayure*/
{"dep_code": "D_1617", "tel_code": "E_510", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1618", "tel_code": "E_510", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1619", "tel_code": "E_510", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1620", "tel_code": "E_510", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 | etat telephone : rayure*/
{"dep_code": "D_1621", "tel_code": "E_511", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1622", "tel_code": "E_511", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1623", "tel_code": "E_511", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1624", "tel_code": "E_511", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z5 | etat telephone : Casse*/
{"dep_code": "D_1625", "tel_code": "E_512", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1626", "tel_code": "E_512", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1627", "tel_code": "E_512", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1628", "tel_code": "E_512", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 Compact | etat telephone : Intact*/
{"dep_code": "D_1629", "tel_code": "E_513", "tel_nom": "Intact", "etat_code": "60m0 €"},
{"dep_code": "D_1630", "tel_code": "E_513", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1631", "tel_code": "E_513", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1632", "tel_code": "E_513", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 Compact | etat telephone : Micro rayure*/
{"dep_code": "D_1633", "tel_code": "E_514", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1634", "tel_code": "E_514", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1635", "tel_code": "E_514", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1636", "tel_code": "E_514", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 Compact | etat telephone : rayure*/
{"dep_code": "D_1637", "tel_code": "E_515", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1638", "tel_code": "E_515", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1639", "tel_code": "E_515", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1640", "tel_code": "E_515", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 Compact | etat telephone : Casse*/
{"dep_code": "D_1641", "tel_code": "E_516", "tel_nom": "Intact", "etat_code": "6004 €"},
{"dep_code": "D_1642", "tel_code": "E_516", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1643", "tel_code": "E_516", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1644", "tel_code": "E_516", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 | etat telephone : Intact*/
{"dep_code": "D_1645", "tel_code": "E_517", "tel_nom": "Intact", "etat_code": "600 lm€"},
{"dep_code": "D_1646", "tel_code": "E_517", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1647", "tel_code": "E_517", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1648", "tel_code": "E_517", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 | etat telephone : Micro rayure*/
{"dep_code": "D_1649", "tel_code": "E_518", "tel_nom": "Intact", "etat_code": "60l0 €"},
{"dep_code": "D_1650", "tel_code": "E_518", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1651", "tel_code": "E_518", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1652", "tel_code": "E_518", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 | etat telephone : rayure*/
{"dep_code": "D_1653", "tel_code": "E_519", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1654", "tel_code": "E_519", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1655", "tel_code": "E_519", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1656", "tel_code": "E_519", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z3 | etat telephone : Cassé*/
{"dep_code": "D_1657", "tel_code": "E_520", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1658", "tel_code": "E_520", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1659", "tel_code": "E_520", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1660", "tel_code": "E_520", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z2 | etat telephone : Intact*/
{"dep_code": "D_1661", "tel_code": "E_521", "tel_nom": "Intact", "etat_code": "60k0 €"},
{"dep_code": "D_1662", "tel_code": "E_521", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1663", "tel_code": "E_521", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1664", "tel_code": "E_521", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z2 | etat telephone : Micro rayure*/
{"dep_code": "D_1665", "tel_code": "E_522", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1666", "tel_code": "E_522", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1667", "tel_code": "E_522", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1668", "tel_code": "E_522", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z2 | etat telephone : rayure*/
{"dep_code": "D_1669", "tel_code": "E_523", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1670", "tel_code": "E_523", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1671", "tel_code": "E_523", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1672", "tel_code": "E_523", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z2 | etat telephone : Casse*/
{"dep_code": "D_1673", "tel_code": "E_524", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1674", "tel_code": "E_524", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1675", "tel_code": "E_524", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1676", "tel_code": "E_524", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 Compact | etat telephone : Intact*/
{"dep_code": "D_1677", "tel_code": "E_525", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1678", "tel_code": "E_525", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1679", "tel_code": "E_525", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1680", "tel_code": "E_525", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 Compact | etat telephone : Micro rayure*/
{"dep_code": "D_1681", "tel_code": "E_526", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1682", "tel_code": "E_526", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1683", "tel_code": "E_526", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1684", "tel_code": "E_526", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 Compact | etat telephone : rayure*/
{"dep_code": "D_1685", "tel_code": "E_527", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1686", "tel_code": "E_527", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1687", "tel_code": "E_527", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1688", "tel_code": "E_527", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 Compact | etat telephone : Casse*/
{"dep_code": "D_1689", "tel_code": "E_528", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1690", "tel_code": "E_528", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1691", "tel_code": "E_528", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1692", "tel_code": "E_528", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 | etat telephone : Intact*/
{"dep_code": "D_1693", "tel_code": "E_529", "tel_nom": "Intact", "etat_code": "600m €"},
{"dep_code": "D_1694", "tel_code": "E_529", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1695", "tel_code": "E_529", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1696", "tel_code": "E_529", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 | etat telephone : Micro rayure*/
{"dep_code": "D_1697", "tel_code": "E_530", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1698", "tel_code": "E_530", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1699", "tel_code": "E_530", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1700", "tel_code": "E_530", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 | etat telephone : rayure*/
{"dep_code": "D_1701", "tel_code": "E_531", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1702", "tel_code": "E_531", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1703", "tel_code": "E_531", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1704", "tel_code": "E_531", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z1 | etat telephone : Casse*/
{"dep_code": "D_1705", "tel_code": "E_532", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1706", "tel_code": "E_532", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1707", "tel_code": "E_532", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1708", "tel_code": "E_532", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z Ultra | etat telephone : Intact*/
{"dep_code": "D_1709", "tel_code": "E_533", "tel_nom": "Intact", "etat_code": "600l €"},
{"dep_code": "D_1710", "tel_code": "E_533", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1711", "tel_code": "E_533", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1712", "tel_code": "E_533", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z Ultra | etat telephone : Micro rayure*/
{"dep_code": "D_1713", "tel_code": "E_534", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1714", "tel_code": "E_534", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1715", "tel_code": "E_534", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1716", "tel_code": "E_534", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z Ultra | etat telephone : rayure*/
{"dep_code": "D_1717", "tel_code": "E_535", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1718", "tel_code": "E_535", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1719", "tel_code": "E_535", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1720", "tel_code": "E_535", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z Ultra | etat telephone : Casse*/
{"dep_code": "D_1721", "tel_code": "E_536", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1722", "tel_code": "E_536", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1723", "tel_code": "E_536", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1724", "tel_code": "E_536", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z | etat telephone : Intact*/
{"dep_code": "D_1725", "tel_code": "E_537", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1726", "tel_code": "E_537", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1727", "tel_code": "E_537", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1728", "tel_code": "E_537", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z | etat telephone : Micro rayure*/
{"dep_code": "D_1729", "tel_code": "E_538", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1730", "tel_code": "E_538", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1731", "tel_code": "E_538", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1732", "tel_code": "E_538", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z | etat telephone : rayure*/
{"dep_code": "D_1733", "tel_code": "E_539", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1734", "tel_code": "E_539", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1735", "tel_code": "E_539", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1736", "tel_code": "E_539", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Sony Xperia Z | etat telephone : Casse*/
{"dep_code": "D_1737", "tel_code": "E_540", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1738", "tel_code": "E_540", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1739", "tel_code": "E_540", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1740", "tel_code": "E_540", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIko Ufeel | etat telephone : Intact*/
{"dep_code": "D_1741", "tel_code": "E_541", "tel_nom": "Intact", "etat_code": "600à €"},
{"dep_code": "D_1742", "tel_code": "E_541", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1743", "tel_code": "E_541", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1744", "tel_code": "E_541", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIko Ufeel | etat telephone : Micro rayure*/
{"dep_code": "D_1745", "tel_code": "E_542", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1746", "tel_code": "E_542", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1747", "tel_code": "E_542", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1748", "tel_code": "E_542", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIko Ufeel | etat telephone : rayure*/
{"dep_code": "D_1749", "tel_code": "E_543", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1750", "tel_code": "E_543", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1751", "tel_code": "E_543", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1752", "tel_code": "E_543", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIko Ufeel | etat telephone : Casse*/
{"dep_code": "D_1753", "tel_code": "E_544", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1754", "tel_code": "E_544", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1755", "tel_code": "E_544", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1756", "tel_code": "E_544", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Tommmy | etat telephone : Intact*/
{"dep_code": "D_1757", "tel_code": "E_545", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1758", "tel_code": "E_545", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1759", "tel_code": "E_545", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1760", "tel_code": "E_545", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Tommmy | etat telephone : Micro rayure*/
{"dep_code": "D_1761", "tel_code": "E_546", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1762", "tel_code": "E_546", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1763", "tel_code": "E_546", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1764", "tel_code": "E_546", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Tommmy | etat telephone : rayure*/
{"dep_code": "D_1765", "tel_code": "E_547", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1766", "tel_code": "E_547", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1767", "tel_code": "E_547", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1768", "tel_code": "E_547", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Tommmy | etat telephone : Casse*/
{"dep_code": "D_1769", "tel_code": "E_548", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1770", "tel_code": "E_548", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1771", "tel_code": "E_548", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1772", "tel_code": "E_548", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Ridge | etat telephone : Intact*/
{"dep_code": "D_1773", "tel_code": "E_549", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1774", "tel_code": "E_549", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1775", "tel_code": "E_549", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1776", "tel_code": "E_549", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Ridge | etat telephone : Micro rayure*/
{"dep_code": "D_1777", "tel_code": "E_550", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1778", "tel_code": "E_550", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1779", "tel_code": "E_550", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1780", "tel_code": "E_550", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Ridge | etat telephone : rayure*/
{"dep_code": "D_1781", "tel_code": "E_551", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1782", "tel_code": "E_551", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1783", "tel_code": "E_551", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1784", "tel_code": "E_551", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Ridge | etat telephone : Casse*/
{"dep_code": "D_1785", "tel_code": "E_552", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1786", "tel_code": "E_552", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1787", "tel_code": "E_552", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1788", "tel_code": "E_552", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Lite Dual Sim | etat telephone : Intact*/
{"dep_code": "D_1789", "tel_code": "E_553", "tel_nom": "Intact", "etat_code": "600$ €"},
{"dep_code": "D_1790", "tel_code": "E_553", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1791", "tel_code": "E_553", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1792", "tel_code": "E_553", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Lite Dual Sim | etat telephone : Micro rayure*/
{"dep_code": "D_1793", "tel_code": "E_554", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1794", "tel_code": "E_554", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1795", "tel_code": "E_554", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1796", "tel_code": "E_554", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Lite Dual Sim | etat telephone : rayure*/
{"dep_code": "D_1797", "tel_code": "E_555", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1798", "tel_code": "E_555", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1799", "tel_code": "E_555", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1800", "tel_code": "E_555", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Lite Dual Sim | etat telephone : Casse*/
{"dep_code": "D_1801", "tel_code": "E_556", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1802", "tel_code": "E_556", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1803", "tel_code": "E_556", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1804", "tel_code": "E_556", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Jam| etat telephone : Intact*/
{"dep_code": "D_1805", "tel_code": "E_557", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1806", "tel_code": "E_557", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1807", "tel_code": "E_557", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1808", "tel_code": "E_557", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Jam| etat telephone : Micro rayure*/
{"dep_code": "D_1809", "tel_code": "E_558", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1810", "tel_code": "E_558", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1811", "tel_code": "E_558", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1812", "tel_code": "E_558", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Jam| etat telephone : rayure*/
{"dep_code": "D_1813", "tel_code": "E_559", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1814", "tel_code": "E_559", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1815", "tel_code": "E_559", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1816", "tel_code": "E_559", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Rainbow Jam| etat telephone : Casse*/
{"dep_code": "D_1817", "tel_code": "E_560", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1818", "tel_code": "E_560", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1819", "tel_code": "E_560", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1820", "tel_code": "E_560", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Pulp 4G| etat telephone : Intact*/
{"dep_code": "D_1821", "tel_code": "E_561", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1822", "tel_code": "E_561", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1823", "tel_code": "E_561", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1824", "tel_code": "E_561", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Pulp 4G| etat telephone : Micro rayure*/
{"dep_code": "D_1825", "tel_code": "E_562", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1826", "tel_code": "E_562", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1827", "tel_code": "E_562", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1828", "tel_code": "E_562", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Pulp 4G| etat telephone : rayure*/
{"dep_code": "D_1829", "tel_code": "E_563", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1830", "tel_code": "E_563", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1831", "tel_code": "E_563", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1832", "tel_code": "E_563", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Pulp 4G| etat telephone : Casse*/
{"dep_code": "D_1833", "tel_code": "E_564", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1834", "tel_code": "E_564", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1835", "tel_code": "E_564", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1836", "tel_code": "E_564", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Lenny 3| etat telephone : Intact*/
{"dep_code": "D_1837", "tel_code": "E_565", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1838", "tel_code": "E_565", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1839", "tel_code": "E_565", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1840", "tel_code": "E_565", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Lenny 3| etat telephone : Micro rayure*/
{"dep_code": "D_1841", "tel_code": "E_566", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1842", "tel_code": "E_566", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1843", "tel_code": "E_566", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1844", "tel_code": "E_566", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Lenny 3| etat telephone : rayure*/
{"dep_code": "D_1845", "tel_code": "E_567", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1846", "tel_code": "E_567", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1847", "tel_code": "E_567", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1848", "tel_code": "E_567", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Lenny 3| etat telephone : Casse*/
{"dep_code": "D_1849", "tel_code": "E_568", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1850", "tel_code": "E_568", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1851", "tel_code": "E_568", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1852", "tel_code": "E_568", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Lenny 2| etat telephone : Intact*/
{"dep_code": "D_1853", "tel_code": "E_569", "tel_nom": "Intact", "etat_code": "60h0 €"},
{"dep_code": "D_1854", "tel_code": "E_569", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1855", "tel_code": "E_569", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1856", "tel_code": "E_569", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Lenny 2| etat telephone : Micro rayure*/
{"dep_code": "D_1857", "tel_code": "E_570", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1858", "tel_code": "E_570", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1859", "tel_code": "E_570", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1860", "tel_code": "E_570", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Lenny 2| etat telephone : rayure*/
{"dep_code": "D_1861", "tel_code": "E_571", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1862", "tel_code": "E_571", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1863", "tel_code": "E_571", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1864", "tel_code": "E_571", "tel_nom": "Cassé", "etat_code": "550 €"}, 


/* telephone : WIKO Lenny 2| etat telephone : Casse*/
{"dep_code": "D_1865", "tel_code": "E_572", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1866", "tel_code": "E_572", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1867", "tel_code": "E_572", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1868", "tel_code": "E_572", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Highway 4G| etat telephone : Intact*/
{"dep_code": "D_1869", "tel_code": "E_573", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1870", "tel_code": "E_573", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1871", "tel_code": "E_573", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1872", "tel_code": "E_573", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Highway 4G| etat telephone : Micro rayure*/
{"dep_code": "D_1873", "tel_code": "E_574", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1874", "tel_code": "E_574", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1875", "tel_code": "E_574", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1876", "tel_code": "E_574", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Highway 4G| etat telephone : rayure*/
{"dep_code": "D_1877", "tel_code": "E_575", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1878", "tel_code": "E_575", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1879", "tel_code": "E_575", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1880", "tel_code": "E_575", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : WIKO Highway 4G| etat telephone : Casse*/
{"dep_code": "D_1881", "tel_code": "E_576", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1882", "tel_code": "E_576", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1883", "tel_code": "E_576", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1884", "tel_code": "E_576", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG V30| etat telephone : Intact*/
{"dep_code": "D_1885", "tel_code": "E_577", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1886", "tel_code": "E_577", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1887", "tel_code": "E_577", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1888", "tel_code": "E_577", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG V30| etat telephone : Micro rayure*/
{"dep_code": "D_1889", "tel_code": "E_578", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1890", "tel_code": "E_578", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1891", "tel_code": "E_578", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1892", "tel_code": "E_578", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG V30| etat telephone : rayure*/
{"dep_code": "D_1893", "tel_code": "E_579", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1894", "tel_code": "E_579", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1895", "tel_code": "E_579", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1896", "tel_code": "E_579", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG V30| etat telephone : Casse*/
{"dep_code": "D_1897", "tel_code": "E_580", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1898", "tel_code": "E_580", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1899", "tel_code": "E_580", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1900", "tel_code": "E_580", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G pro| etat telephone : Intact*/
{"dep_code": "D_1901", "tel_code": "E_581", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1902", "tel_code": "E_581", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1903", "tel_code": "E_581", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1904", "tel_code": "E_581", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G pro| etat telephone : Micro rayure*/
{"dep_code": "D_1905", "tel_code": "E_582", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1906", "tel_code": "E_582", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1907", "tel_code": "E_582", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1908", "tel_code": "E_582", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G pro| etat telephone : rayure*/
{"dep_code": "D_1909", "tel_code": "E_583", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1910", "tel_code": "E_583", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1911", "tel_code": "E_583", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1912", "tel_code": "E_583", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G pro| etat telephone : Casse*/
{"dep_code": "D_1913", "tel_code": "E_584", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1914", "tel_code": "E_584", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1915", "tel_code": "E_584", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1916", "tel_code": "E_584", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G E975| etat telephone : Intact*/
{"dep_code": "D_1917", "tel_code": "E_585", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1918", "tel_code": "E_585", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1919", "tel_code": "E_585", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1920", "tel_code": "E_585", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G E975| etat telephone : Micro rayure*/
{"dep_code": "D_1921", "tel_code": "E_586", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1922", "tel_code": "E_586", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1923", "tel_code": "E_586", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1924", "tel_code": "E_586", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G E975| etat telephone : rayure*/
{"dep_code": "D_1925", "tel_code": "E_587", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1926", "tel_code": "E_587", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1927", "tel_code": "E_587", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1928", "tel_code": "E_587", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G E975| etat telephone : Casse*/
{"dep_code": "D_1929", "tel_code": "E_588", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1930", "tel_code": "E_588", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1931", "tel_code": "E_588", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1932", "tel_code": "E_588", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G | etat telephone : Intact*/
{"dep_code": "D_1933", "tel_code": "E_589", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1934", "tel_code": "E_589", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1935", "tel_code": "E_589", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1936", "tel_code": "E_589", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G | etat telephone : Micro rayure*/
{"dep_code": "D_1937", "tel_code": "E_590", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1938", "tel_code": "E_590", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1939", "tel_code": "E_590", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1940", "tel_code": "E_590", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G | etat telephone : rayure*/
{"dep_code": "D_1941", "tel_code": "E_591", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1942", "tel_code": "E_591", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1943", "tel_code": "E_591", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1944", "tel_code": "E_591", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus G | etat telephone : Casse*/
{"dep_code": "D_1945", "tel_code": "E_592", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1946", "tel_code": "E_592", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1947", "tel_code": "E_592", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1948", "tel_code": "E_592", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus 4X HD | etat telephone : Intact*/
{"dep_code": "D_1949", "tel_code": "E_593", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1950", "tel_code": "E_593", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1951", "tel_code": "E_593", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1952", "tel_code": "E_593", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus 4X HD | etat telephone : Micro rayure*/
{"dep_code": "D_1953", "tel_code": "E_594", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1954", "tel_code": "E_594", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1955", "tel_code": "E_594", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1956", "tel_code": "E_594", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus 4X HD | etat telephone : rayure*/
{"dep_code": "D_1957", "tel_code": "E_595", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1958", "tel_code": "E_595", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1959", "tel_code": "E_595", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1960", "tel_code": "E_595", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Optimus 4X HD | etat telephone : Casse*/
{"dep_code": "D_1961", "tel_code": "E_596", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1962", "tel_code": "E_596", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1963", "tel_code": "E_596", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1964", "tel_code": "E_596", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5X | etat telephone : Intact*/
{"dep_code": "D_1965", "tel_code": "E_597", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1966", "tel_code": "E_597", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1967", "tel_code": "E_597", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1968", "tel_code": "E_597", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5X | etat telephone : Micro rayure*/
{"dep_code": "D_1969", "tel_code": "E_598", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1970", "tel_code": "E_598", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1971", "tel_code": "E_598", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1972", "tel_code": "E_598", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5X | etat telephone : rayure*/
{"dep_code": "D_1973", "tel_code": "E_599", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1974", "tel_code": "E_599", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1975", "tel_code": "E_599", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1976", "tel_code": "E_599", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5X | etat telephone : Casse*/
{"dep_code": "D_1977", "tel_code": "E_600", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1978", "tel_code": "E_600", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1979", "tel_code": "E_600", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1980", "tel_code": "E_600", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5 | etat telephone : Intact*/
{"dep_code": "D_1985", "tel_code": "E_601", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1986", "tel_code": "E_601", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1987", "tel_code": "E_601", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1988", "tel_code": "E_601", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5 | etat telephone : Micro rayure*/
{"dep_code": "D_1989", "tel_code": "E_602", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1990", "tel_code": "E_602", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1991", "tel_code": "E_602", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1992", "tel_code": "E_602", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5 | etat telephone : rayure*/
{"dep_code": "D_1993", "tel_code": "E_603", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1994", "tel_code": "E_603", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1995", "tel_code": "E_603", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_1996", "tel_code": "E_603", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 5 | etat telephone : Casse*/
{"dep_code": "D_1997", "tel_code": "E_604", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_1998", "tel_code": "E_604", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_1999", "tel_code": "E_604", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2000", "tel_code": "E_604", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 4 | etat telephone : Intact*/
{"dep_code": "D_2001", "tel_code": "E_605", "tel_nom": "Intact", "etat_code": "600y €"},
{"dep_code": "D_2002", "tel_code": "E_605", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2003", "tel_code": "E_605", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2004", "tel_code": "E_605", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 4 | etat telephone : Micro rayure*/
{"dep_code": "D_2005", "tel_code": "E_606", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2006", "tel_code": "E_606", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2007", "tel_code": "E_606", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2008", "tel_code": "E_606", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 4 | etat telephone : rayure*/
{"dep_code": "D_2009", "tel_code": "E_607", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2010", "tel_code": "E_607", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2011", "tel_code": "E_607", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2012", "tel_code": "E_607", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG Nexus 4 | etat telephone : Casse*/
{"dep_code": "D_2013", "tel_code": "E_608", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2014", "tel_code": "E_608", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2015", "tel_code": "E_608", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2016", "tel_code": "E_608", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G7 | etat telephone : Intact*/
{"dep_code": "D_2017", "tel_code": "E_609", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2018", "tel_code": "E_609", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2019", "tel_code": "E_609", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2020", "tel_code": "E_609", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G7 | etat telephone : Micro rayure*/
{"dep_code": "D_2021", "tel_code": "E_610", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2022", "tel_code": "E_610", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2023", "tel_code": "E_610", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2024", "tel_code": "E_610", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G7 | etat telephone : rayure*/
{"dep_code": "D_2025", "tel_code": "E_611", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2026", "tel_code": "E_611", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2027", "tel_code": "E_611", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2028", "tel_code": "E_611", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G7 | etat telephone : Casse*/
{"dep_code": "D_2029", "tel_code": "E_612", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2030", "tel_code": "E_612", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2031", "tel_code": "E_612", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2032", "tel_code": "E_612", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G6 | etat telephone : Intact*/
{"dep_code": "D_2033", "tel_code": "E_613", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2034", "tel_code": "E_613", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2035", "tel_code": "E_613", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2036", "tel_code": "E_613", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G6 | etat telephone : Micro rayure*/
{"dep_code": "D_2037", "tel_code": "E_614", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2038", "tel_code": "E_614", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2039", "tel_code": "E_614", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2040", "tel_code": "E_614", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G6 | etat telephone : Rayure*/
{"dep_code": "D_2041", "tel_code": "E_615", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2042", "tel_code": "E_615", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2043", "tel_code": "E_615", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2044", "tel_code": "E_615", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G6 | etat telephone : Casse*/
{"dep_code": "D_2045", "tel_code": "E_616", "tel_nom": "Intact", "etat_code": "600u €"},
{"dep_code": "D_2046", "tel_code": "E_616", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2047", "tel_code": "E_616", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2048", "tel_code": "E_616", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G5 | etat telephone : Intact*/
{"dep_code": "D_2049", "tel_code": "E_617", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2050", "tel_code": "E_617", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2051", "tel_code": "E_617", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2052", "tel_code": "E_617", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G5 | etat telephone : Micro rayure*/
{"dep_code": "D_2053", "tel_code": "E_618", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2054", "tel_code": "E_618", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2055", "tel_code": "E_618", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2056", "tel_code": "E_618", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G5 | etat telephone : rayure*/
{"dep_code": "D_2057", "tel_code": "E_619", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2058", "tel_code": "E_619", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2059", "tel_code": "E_619", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2060", "tel_code": "E_619", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G5 | etat telephone : Casse*/
{"dep_code": "D_2061", "tel_code": "E_620", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2062", "tel_code": "E_620", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2063", "tel_code": "E_620", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2064", "tel_code": "E_620", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G4 | etat telephone : Intact*/
{"dep_code": "D_2065", "tel_code": "E_621", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2066", "tel_code": "E_621", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2067", "tel_code": "E_621", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2068", "tel_code": "E_621", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G4 | etat telephone : Micro rayure*/
{"dep_code": "D_2069", "tel_code": "E_622", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2070", "tel_code": "E_622", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2071", "tel_code": "E_622", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2072", "tel_code": "E_622", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G4 | etat telephone : rayure*/
{"dep_code": "D_2073", "tel_code": "E_623", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2074", "tel_code": "E_623", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2075", "tel_code": "E_623", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2076", "tel_code": "E_623", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G4 | etat telephone : Casse*/
{"dep_code": "D_2077", "tel_code": "E_624", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2078", "tel_code": "E_624", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2079", "tel_code": "E_624", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2080", "tel_code": "E_624", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3S | etat telephone : Intact*/
{"dep_code": "D_2081", "tel_code": "E_625", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2082", "tel_code": "E_625", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2083", "tel_code": "E_625", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2084", "tel_code": "E_625", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3S | etat telephone : Micro rayure*/
{"dep_code": "D_2085", "tel_code": "E_626", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2086", "tel_code": "E_626", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2087", "tel_code": "E_626", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2088", "tel_code": "E_626", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3S | etat telephone : rayure*/
{"dep_code": "D_2089", "tel_code": "E_627", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2090", "tel_code": "E_627", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2091", "tel_code": "E_627", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2092", "tel_code": "E_627", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3S | etat telephone : Casse*/
{"dep_code": "D_2093", "tel_code": "E_628", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2094", "tel_code": "E_628", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2095", "tel_code": "E_628", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2096", "tel_code": "E_628", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3 | etat telephone : Intact*/
{"dep_code": "D_2097", "tel_code": "E_629", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2098", "tel_code": "E_629", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2099", "tel_code": "E_629", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2100", "tel_code": "E_629", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3 | etat telephone : Micro rayure*/
{"dep_code": "D_2101", "tel_code": "E_630", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2102", "tel_code": "E_630", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2103", "tel_code": "E_630", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2104", "tel_code": "E_630", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3 | etat telephone : rayure*/
{"dep_code": "D_2105", "tel_code": "E_631", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2106", "tel_code": "E_631", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2107", "tel_code": "E_631", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2108", "tel_code": "E_631", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G3 | etat telephone : Casse*/
{"dep_code": "D_2109", "tel_code": "E_632", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2110", "tel_code": "E_632", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2111", "tel_code": "E_632", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2112", "tel_code": "E_632", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G2 | etat telephone : Intact*/
{"dep_code": "D_2113", "tel_code": "E_633", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2114", "tel_code": "E_633", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2115", "tel_code": "E_633", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2116", "tel_code": "E_633", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G2 | etat telephone : Micro rayure*/
{"dep_code": "D_2117", "tel_code": "E_634", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2118", "tel_code": "E_634", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2119", "tel_code": "E_634", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2120", "tel_code": "E_634", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G2 | etat telephone : rayure*/
{"dep_code": "D_2121", "tel_code": "E_635", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2122", "tel_code": "E_635", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2123", "tel_code": "E_635", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2124", "tel_code": "E_635", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G2 | etat telephone : Casse*/
{"dep_code": "D_2125", "tel_code": "E_636", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2126", "tel_code": "E_636", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2127", "tel_code": "E_636", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2128", "tel_code": "E_636", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G pro 2 | etat telephone : Intact*/
{"dep_code": "D_2129", "tel_code": "E_637", "tel_nom": "Intact", "etat_code": "x0 €"},
{"dep_code": "D_2130", "tel_code": "E_637", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2131", "tel_code": "E_637", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2132", "tel_code": "E_637", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G pro 2 | etat telephone : Micro rayure*/
{"dep_code": "D_2133", "tel_code": "E_638", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2134", "tel_code": "E_638", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2135", "tel_code": "E_638", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2136", "tel_code": "E_638", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G pro 2 | etat telephone : rayure*/
{"dep_code": "D_2137", "tel_code": "E_639", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2138", "tel_code": "E_639", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2139", "tel_code": "E_639", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2140", "tel_code": "E_639", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G pro 2 | etat telephone : Casse*/
{"dep_code": "D_2141", "tel_code": "E_640", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2142", "tel_code": "E_640", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2143", "tel_code": "E_640", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2144", "tel_code": "E_640", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex 2 | etat telephone : Intact*/
{"dep_code": "D_2145", "tel_code": "E_641", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2146", "tel_code": "E_641", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2147", "tel_code": "E_641", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2148", "tel_code": "E_641", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex 2 | etat telephone : Micro rayure*/
{"dep_code": "D_2149", "tel_code": "E_642", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2150", "tel_code": "E_642", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2151", "tel_code": "E_642", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2152", "tel_code": "E_642", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex 2 | etat telephone : rayure*/
{"dep_code": "D_2153", "tel_code": "E_643", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2154", "tel_code": "E_643", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2155", "tel_code": "E_643", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2156", "tel_code": "E_643", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex 2 | etat telephone : Casse*/
{"dep_code": "D_2157", "tel_code": "E_644", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2158", "tel_code": "E_644", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2159", "tel_code": "E_644", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2160", "tel_code": "E_644", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex | etat telephone : Intact*/
{"dep_code": "D_2161", "tel_code": "E_645", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2162", "tel_code": "E_645", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2163", "tel_code": "E_645", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2164", "tel_code": "E_645", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex | etat telephone : Micro rayure*/
{"dep_code": "D_2165", "tel_code": "E_646", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2166", "tel_code": "E_646", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2167", "tel_code": "E_646", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2168", "tel_code": "E_646", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex | etat telephone : rayure*/
{"dep_code": "D_2169", "tel_code": "E_647", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2170", "tel_code": "E_647", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2171", "tel_code": "E_647", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2172", "tel_code": "E_647", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : LG G Flex | etat telephone : Casse*/
{"dep_code": "D_2173", "tel_code": "E_648", "tel_nom": "Intact", "etat_code": "600: €"},
{"dep_code": "D_2174", "tel_code": "E_648", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2175", "tel_code": "E_648", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2176", "tel_code": "E_648", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 20| etat telephone : Intact*/
{"dep_code": "D_2177", "tel_code": "E_649", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2178", "tel_code": "E_649", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2179", "tel_code": "E_649", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2180", "tel_code": "E_649", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 20| etat telephone : Micro rayure*/
{"dep_code": "D_2181", "tel_code": "E_650", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2182", "tel_code": "E_650", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2183", "tel_code": "E_650", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2184", "tel_code": "E_650", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 20| etat telephone : rayure*/
{"dep_code": "D_2185", "tel_code": "E_651", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2186", "tel_code": "E_651", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2187", "tel_code": "E_651", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2188", "tel_code": "E_651", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 20| etat telephone : Casse*/
{"dep_code": "D_2189", "tel_code": "E_652", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2190", "tel_code": "E_652", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2191", "tel_code": "E_652", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2192", "tel_code": "E_652", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 10| etat telephone : Intact*/
{"dep_code": "D_2193", "tel_code": "E_653", "tel_nom": "Intact", "etat_code": "x €"},
{"dep_code": "D_2194", "tel_code": "E_653", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2195", "tel_code": "E_653", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2196", "tel_code": "E_653", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 10| etat telephone : Micro rayure*/
{"dep_code": "D_2197", "tel_code": "E_654", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2198", "tel_code": "E_654", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2199", "tel_code": "E_654", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2200", "tel_code": "E_654", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 10| etat telephone : rayure*/
{"dep_code": "D_2201", "tel_code": "E_655", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2202", "tel_code": "E_655", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2203", "tel_code": "E_655", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2204", "tel_code": "E_655", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor view 10| etat telephone : Casse*/
{"dep_code": "D_2205", "tel_code": "E_656", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2206", "tel_code": "E_656", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2207", "tel_code": "E_656", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2208", "tel_code": "E_656", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor Play| etat telephone : Intact*/
{"dep_code": "D_2209", "tel_code": "E_657", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2210", "tel_code": "E_657", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2211", "tel_code": "E_657", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2212", "tel_code": "E_657", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor Play| etat telephone : Micro rayure*/
{"dep_code": "D_2213", "tel_code": "E_658", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2214", "tel_code": "E_658", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2215", "tel_code": "E_658", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2216", "tel_code": "E_658", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor Play| etat telephone : rayure*/
{"dep_code": "D_2217", "tel_code": "E_659", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2218", "tel_code": "E_659", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2219", "tel_code": "E_659", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2220", "tel_code": "E_659", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor Play| etat telephone : Casse*/
{"dep_code": "D_2221", "tel_code": "E_660", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2222", "tel_code": "E_660", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2223", "tel_code": "E_660", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2224", "tel_code": "E_660", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 10 | etat telephone : Intact*/
{"dep_code": "D_2225", "tel_code": "E_661", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2226", "tel_code": "E_661", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2227", "tel_code": "E_661", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2228", "tel_code": "E_661", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 10 | etat telephone : Micro rayure*/
{"dep_code": "D_2229", "tel_code": "E_662", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2230", "tel_code": "E_662", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2231", "tel_code": "E_662", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2232", "tel_code": "E_662", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 10 | etat telephone : rayure*/
{"dep_code": "D_2233", "tel_code": "E_663", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2234", "tel_code": "E_663", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2235", "tel_code": "E_663", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2236", "tel_code": "E_663", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 10 | etat telephone : Casse*/
{"dep_code": "D_2237", "tel_code": "E_664", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2238", "tel_code": "E_664", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2239", "tel_code": "E_664", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2240", "tel_code": "E_664", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 Lite | etat telephone : Intact*/
{"dep_code": "D_2241", "tel_code": "E_665", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2242", "tel_code": "E_665", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2243", "tel_code": "E_665", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2244", "tel_code": "E_665", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 Lite | etat telephone : Micro Rayure*/
{"dep_code": "D_2245", "tel_code": "E_666", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2246", "tel_code": "E_666", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2247", "tel_code": "E_666", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2248", "tel_code": "E_666", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 Lite | etat telephone : rayure*/
{"dep_code": "D_2249", "tel_code": "E_667", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2250", "tel_code": "E_667", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2251", "tel_code": "E_667", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2252", "tel_code": "E_667", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 Lite | etat telephone : Casse*/
{"dep_code": "D_2253", "tel_code": "E_668", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2254", "tel_code": "E_668", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2255", "tel_code": "E_668", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2256", "tel_code": "E_668", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 | etat telephone : Intact*/
{"dep_code": "D_2257", "tel_code": "E_669", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2258", "tel_code": "E_669", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2259", "tel_code": "E_669", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2260", "tel_code": "E_669", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 | etat telephone : Micro rayure*/
{"dep_code": "D_2261", "tel_code": "E_670", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2262", "tel_code": "E_670", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2263", "tel_code": "E_670", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2264", "tel_code": "E_670", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 | etat telephone : rayure*/
{"dep_code": "D_2265", "tel_code": "E_671", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2266", "tel_code": "E_671", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2267", "tel_code": "E_671", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2268", "tel_code": "E_671", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 9 | etat telephone : Casse*/
{"dep_code": "D_2269", "tel_code": "E_672", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2270", "tel_code": "E_672", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2271", "tel_code": "E_672", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2272", "tel_code": "E_672", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8X | etat telephone : Intact*/
{"dep_code": "D_2273", "tel_code": "E_673", "tel_nom": "Intact", "etat_code": "600k €"},
{"dep_code": "D_2274", "tel_code": "E_673", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2275", "tel_code": "E_673", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2276", "tel_code": "E_673", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8X | etat telephone : micro rayure*/
{"dep_code": "D_2277", "tel_code": "E_674", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2278", "tel_code": "E_674", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2279", "tel_code": "E_674", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2280", "tel_code": "E_674", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8X | etat telephone : rayure*/
{"dep_code": "D_2281", "tel_code": "E_675", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2282", "tel_code": "E_675", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2283", "tel_code": "E_675", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2284", "tel_code": "E_675", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8X | etat telephone : Casse*/
{"dep_code": "D_2285", "tel_code": "E_676", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2286", "tel_code": "E_676", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2287", "tel_code": "E_676", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2288", "tel_code": "E_676", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8| etat telephone : Intact*/
{"dep_code": "D_2289", "tel_code": "E_677", "tel_nom": "Intact", "etat_code": "x €"},
{"dep_code": "D_2290", "tel_code": "E_677", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2291", "tel_code": "E_677", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2292", "tel_code": "E_677", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8| etat telephone : Micro rayure*/
{"dep_code": "D_2293", "tel_code": "E_678", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2294", "tel_code": "E_678", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2295", "tel_code": "E_678", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2296", "tel_code": "E_678", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8 | etat telephone : Rayure*/
{"dep_code": "D_2297", "tel_code": "E_679", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2298", "tel_code": "E_679", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2299", "tel_code": "E_679", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2300", "tel_code": "E_679", "tel_nom": "Cassé", "etat_code": "550 €"},


/* telephone : Honor 8 | etat telephone : Casse*/
{"dep_code": "D_2301", "tel_code": "E_680", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2302", "tel_code": "E_680", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2303", "tel_code": "E_680", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2304", "tel_code": "E_680", "tel_nom": "Cassé", "etat_code": "kl €"},


/* telephone : Honor 7X | etat telephone : Intact*/
{"dep_code": "D_2305", "tel_code": "E_681", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2306", "tel_code": "E_681", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2307", "tel_code": "E_681", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2308", "tel_code": "E_681", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7X | etat telephone : Micro rayure*/
{"dep_code": "D_2309", "tel_code": "E_682", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2310", "tel_code": "E_682", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2311", "tel_code": "E_682", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2312", "tel_code": "E_682", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7X | etat telephone : rayure*/
{"dep_code": "D_2313", "tel_code": "E_683", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2314", "tel_code": "E_683", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2315", "tel_code": "E_683", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2316", "tel_code": "E_683", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7X | etat telephone : Casse*/
{"dep_code": "D_2317", "tel_code": "E_684", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2318", "tel_code": "E_684", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2319", "tel_code": "E_684", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2320", "tel_code": "E_684", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7 Prenuim | etat telephone : Intact*/
{"dep_code": "D_2321", "tel_code": "E_685", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2322", "tel_code": "E_685", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2323", "tel_code": "E_685", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2324", "tel_code": "E_685", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7 Prenuim | etat telephone : Micro rayure*/
{"dep_code": "D_2325", "tel_code": "E_686", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2326", "tel_code": "E_686", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2327", "tel_code": "E_686", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2328", "tel_code": "E_686", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7 Prenuim | etat telephone : rayure*/
{"dep_code": "D_2329", "tel_code": "E_687", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2330", "tel_code": "E_687", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2331", "tel_code": "E_687", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2332", "tel_code": "E_687", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7 Prenuim | etat telephone : Casse*/
{"dep_code": "D_2333", "tel_code": "E_688", "tel_nom": "Intact", "etat_code": "600 €"},
{"dep_code": "D_2334", "tel_code": "E_688", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2335", "tel_code": "E_688", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2336", "tel_code": "E_688", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6X| etat telephone : Intact*/
{"dep_code": "D_2337", "tel_code": "E_689", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2338", "tel_code": "E_689", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2339", "tel_code": "E_689", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2340", "tel_code": "E_689", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6X| etat telephone : Micro rayure*/
{"dep_code": "D_2341", "tel_code": "E_690", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2342", "tel_code": "E_690", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2343", "tel_code": "E_690", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2344", "tel_code": "E_690", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6X| etat telephone : rayure*/
{"dep_code": "D_2345", "tel_code": "E_691", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2346", "tel_code": "E_691", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2347", "tel_code": "E_691", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2348", "tel_code": "E_691", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6X| etat telephone : Casse*/
{"dep_code": "D_2349", "tel_code": "E_692", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2350", "tel_code": "E_692", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2351", "tel_code": "E_692", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2352", "tel_code": "E_692", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6C| etat telephone : Intact*/
{"dep_code": "D_2353", "tel_code": "E_693", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2354", "tel_code": "E_693", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2355", "tel_code": "E_693", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2356", "tel_code": "E_693", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6C| etat telephone : Micro rayure*/
{"dep_code": "D_2357", "tel_code": "E_694", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2358", "tel_code": "E_694", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2359", "tel_code": "E_694", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2360", "tel_code": "E_694", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6C| etat telephone : rayure*/
{"dep_code": "D_2361", "tel_code": "E_695", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2362", "tel_code": "E_695", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2363", "tel_code": "E_695", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2364", "tel_code": "E_695", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 6C| etat telephone : Casse*/
{"dep_code": "D_2365", "tel_code": "E_696", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2366", "tel_code": "E_696", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2367", "tel_code": "E_696", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2368", "tel_code": "E_696", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5X| etat telephone : Intact*/
{"dep_code": "D_2369", "tel_code": "E_697", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2370", "tel_code": "E_697", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2371", "tel_code": "E_697", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2372", "tel_code": "E_697", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5X| etat telephone : Micro rayure*/
{"dep_code": "D_2373", "tel_code": "E_698", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2374", "tel_code": "E_698", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2375", "tel_code": "E_698", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2376", "tel_code": "E_698", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5X| etat telephone : rayure*/
{"dep_code": "D_2377", "tel_code": "E_699", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2378", "tel_code": "E_699", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2379", "tel_code": "E_699", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2380", "tel_code": "E_699", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5X| etat telephone : Casse*/
{"dep_code": "D_2381", "tel_code": "E_700", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2382", "tel_code": "E_700", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2383", "tel_code": "E_700", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2384", "tel_code": "E_700", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5C| etat telephone : Intact*/
{"dep_code": "D_2385", "tel_code": "E_701", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2386", "tel_code": "E_701", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2387", "tel_code": "E_701", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2388", "tel_code": "E_701", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5C| etat telephone : Micro rayure*/
{"dep_code": "D_2389", "tel_code": "E_702", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2390", "tel_code": "E_702", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2391", "tel_code": "E_702", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2392", "tel_code": "E_702", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5C| etat telephone : rayure*/
{"dep_code": "D_2393", "tel_code": "E_703", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2394", "tel_code": "E_703", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2395", "tel_code": "E_703", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2396", "tel_code": "E_703", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 5C| etat telephone : Casse*/
{"dep_code": "D_2397", "tel_code": "E_704", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2398", "tel_code": "E_704", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2399", "tel_code": "E_704", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2400", "tel_code": "E_704", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 8 Pro | etat telephone : Intact*/
{"dep_code": "D_2401", "tel_code": "E_705", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2402", "tel_code": "E_705", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2403", "tel_code": "E_705", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2404", "tel_code": "E_705", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 8 Pro| etat telephone : Micro rayure*/
{"dep_code": "D_2405", "tel_code": "E_706", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2406", "tel_code": "E_706", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2407", "tel_code": "E_706", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2408", "tel_code": "E_706", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 8 Pro| etat telephone : rayure*/
{"dep_code": "D_2409", "tel_code": "E_707", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2410", "tel_code": "E_707", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2411", "tel_code": "E_707", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2412", "tel_code": "E_707", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 8 PRO| etat telephone : Casse*/
{"dep_code": "D_2413", "tel_code": "E_708", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2414", "tel_code": "E_708", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2415", "tel_code": "E_708", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2416", "tel_code": "E_708", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7A| etat telephone : Intact*/
{"dep_code": "D_2417", "tel_code": "E_709", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2418", "tel_code": "E_709", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2419", "tel_code": "E_709", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2420", "tel_code": "E_709", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7A| etat telephone : Micro rayure*/
{"dep_code": "D_2421", "tel_code": "E_710", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2422", "tel_code": "E_710", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2423", "tel_code": "E_710", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2424", "tel_code": "E_710", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7A| etat telephone : rayure*/
{"dep_code": "D_2425", "tel_code": "E_711", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2426", "tel_code": "E_711", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2427", "tel_code": "E_711", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2428", "tel_code": "E_711", "tel_nom": "Cassé", "etat_code": "7A €"},


/* telephone : Honor 7A| etat telephone : Casse*/
{"dep_code": "D_2429", "tel_code": "E_712", "tel_nom": "Intact", "etat_code": "60 €"},
{"dep_code": "D_2430", "tel_code": "E_712", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2431", "tel_code": "E_712", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2432", "tel_code": "E_712", "tel_nom": "Cassé", "etat_code": "7A €"},


/* ceci est un exemple pour un nouveau telephone 

{"dep_code": "D_2433", "tel_code": "E_713", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2434", "tel_code": "E_713", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2435", "tel_code": "E_713", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2436", "tel_code": "E_713", "tel_nom": "Cassé", "etat_code": "7A €"},



{"dep_code": "D_2437", "tel_code": "E_714", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2438", "tel_code": "E_714", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2439", "tel_code": "E_714", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2440", "tel_code": "E_714", "tel_nom": "Cassé", "etat_code": "7A €"},



{"dep_code": "D_2441", "tel_code": "E_715", "tel_nom": "Intact", "etat_code": "600h €"},
{"dep_code": "D_2442", "tel_code": "E_715", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2443", "tel_code": "E_715", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2444", "tel_code": "E_715", "tel_nom": "Cassé", "etat_code": "7A €"},



{"dep_code": "D_2445", "tel_code": "E_716", "tel_nom": "Intact", "etat_code": "60 €"},
{"dep_code": "D_2446", "tel_code": "E_716", "tel_nom": "Micro-rayure", "etat_code": "550 €"},
{"dep_code": "D_2447", "tel_code": "E_716", "tel_nom": "rayure", "etat_code": "550 €"},
{"dep_code": "D_2448", "tel_code": "E_716", "tel_nom": "Cassé", "etat_code": "7A €"},
*/

];
// tri dans l'ordre alpha pour new rÃ©gion
tbl_region_2016.sort(function(a, b){
  if( a.reg_2016_nom < b.reg_2016_nom)
     return( -1);
  if( a.reg_2016_nom > b.reg_2016_nom)
     return( 1);
  return( 0);    
});
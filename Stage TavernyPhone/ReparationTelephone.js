	var tbl_region_2016 = [
{"reg_2016_code": "NR_01", "reg_2016_nom": "Apple"}, 
{"reg_2016_code": "NR_02", "reg_2016_nom": "Samsung"}, 
{"reg_2016_code": "NR_03", "reg_2016_nom": "Huawei"}, 
{"reg_2016_code": "NR_04", "reg_2016_nom": "Sony"}, 
{"reg_2016_code": "NR_05", "reg_2016_nom": "Wiko"},
{"reg_2016_code": "NR_06", "reg_2016_nom": "LG"}, 
{"reg_2016_code": "NR_07", "reg_2016_nom": "Honor"},
];

var tbl_old_region =[ 

/* Téléphone Iphone ici */

{"reg_code": "R_01", "reg_nom": "Iphone XR", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_02", "reg_nom": "Iphone XS Max", "reg_2016_code": "NR_01"},
{"reg_code": "R_03", "reg_nom": "Iphone XS", "reg_2016_code": "NR_01"},  
{"reg_code": "R_04", "reg_nom": "Iphone X", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_05", "reg_nom": "Iphone 8 Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_06", "reg_nom": "Iphone 8", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_07", "reg_nom": "Iphone 7 Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_08", "reg_nom": "Iphone 7", "reg_2016_code": "NR_01"},  
{"reg_code": "R_09", "reg_nom": "Iphone 6s Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_10", "reg_nom": "Iphone 6s", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_11", "reg_nom": "Iphone 6 Plus", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_12", "reg_nom": "Iphone 6", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_13", "reg_nom": "Iphone 5SE", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_14", "reg_nom": "Iphone 5S", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_15", "reg_nom": "Iphone 5C", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_16", "reg_nom": "Iphone 5", "reg_2016_code": "NR_01"}, 
{"reg_code": "R_17", "reg_nom": "Iphone 4s", "reg_2016_code": "NR_01"},
{"reg_code": "R_18", "reg_nom": "Iphone 4", "reg_2016_code": "NR_01"},

/* Téléphone Samsung S */
{"reg_code": "R_19", "reg_nom": "Galaxy S10+", "reg_2016_code": "NR_02"},
{"reg_code": "R_20", "reg_nom": "Galaxy S10E", "reg_2016_code": "NR_02"},
{"reg_code": "R_21", "reg_nom": "Galaxy S10", "reg_2016_code": "NR_02"},
{"reg_code": "R_22", "reg_nom": "Galaxy S9+", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_23", "reg_nom": "Galaxy S9", "reg_2016_code": "NR_02"},
{"reg_code": "R_24", "reg_nom": "Galaxy S8+", "reg_2016_code": "NR_02"},
{"reg_code": "R_25", "reg_nom": "Galaxy S8", "reg_2016_code": "NR_02"},
{"reg_code": "R_26", "reg_nom": "Galaxy S7 Edge", "reg_2016_code": "NR_02"},
{"reg_code": "R_27", "reg_nom": "Galaxy S7", "reg_2016_code": "NR_02"},
{"reg_code": "R_28", "reg_nom": "Galaxy S6 Edge Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_29", "reg_nom": "Galaxy S6 Edge ", "reg_2016_code": "NR_02"},
{"reg_code": "R_30", "reg_nom": "Galaxy S6", "reg_2016_code": "NR_02"},
{"reg_code": "R_31", "reg_nom": "Galaxy S5 K Zoom", "reg_2016_code": "NR_02"},
{"reg_code": "R_32", "reg_nom": "Galaxy S5 Active", "reg_2016_code": "NR_02"},
{"reg_code": "R_33", "reg_nom": "Galaxy S5 4G+", "reg_2016_code": "NR_02"},
{"reg_code": "R_34", "reg_nom": "Galaxy S5", "reg_2016_code": "NR_02"},
{"reg_code": "R_35", "reg_nom": "Galaxy S4 Zoom", "reg_2016_code": "NR_02"},
{"reg_code": "R_36", "reg_nom": "Galaxy S4 Value Edition", "reg_2016_code": "NR_02"},
{"reg_code": "R_37", "reg_nom": "Galaxy S4 Advance 4G", "reg_2016_code": "NR_02"},
{"reg_code": "R_38", "reg_nom": "Galaxy S4 Active", "reg_2016_code": "NR_02"},
{"reg_code": "R_39", "reg_nom": "Galaxy S4 4G (19505)", "reg_2016_code": "NR_02"},
{"reg_code": "R_40", "reg_nom": "Galaxy S4", "reg_2016_code": "NR_02"},
{"reg_code": "R_41", "reg_nom": "Galaxy S3", "reg_2016_code": "NR_02"},
{"reg_code": "R_42", "reg_nom": "Galaxy S2 Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_43", "reg_nom": "Galaxy S2", "reg_2016_code": "NR_02"},
{"reg_code": "R_44", "reg_nom": "Galaxy S Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_45", "reg_nom": "Galaxy S Duos", "reg_2016_code": "NR_02"},
{"reg_code": "R_46", "reg_nom": "Galaxy S5 Mini", "reg_2016_code": "NR_02"},
{"reg_code": "R_47", "reg_nom": "Galaxy S4 Mini Duos", "reg_2016_code": "NR_02"},
{"reg_code": "R_48", "reg_nom": "Galaxy S4 Mini 4G", "reg_2016_code": "NR_02"},
{"reg_code": "R_49", "reg_nom": "Galaxy S4 Mini", "reg_2016_code": "NR_02"},
{"reg_code": "R_50", "reg_nom": "Galaxy S3 Mini", "reg_2016_code": "NR_02"},


/*Telephone Samsung Note */
{"reg_code": "R_51", "reg_nom": "Galaxy Note Edge", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_52", "reg_nom": "Galaxy Note 9", "reg_2016_code": "NR_02"},  
{"reg_code": "R_53", "reg_nom": "Galaxy Note 8", "reg_2016_code": "NR_02"},
{"reg_code": "R_54", "reg_nom": "Galaxy Note 7", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_55", "reg_nom": "Galaxy Note 5", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_56", "reg_nom": "Galaxy Note 4", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_57", "reg_nom": "Galaxy Note 3 Neo", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_58", "reg_nom": "Galaxy Note 3 Lte", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_59", "reg_nom": "Galaxy Note 3 Dual", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_60", "reg_nom": "Galaxy Note 2 Lte", "reg_2016_code": "NR_02"},
{"reg_code": "R_61", "reg_nom": "Galaxy Note 2", "reg_2016_code": "NR_02"}, 
{"reg_code": "R_62", "reg_nom": "Galaxy Note", "reg_2016_code": "NR_02"},
{"reg_code": "R_63", "reg_nom": "Galaxy A9", "reg_2016_code": "NR_02"},
{"reg_code": "R_64", "reg_nom": "Galaxy A8 (2018)", "reg_2016_code": "NR_02"},
{"reg_code": "R_65", "reg_nom": "Galaxy A8", "reg_2016_code": "NR_02"},
{"reg_code": "R_66", "reg_nom": "Galaxy A70", "reg_2016_code": "NR_02"},
{"reg_code": "R_67", "reg_nom": "Galaxy A7 (2018)", "reg_2016_code": "NR_02"},
{"reg_code": "R_68", "reg_nom": "Galaxy A7 (2017)", "reg_2016_code": "NR_02"},
{"reg_code": "R_69", "reg_nom": "Galaxy A7 (2016)", "reg_2016_code": "NR_02"},
{"reg_code": "R_70", "reg_nom": "Galaxy A7", "reg_2016_code": "NR_02"},
{"reg_code": "R_71", "reg_nom": "Galaxy A6 Plus", "reg_2016_code": "NR_02"},
{"reg_code": "R_72", "reg_nom": "Galaxy A6", "reg_2016_code": "NR_02"},
{"reg_code": "R_73", "reg_nom": "Galaxy A50", "reg_2016_code": "NR_02"},
{"reg_code": "R_74", "reg_nom": "Galaxy A5 (2017)", "reg_2016_code": "NR_02"},
{"reg_code": "R_75", "reg_nom": "Galaxy A5 (2016)", "reg_2016_code": "NR_02"},
{"reg_code": "R_76", "reg_nom": "Galaxy A5 (2015)", "reg_2016_code": "NR_02"},
{"reg_code": "R_77", "reg_nom": "Galaxy A40", "reg_2016_code": "NR_02"},
{"reg_code": "R_78", "reg_nom": "Galaxy A30", "reg_2016_code": "NR_02"},
{"reg_code": "R_79", "reg_nom": "Galaxy A3 (2017)", "reg_2016_code": "NR_02"},
{"reg_code": "R_80", "reg_nom": "Galaxy A3 (2016)", "reg_2016_code": "NR_02"},
{"reg_code": "R_81", "reg_nom": "Galaxy A3", "reg_2016_code": "NR_02"},
{"reg_code": "R_82", "reg_nom": "Galaxy Ace 4", "reg_2016_code": "NR_02"},
{"reg_code": "R_83", "reg_nom": "Galaxy Ace 3 S7270", "reg_2016_code": "NR_02"},
{"reg_code": "R_84", "reg_nom": "Galaxy Ace 3", "reg_2016_code": "NR_02"},
{"reg_code": "R_85", "reg_nom": "Galaxy Ace 2", "reg_2016_code": "NR_02"},
{"reg_code": "R_86", "reg_nom": "Galaxy Ace", "reg_2016_code": "NR_02"},

{"reg_code": "R_87", "reg_nom": "Galaxy J7 2017", "reg_2016_code": "NR_02"},
{"reg_code": "R_88", "reg_nom": "Galaxy J7 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_89", "reg_nom": "Galaxy J6 2018", "reg_2016_code": "NR_02"},
{"reg_code": "R_90", "reg_nom": "Galaxy J5 2017", "reg_2016_code": "NR_02"},
{"reg_code": "R_91", "reg_nom": "Galaxy J5 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_92", "reg_nom": "Galaxy J4 2018", "reg_2016_code": "NR_02"},
{"reg_code": "R_93", "reg_nom": "Galaxy J3 2017", "reg_2016_code": "NR_02"},
{"reg_code": "R_94", "reg_nom": "Galaxy J3 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_95", "reg_nom": "Galaxy J1 2016", "reg_2016_code": "NR_02"},
{"reg_code": "R_96", "reg_nom": "Galaxy J1", "reg_2016_code": "NR_02"},

/* telephone huawei */
{"reg_code": "R_97", "reg_nom": "P9 Plus", "reg_2016_code": "NR_03"},
{"reg_code": "R_98", "reg_nom": "P9 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_99", "reg_nom": "P9", "reg_2016_code": "NR_03"},
{"reg_code": "R_100", "reg_nom": "P8 Lite (2017)", "reg_2016_code": "NR_03"},
{"reg_code": "R_101", "reg_nom": "P8", "reg_2016_code": "NR_03"},
{"reg_code": "R_102", "reg_nom": "P30 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_103", "reg_nom": "P30 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_104", "reg_nom": "P30", "reg_2016_code": "NR_03"},
{"reg_code": "R_105", "reg_nom": "P20 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_106", "reg_nom": "P20 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_107", "reg_nom": "P20", "reg_2016_code": "NR_03"},
{"reg_code": "R_108", "reg_nom": "P10 Plus", "reg_2016_code": "NR_03"},
{"reg_code": "R_109", "reg_nom": "P10 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_110", "reg_nom": "P10", "reg_2016_code": "NR_03"},
{"reg_code": "R_111", "reg_nom": "P Smart+", "reg_2016_code": "NR_03"},
{"reg_code": "R_112", "reg_nom": "P Smart", "reg_2016_code": "NR_03"},
{"reg_code": "R_113", "reg_nom": "Porsche Design Mate Rs", "reg_2016_code": "NR_03"},
{"reg_code": "R_114", "reg_nom": "Mate S", "reg_2016_code": "NR_03"},
{"reg_code": "R_115", "reg_nom": "Mate 9", "reg_2016_code": "NR_03"},
{"reg_code": "R_116", "reg_nom": "Mate 8", "reg_2016_code": "NR_03"},
{"reg_code": "R_117", "reg_nom": "Mate 7", "reg_2016_code": "NR_03"},
{"reg_code": "R_118", "reg_nom": "Mate 20X", "reg_2016_code": "NR_03"},
{"reg_code": "R_119", "reg_nom": "Mate 20 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_120", "reg_nom": "Mate 20 Lite", "reg_2016_code": "NR_03"},
{"reg_code": "R_121", "reg_nom": "Mate 20", "reg_2016_code": "NR_03"},
{"reg_code": "R_122", "reg_nom": "Mate 10 Pro", "reg_2016_code": "NR_03"},
{"reg_code": "R_123", "reg_nom": "Mate 10 Lite", "reg_2016_code": "NR_03"},

/* telephone Sony */
{"reg_code": "R_124", "reg_nom": "Xperia Zr", "reg_2016_code": "NR_04"},
{"reg_code": "R_125", "reg_nom": "Xperia Z5 Premium", "reg_2016_code": "NR_04"},
{"reg_code": "R_126", "reg_nom": "Xperia Z5 Dual", "reg_2016_code": "NR_04"},
{"reg_code": "R_127", "reg_nom": "Xperia Z5 Compact", "reg_2016_code": "NR_04"},
{"reg_code": "R_128", "reg_nom": "Xperia Z5 ", "reg_2016_code": "NR_04"},
{"reg_code": "R_129", "reg_nom": "Xperia Z3 Compact", "reg_2016_code": "NR_04"},
{"reg_code": "R_130", "reg_nom": "Xperia Z3", "reg_2016_code": "NR_04"},
{"reg_code": "R_131", "reg_nom": "Xperia Z2", "reg_2016_code": "NR_04"},
{"reg_code": "R_132", "reg_nom": "Xperia Z1 Compact", "reg_2016_code": "NR_04"},
{"reg_code": "R_133", "reg_nom": "Xperia Z1", "reg_2016_code": "NR_04"},
{"reg_code": "R_134", "reg_nom": "Xperia Z Ultra", "reg_2016_code": "NR_04"},
{"reg_code": "R_135", "reg_nom": "Xperia Z", "reg_2016_code": "NR_04"},

/* WIKO */
{"reg_code": "R_136", "reg_nom": "Ufeel", "reg_2016_code": "NR_05"},
{"reg_code": "R_137", "reg_nom": "Tommy", "reg_2016_code": "NR_05"},
{"reg_code": "R_138", "reg_nom": "Ridge", "reg_2016_code": "NR_05"},
{"reg_code": "R_139", "reg_nom": "Rainbow Lite Dual Sim", "reg_2016_code": "NR_05"},
{"reg_code": "R_140", "reg_nom": "Rainbow Jam", "reg_2016_code": "NR_05"},
{"reg_code": "R_141", "reg_nom": "Pulp 4G", "reg_2016_code": "NR_05"},
{"reg_code": "R_142", "reg_nom": "Lenny 3", "reg_2016_code": "NR_05"},
{"reg_code": "R_143", "reg_nom": "Lenny 2", "reg_2016_code": "NR_05"},
{"reg_code": "R_144", "reg_nom": "Highway 4G", "reg_2016_code": "NR_05"},

/* LG */
{"reg_code": "R_145", "reg_nom": "V30", "reg_2016_code": "NR_06"},
{"reg_code": "R_146", "reg_nom": "Optimus G Pro", "reg_2016_code": "NR_06"},
{"reg_code": "R_147", "reg_nom": "Optimus G E975", "reg_2016_code": "NR_06"},
{"reg_code": "R_148", "reg_nom": "Optimus G", "reg_2016_code": "NR_06"},
{"reg_code": "R_149", "reg_nom": "Optimus 4X Hd", "reg_2016_code": "NR_06"},
{"reg_code": "R_150", "reg_nom": "Nexus 5X", "reg_2016_code": "NR_06"},
{"reg_code": "R_151", "reg_nom": "Nexus 5", "reg_2016_code": "NR_06"},
{"reg_code": "R_152", "reg_nom": "Nexus 4", "reg_2016_code": "NR_06"},
{"reg_code": "R_153", "reg_nom": "G7", "reg_2016_code": "NR_06"},
{"reg_code": "R_154", "reg_nom": "G6", "reg_2016_code": "NR_06"},
{"reg_code": "R_155", "reg_nom": "G5", "reg_2016_code": "NR_06"},
{"reg_code": "R_156", "reg_nom": "G4", "reg_2016_code": "NR_06"},
{"reg_code": "R_157", "reg_nom": "G3S", "reg_2016_code": "NR_06"},
{"reg_code": "R_158", "reg_nom": "G3", "reg_2016_code": "NR_06"},
{"reg_code": "R_159", "reg_nom": "G2", "reg_2016_code": "NR_06"},
{"reg_code": "R_160", "reg_nom": "G Pro 2", "reg_2016_code": "NR_06"},
{"reg_code": "R_161", "reg_nom": "G Flex 2", "reg_2016_code": "NR_06"},
{"reg_code": "R_162", "reg_nom": "G Flex", "reg_2016_code": "NR_06"},
{"reg_code": "R_163", "reg_nom": "G3S", "reg_2016_code": "NR_06"},

/*Honor */
{"reg_code": "R_164", "reg_nom": "View 20", "reg_2016_code": "NR_07"},
{"reg_code": "R_165", "reg_nom": "View 10", "reg_2016_code": "NR_07"},
{"reg_code": "R_166", "reg_nom": "Play", "reg_2016_code": "NR_07"},
{"reg_code": "R_167", "reg_nom": "Honor 10", "reg_2016_code": "NR_07"},
{"reg_code": "R_168", "reg_nom": "Honor 9 Lite", "reg_2016_code": "NR_07"},
{"reg_code": "R_169", "reg_nom": "Honor 9", "reg_2016_code": "NR_07"},
{"reg_code": "R_170", "reg_nom": "Honor 8X", "reg_2016_code": "NR_07"},
{"reg_code": "R_171", "reg_nom": "Honor 8", "reg_2016_code": "NR_07"},
{"reg_code": "R_172", "reg_nom": "Honor 7X", "reg_2016_code": "NR_07"},
{"reg_code": "R_173", "reg_nom": "Honor 7 Prenuim", "reg_2016_code": "NR_07"},
{"reg_code": "R_174", "reg_nom": "Honor 6X", "reg_2016_code": "NR_07"},
{"reg_code": "R_175", "reg_nom": "Honor 6C", "reg_2016_code": "NR_07"},
{"reg_code": "R_176", "reg_nom": "Honor 5X", "reg_2016_code": "NR_07"},
{"reg_code": "R_177", "reg_nom": "Honor 5C", "reg_2016_code": "NR_07"},
{"reg_code": "R_178", "reg_nom": "8 pro", "reg_2016_code": "NR_07"},
{"reg_code": "R_179", "reg_nom": "7A", "reg_2016_code": "NR_07"},



/* {"reg_code": "R_180", "reg_nom": "New telephone( mettre le nom du telephone)", "reg_2016_code": "R_07"},
  */


];

var tbl_departement = [
/* ------------------------------------------------------------CHANGER LES PRIX ICI -------------------------------------------------------------------------------------------------------------------*/
/* Iphone ici */

/* Pour changer les prix il faut juste remplacer la phrase ou le prix entre guillemets après le dep_prefecture par exemple dans l iphone XR ce cera Veuillez 8 vous deplacer en magasins */
/* ! ne jamais enlever les guillemets sinon ca peut ne plus marcher il faut juste remplacer entre guillemets */


/* iphone XR*/
{"dep_code": "D_01", "reg_code": "R_01", "dep_nom": "Batterie", "dep_prefecture": "Veuillez 8 vous déplacer en magasin"},
{"dep_code": "D_02", "reg_code": "R_01", "dep_nom": "Ecran", "dep_prefecture": "Veuillez vous déplacer en magasin"},

/* iphone XS MAX */
{"dep_code": "D_03", "reg_code": "R_02", "dep_nom": "Batterie", "dep_prefecture": "Veuillez vous déplacer en magasin"},
{"dep_code": "D_04", "reg_code": "R_02", "dep_nom": "Ecran", "dep_prefecture": "Veuillez vous déplacer en magasin"},

/* IPHONE XS */
{"dep_code": "D_05", "reg_code": "R_03", "dep_nom": "Batterie", "dep_prefecture": "Veuillez vous déplacer en magasin"},
{"dep_code": "D_06", "reg_code": "R_03", "dep_nom": "Ecran", "dep_prefecture": "Veuillez vous déplacer en magasin"},

/* iphone X */
{"dep_code": "D_07", "reg_code": "R_04", "dep_nom": "Batterie", "dep_prefecture": "Veuillez vous déplacer en magasin"},
{"dep_code": "D_08", "reg_code": "R_04", "dep_nom": "Ecran", "dep_prefecture": "Veuillez vous déplacer en magasin"},

/* iphone 8 plus */
{"dep_code": "D_09", "reg_code": "R_05", "dep_nom": "Batterie", "dep_prefecture": "Veuillez vous déplacer en magasin"},
{"dep_code": "D_10", "reg_code": "R_05", "dep_nom": "Ecran", "dep_prefecture": "159 €"},

/* iphone 8 */
{"dep_code": "D_11", "reg_code": "R_06", "dep_nom": "Batterie", "dep_prefecture": "Veuillez vous déplacer en magasin"},
{"dep_code": "D_12", "reg_code": "R_06", "dep_nom": "Ecran", "dep_prefecture": "130 €"},

/* iphone 7 plus */
{"dep_code": "D_13", "reg_code": "R_07", "dep_nom": "Batterie", "dep_prefecture": "70 €"},
{"dep_code": "D_14", "reg_code": "R_07", "dep_nom": "Ecran", "dep_prefecture": "105 €"},

/* iphone 7 */
{"dep_code": "D_15", "reg_code": "R_08", "dep_nom": "Batterie", "dep_prefecture": "70 €"},
{"dep_code": "D_16", "reg_code": "R_08", "dep_nom": "Ecran", "dep_prefecture": "90 €"},

/* iphone 6S Plus */
{"dep_code": "D_17", "reg_code": "R_09", "dep_nom": "Batterie", "dep_prefecture": "60 €"},
{"dep_code": "D_18", "reg_code": "R_09", "dep_nom": "Ecran", "dep_prefecture": "85 €"},

/*IPHONE 6S*/
{"dep_code": "D_19", "reg_code": "R_10", "dep_nom": "Batterie", "dep_prefecture": "50 €"},
{"dep_code": "D_20", "reg_code": "R_10", "dep_nom": "Ecran", "dep_prefecture": "70 €"},

/* iphone 6 Plus */
{"dep_code": "D_21", "reg_code": "R_11", "dep_nom": "Batterie", "dep_prefecture": "60 €"},
{"dep_code": "D_22", "reg_code": "R_11", "dep_nom": "Ecran", "dep_prefecture": "80 €"},

/* iphone 6 */
{"dep_code": "D_23", "reg_code": "R_12", "dep_nom": "Batterie", "dep_prefecture": "50 €"},
{"dep_code": "D_24", "reg_code": "R_12", "dep_nom": "Ecran", "dep_prefecture": "70 €"},

/* iphone 5SE */
{"dep_code": "D_25", "reg_code": "R_13", "dep_nom": "Batterie", "dep_prefecture": "45 €"},
{"dep_code": "D_26", "reg_code": "R_13", "dep_nom": "Ecran", "dep_prefecture": "60 €"},

/* iphone 5S */
{"dep_code": "D_27", "reg_code": "R_14", "dep_nom": "Batterie", "dep_prefecture": "45 €"},
{"dep_code": "D_28", "reg_code": "R_14", "dep_nom": "Ecran", "dep_prefecture": "60 €"},

/* iphone 5C */
{"dep_code": "D_29", "reg_code": "R_15", "dep_nom": "Batterie", "dep_prefecture": "45 €"},
{"dep_code": "D_30", "reg_code": "R_15", "dep_nom": "Ecran", "dep_prefecture": "60 €"},

/* iphone 5 */
{"dep_code": "D_31", "reg_code": "R_16", "dep_nom": "Batterie", "dep_prefecture": "45 €"},
{"dep_code": "D_32", "reg_code": "R_16", "dep_nom": "Ecran", "dep_prefecture": "60 €"},

/* IPHONE 4S */
{"dep_code": "D_33", "reg_code": "R_17", "dep_nom": "Batterie", "dep_prefecture": "30 €"},
{"dep_code": "D_34", "reg_code": "R_17", "dep_nom": "Ecran", "dep_prefecture": "40 €"},

/* Iphone 4 */
{"dep_code": "D_35", "reg_code": "R_18", "dep_nom": "Batterie", "dep_prefecture": "30 €"},
{"dep_code": "D_36", "reg_code": "R_18", "dep_nom": "Ecran", "dep_prefecture": "40 €"},

/* Galaxy S10+ */ 
{"dep_code": "D_37", "reg_code": "R_19", "dep_nom": "Batterie", "dep_prefecture": "s10+ €"},
{"dep_code": "D_38", "reg_code": "R_19", "dep_nom": "Ecran", "dep_prefecture": "s10+ €"},

/* Galaxy S10E */
{"dep_code": "D_39", "reg_code": "R_20", "dep_nom": "Batterie", "dep_prefecture": "S10E€"},
{"dep_code": "D_40", "reg_code": "R_20", "dep_nom": "Ecran", "dep_prefecture": "S10E€"},

/* Galaxy S10 */
{"dep_code": "D_41", "reg_code": "R_21", "dep_nom": "Batterie", "dep_prefecture": "S10 €"},
{"dep_code": "D_42", "reg_code": "R_21", "dep_nom": "Ecran", "dep_prefecture": "S10 €"},

/* Galaxy S9+ */
{"dep_code": "D_43", "reg_code": "R_22", "dep_nom": "Batterie", "dep_prefecture": "S9+ €"},
{"dep_code": "D_44", "reg_code": "R_22", "dep_nom": "Ecran", "dep_prefecture": "s9+ €"},

/* Galaxy S9 */ 
{"dep_code": "D_45", "reg_code": "R_23", "dep_nom": "Batterie", "dep_prefecture": "s9 €"},
{"dep_code": "D_46", "reg_code": "R_23", "dep_nom": "Ecran", "dep_prefecture": "320 €"},

/* Galaxy S8+ */
{"dep_code": "D_47", "reg_code": "R_24", "dep_nom": "Batterie", "dep_prefecture": "80 €"},
{"dep_code": "D_48", "reg_code": "R_24", "dep_nom": "Ecran", "dep_prefecture": "300 €"},

/* Galaxy S8 */
{"dep_code": "D_49", "reg_code": "R_25", "dep_nom": "Batterie", "dep_prefecture": "80 €"},
{"dep_code": "D_50", "reg_code": "R_25", "dep_nom": "Ecran", "dep_prefecture": "280 €"},

/* Galaxy S7 Edge*/
{"dep_code": "D_51", "reg_code": "R_26", "dep_nom": "Batterie", "dep_prefecture": "80 €"},
{"dep_code": "D_52", "reg_code": "R_26", "dep_nom": "Ecran", "dep_prefecture": "220 €"},

/* Galaxy S7  */
{"dep_code": "D_53", "reg_code": "R_27", "dep_nom": "Batterie", "dep_prefecture": "S7 edge €"},
{"dep_code": "D_54", "reg_code": "R_27", "dep_nom": "Ecran", "dep_prefecture": "300 €"},

/* Galaxy S6 Edge Plus */
{"dep_code": "D_55", "reg_code": "R_28", "dep_nom": "Batterie", "dep_prefecture": "80 €"},
{"dep_code": "D_56", "reg_code": "R_28", "dep_nom": "Ecran", "dep_prefecture": "220 €"},

/* Galaxy S6 Edge*/
{"dep_code": "D_57", "reg_code": "R_29", "dep_nom": "Batterie", "dep_prefecture": "x €"},
{"dep_code": "D_58", "reg_code": "R_29", "dep_nom": "Ecran", "dep_prefecture": "220 €"},

/* Galaxy S6 */
{"dep_code": "D_59", "reg_code": "R_30", "dep_nom": "Batterie", "dep_prefecture": "80 €"},
{"dep_code": "D_60", "reg_code": "R_30", "dep_nom": "Ecran", "dep_prefecture": "220 €"},

/* Galaxy S5 K Zoom*/
{"dep_code": "D_61", "reg_code": "R_31", "dep_nom": "Batterie", "dep_prefecture": "800 €"},
{"dep_code": "D_62", "reg_code": "R_31", "dep_nom": "Ecran", "dep_prefecture": "220 €"},

/* Galaxy S5 Active */
{"dep_code": "D_63", "reg_code": "R_32", "dep_nom": "Batterie", "dep_prefecture": "x€"},
{"dep_code": "D_64", "reg_code": "R_32", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S5 4G+ */
{"dep_code": "D_65", "reg_code": "R_33", "dep_nom": "Batterie", "dep_prefecture": "x€"},
{"dep_code": "D_66", "reg_code": "R_33", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S5 */
{"dep_code": "D_67", "reg_code": "R_34", "dep_nom": "Batterie", "dep_prefecture": "x€"},
{"dep_code": "D_68", "reg_code": "R_34", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4 Zoom */
{"dep_code": "D_69", "reg_code": "R_35", "dep_nom": "Batterie", "dep_prefecture": "x€"},
{"dep_code": "D_70", "reg_code": "R_35", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4 Value Edition */
{"dep_code": "D_71", "reg_code": "R_36", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_72", "reg_code": "R_36", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4 Advance 4G*/
{"dep_code": "D_73", "reg_code": "R_37", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_74", "reg_code": "R_37", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4 Active */
{"dep_code": "D_75", "reg_code": "R_38", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_76", "reg_code": "R_38", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4 4G (19505) */
{"dep_code": "D_77", "reg_code": "R_39", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_78", "reg_code": "R_39", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4  */
{"dep_code": "D_79", "reg_code": "R_40", "dep_nom": "Batterie", "dep_prefecture": "  €"},
{"dep_code": "D_80", "reg_code": "R_40", "dep_nom": "Ecran", "dep_prefecture": "150 €"},

/* Galaxy S3  */
{"dep_code": "D_81", "reg_code": "R_41", "dep_nom": "Batterie", "dep_prefecture": " 150 €"},
{"dep_code": "D_82", "reg_code": "R_41", "dep_nom": "Ecran", "dep_prefecture": "€"},

/* Galaxy S2 Plus*/
{"dep_code": "D_83", "reg_code": "R_42", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_84", "reg_code": "R_42", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S2*/
{"dep_code": "D_85", "reg_code": "R_43", "dep_nom": "Batterie", "dep_prefecture": " 85 €"},
{"dep_code": "D_86", "reg_code": "R_43", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S Plus*/
{"dep_code": "D_87", "reg_code": "R_44", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_88", "reg_code": "R_44", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S Duos*/
{"dep_code": "D_89", "reg_code": "R_45", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_90", "reg_code": "R_45", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S5 Mini*/
{"dep_code": "D_91", "reg_code": "R_46", "dep_nom": "Batterie", "dep_prefecture": " 30 "},
{"dep_code": "D_92", "reg_code": "R_46", "dep_nom": "Ecran", "dep_prefecture": "110 €"},

/* Galaxy S4 Mini Duos*/
{"dep_code": "D_93", "reg_code": "R_47", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_94", "reg_code": "R_47", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4 Mini 4G*/
{"dep_code": "D_95", "reg_code": "R_48", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_96", "reg_code": "R_48", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy S4 Mini*/
{"dep_code": "D_97", "reg_code": "R_49", "dep_nom": "Batterie", "dep_prefecture": " 30 €"},
{"dep_code": "D_98", "reg_code": "R_49", "dep_nom": "Ecran", "dep_prefecture": "120 €"},

/* Galaxy S3 Mini*/
{"dep_code": "D_99", "reg_code": "R_50", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_100", "reg_code": "R_50", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note Edge */
{"dep_code": "D_101", "reg_code": "R_51", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_102", "reg_code": "R_51", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 9 */
{"dep_code": "D_103", "reg_code": "R_52", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_104", "reg_code": "R_52", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 8*/
{"dep_code": "D_105", "reg_code": "R_53", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_106", "reg_code": "R_53", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 7*/
{"dep_code": "D_107", "reg_code": "R_54", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_108", "reg_code": "R_54", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 5*/
{"dep_code": "D_109", "reg_code": "R_55", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_110", "reg_code": "R_55", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 4*/
{"dep_code": "D_111", "reg_code": "R_56", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_112", "reg_code": "R_56", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 3 Neo*/
{"dep_code": "D_113", "reg_code": "R_57", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_114", "reg_code": "R_57", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 3 Lte*/
{"dep_code": "D_115", "reg_code": "R_58", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_116", "reg_code": "R_58", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 3 Dual*/
{"dep_code": "D_117", "reg_code": "R_59", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_118", "reg_code": "R_59", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 2 Lte*/
{"dep_code": "D_119", "reg_code": "R_60", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_120", "reg_code": "R_60", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note 2*/
{"dep_code": "D_121", "reg_code": "R_61", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_122", "reg_code": "R_61", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Note */
{"dep_code": "D_123", "reg_code": "R_62", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_124", "reg_code": "R_62", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A9*/
{"dep_code": "D_125", "reg_code": "R_63", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_126", "reg_code": "R_63", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A8 (2018)*/
{"dep_code": "D_127", "reg_code": "R_64", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_128", "reg_code": "R_64", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A8*/
{"dep_code": "D_129", "reg_code": "R_65", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_130", "reg_code": "R_65", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A70*/
{"dep_code": "D_131", "reg_code": "R_66", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_132", "reg_code": "R_66", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A7 (2018)*/
{"dep_code": "D_133", "reg_code": "R_67", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_134", "reg_code": "R_67", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A7 (2017)*/
{"dep_code": "D_135", "reg_code": "R_68", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_136", "reg_code": "R_68", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A7 (2016)*/
{"dep_code": "D_137", "reg_code": "R_69", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_138", "reg_code": "R_69", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A7 */
{"dep_code": "D_139", "reg_code": "R_70", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_140", "reg_code": "R_70", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A6 Plus*/
{"dep_code": "D_141", "reg_code": "R_71", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_142", "reg_code": "R_71", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A6*/
{"dep_code": "D_143", "reg_code": "R_72", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_144", "reg_code": "R_72", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A50*/
{"dep_code": "D_145", "reg_code": "R_73", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_146", "reg_code": "R_73", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A5 (2017)*/
{"dep_code": "D_147", "reg_code": "R_74", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_148", "reg_code": "R_74", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A5 (2016)*/
{"dep_code": "D_149", "reg_code": "R_75", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_150", "reg_code": "R_75", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A5 (2015)*/
{"dep_code": "D_151", "reg_code": "R_76", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_152<", "reg_code": "R_76", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A40 */
{"dep_code": "D_153", "reg_code": "R_77", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_154", "reg_code": "R_77", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A30*/
{"dep_code": "D_155", "reg_code": "R_78", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_156", "reg_code": "R_78", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A3 (2017)*/
{"dep_code": "D_157", "reg_code": "R_79", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_158", "reg_code": "R_79", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A3 (2016)*/
{"dep_code": "D_159", "reg_code": "R_80", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_160", "reg_code": "R_80", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy A3*/
{"dep_code": "D_161", "reg_code": "R_81", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_162", "reg_code": "R_81", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Ace 4*/
{"dep_code": "D_163", "reg_code": "R_82", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_164", "reg_code": "R_82", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Ace 3 S7270*/
{"dep_code": "D_165", "reg_code": "R_83", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_166", "reg_code": "R_83", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Ace 3 */
{"dep_code": "D_167", "reg_code": "R_84", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_168", "reg_code": "R_84", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Ace 2*/
{"dep_code": "D_169", "reg_code": "R_85", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_170", "reg_code": "R_85", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy Ace*/
{"dep_code": "D_171", "reg_code": "R_86", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_172", "reg_code": "R_86", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J7 2017*/
{"dep_code": "D_173", "reg_code": "R_87", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_174", "reg_code": "R_87", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J7 2016*/
{"dep_code": "D_175", "reg_code": "R_88", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_176", "reg_code": "R_88", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J6 2018*/
{"dep_code": "D_177", "reg_code": "R_89", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_178", "reg_code": "R_89", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J5 2017*/
{"dep_code": "D_179", "reg_code": "R_90", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_180", "reg_code": "R_90", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J5 2016*/
{"dep_code": "D_181", "reg_code": "R_91", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_182", "reg_code": "R_91", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J4 2018*/
{"dep_code": "D_183", "reg_code": "R_92", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_184", "reg_code": "R_92", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J3 2017*/
{"dep_code": "D_185", "reg_code": "R_93", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_186", "reg_code": "R_93", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J3 2016*/
{"dep_code": "D_187", "reg_code": "R_94", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_188", "reg_code": "R_94", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J1 2016*/
{"dep_code": "D_189", "reg_code": "R_95", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_190", "reg_code": "R_95", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Galaxy J1*/
{"dep_code": "D_191", "reg_code": "R_96", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_192", "reg_code": "R_96", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Huawei*/
/*P9 Plus */
{"dep_code": "D_193", "reg_code": "R_97", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_194", "reg_code": "R_97", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P9 Lite*/
{"dep_code": "D_195", "reg_code": "R_98", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_196", "reg_code": "R_98", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P9*/
{"dep_code": "D_197", "reg_code": "R_99", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_198", "reg_code": "R_99", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P8 Lite (2017)*/
{"dep_code": "D_199", "reg_code": "R_100", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_200", "reg_code": "R_100", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P8*/
{"dep_code": "D_201", "reg_code": "R_101", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_202", "reg_code": "R_101", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P30 Pro*/
{"dep_code": "D_203", "reg_code": "R_102", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_204", "reg_code": "R_102", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P30 Lite*/
{"dep_code": "D_205", "reg_code": "R_103", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_206", "reg_code": "R_103", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P30*/
{"dep_code": "D_207", "reg_code": "R_104", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_208", "reg_code": "R_104", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P20 Pro*/
{"dep_code": "D_209", "reg_code": "R_105", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_210", "reg_code": "R_105", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P20 Lite*/
{"dep_code": "D_211", "reg_code": "R_106", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_212", "reg_code": "R_106", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P20*/
{"dep_code": "D_213", "reg_code": "R_107", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_214", "reg_code": "R_107", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P10 Plus*/
{"dep_code": "D_215", "reg_code": "R_108", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_216", "reg_code": "R_108", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P10 Lite*/
{"dep_code": "D_217", "reg_code": "R_109", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_218", "reg_code": "R_109", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P10*/
{"dep_code": "D_220", "reg_code": "R_110", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_221", "reg_code": "R_110", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P Smart +*/
{"dep_code": "D_222", "reg_code": "R_111", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_223", "reg_code": "R_111", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*P Smart*/
{"dep_code": "D_224", "reg_code": "R_112", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_225", "reg_code": "R_112", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Porsche Design Mate RS*/
{"dep_code": "D_226", "reg_code": "R_113", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_227", "reg_code": "R_113", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate S*/
{"dep_code": "D_228", "reg_code": "R_114", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_229", "reg_code": "R_114", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 9*/
{"dep_code": "D_230", "reg_code": "R_115", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_231", "reg_code": "R_115", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 8*/
{"dep_code": "D_232", "reg_code": "R_116", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_233", "reg_code": "R_116", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 7 */
{"dep_code": "D_234", "reg_code": "R_117", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_235", "reg_code": "R_117", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 20x*/
{"dep_code": "D_236", "reg_code": "R_118", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_237", "reg_code": "R_118", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 20 Pro*/
{"dep_code": "D_238", "reg_code": "R_119", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_239", "reg_code": "R_119", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 20 Lite*/
{"dep_code": "D_240", "reg_code": "R_120", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_241", "reg_code": "R_120", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 20*/
{"dep_code": "D_242", "reg_code": "R_121", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_243", "reg_code": "R_121", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 10 Pro*/
{"dep_code": "D_244", "reg_code": "R_122", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_245", "reg_code": "R_122", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Mate 10 Lite*/
{"dep_code": "D_246", "reg_code": "R_123", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_247", "reg_code": "R_123", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Sony*/
/*Xperia Zr*/
{"dep_code": "D_248", "reg_code": "R_124", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_249", "reg_code": "R_124", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z5 Prenuim*/
{"dep_code": "D_250", "reg_code": "R_125", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_251", "reg_code": "R_125", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z5 Dual*/
{"dep_code": "D_252", "reg_code": "R_126", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_253", "reg_code": "R_126", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z5 Compact*/
{"dep_code": "D_254", "reg_code": "R_127", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_255", "reg_code": "R_127", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z5 */
{"dep_code": "D_256", "reg_code": "R_128", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_257", "reg_code": "R_128", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z3 Compact*/
{"dep_code": "D_258", "reg_code": "R_129", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_259", "reg_code": "R_129", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z3*/
{"dep_code": "D_260", "reg_code": "R_130", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_261", "reg_code": "R_130", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z2*/
{"dep_code": "D_262", "reg_code": "R_131", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_263", "reg_code": "R_131", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z1 Compact*/
{"dep_code": "D_264", "reg_code": "R_132", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_265", "reg_code": "R_132", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z1*/
{"dep_code": "D_266", "reg_code": "R_133", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_267", "reg_code": "R_133", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z Ultra*/
{"dep_code": "D_268", "reg_code": "R_134", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_269", "reg_code": "R_134", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Xperia Z */
{"dep_code": "D_270", "reg_code": "R_135", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_271", "reg_code": "R_135", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Wiko */
/*ufeel*/
{"dep_code": "D_272", "reg_code": "R_136", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_273", "reg_code": "R_136", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Tommy*/
{"dep_code": "D_274", "reg_code": "R_137", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_275", "reg_code": "R_137", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*ridge*/
{"dep_code": "D_276", "reg_code": "R_138", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_277", "reg_code": "R_138", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*rainbow Lite Dual Sim*/
{"dep_code": "D_278", "reg_code": "R_139", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_279", "reg_code": "R_139", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Rainbow Jam*/
{"dep_code": "D_280", "reg_code": "R_140", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_281", "reg_code": "R_140", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Pulp 4G*/
{"dep_code": "D_282", "reg_code": "R_141", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_283", "reg_code": "R_141", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Lenny 3*/
{"dep_code": "D_284", "reg_code": "R_142", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_285", "reg_code": "R_142", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Lenny 2 */
{"dep_code": "D_286", "reg_code": "R_143", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_287", "reg_code": "R_143", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Highway 4G */
{"dep_code": "D_288", "reg_code": "R_144", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_289", "reg_code": "R_144", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*LG*/
/*V30*/
{"dep_code": "D_290", "reg_code": "R_145", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_291", "reg_code": "R_145", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Optimus G pro */
{"dep_code": "D_292", "reg_code": "R_146", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_293", "reg_code": "R_146", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Optimus G E975*/
{"dep_code": "D_294", "reg_code": "R_147", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_295", "reg_code": "R_147", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Optimus G */
{"dep_code": "D_296", "reg_code": "R_148", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_297", "reg_code": "R_148", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Optimus 4X HD*/
{"dep_code": "D_298", "reg_code": "R_149", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_299", "reg_code": "R_149", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Nexus 5X*/
{"dep_code": "D_300", "reg_code": "R_150", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_301", "reg_code": "R_150", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Nexus 5*/
{"dep_code": "D_302", "reg_code": "R_151", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_303", "reg_code": "R_151", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Nexus 4*/
{"dep_code": "D_304", "reg_code": "R_152", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_305", "reg_code": "R_152", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* G7 */
{"dep_code": "D_306", "reg_code": "R_153", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_307", "reg_code": "R_153", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* G6*/
{"dep_code": "D_308", "reg_code": "R_154", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_309", "reg_code": "R_154", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*G5*/
{"dep_code": "D_310", "reg_code": "R_155", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_311", "reg_code": "R_155", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*G4*/
{"dep_code": "D_312", "reg_code": "R_156", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_313", "reg_code": "R_156", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*G3S*/
{"dep_code": "D_314", "reg_code": "R_157", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_315", "reg_code": "R_157", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* G3*/
{"dep_code": "D_316", "reg_code": "R_158", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_317", "reg_code": "R_158", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*G2*/
{"dep_code": "D_318", "reg_code": "R_159", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_319", "reg_code": "R_159", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* G pro 2*/
{"dep_code": "D_320", "reg_code": "R_160", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_321", "reg_code": "R_160", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* G flex 2 */
{"dep_code": "D_322", "reg_code": "R_161", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_323", "reg_code": "R_161", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* G flex*/
{"dep_code": "D_324", "reg_code": "R_162", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_325", "reg_code": "R_162", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Honor */ 
/* view 20*/
{"dep_code": "D_326", "reg_code": "R_163", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_327", "reg_code": "R_163", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* view 10*/
{"dep_code": "D_328", "reg_code": "R_164", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_329", "reg_code": "R_164", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Play */
{"dep_code": "D_330", "reg_code": "R_165", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_331", "reg_code": "R_165", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 10*/
{"dep_code": "D_332", "reg_code": "R_166", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_333", "reg_code": "R_166", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 9 Lite*/
{"dep_code": "D_334", "reg_code": "R_167", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_335", "reg_code": "R_167", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 9*/
{"dep_code": "D_336", "reg_code": "R_168", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_337", "reg_code": "R_168", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 8X */
{"dep_code": "D_338", "reg_code": "R_169", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_339", "reg_code": "R_169", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 8*/
{"dep_code": "D_340", "reg_code": "R_170", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_341", "reg_code": "R_170", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/*Honor 7X */
{"dep_code": "D_342", "reg_code": "R_171", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_343", "reg_code": "R_171", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 7 prenuim*/
{"dep_code": "D_344", "reg_code": "R_172", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_345", "reg_code": "R_172", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 6X*/
{"dep_code": "D_346", "reg_code": "R_173", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_347", "reg_code": "R_173", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 6c */
{"dep_code": "D_348", "reg_code": "R_174", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_349", "reg_code": "R_174", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 5X*/
{"dep_code": "D_350", "reg_code": "R_175", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_351", "reg_code": "R_175", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* Honor 5C*/
{"dep_code": "D_352", "reg_code": "R_176", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_353", "reg_code": "R_176", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* 9 lite*/
{"dep_code": "D_354", "reg_code": "R_177", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_355", "reg_code": "R_177", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* 8 pro */
{"dep_code": "D_356", "reg_code": "R_178", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_357", "reg_code": "R_178", "dep_nom": "Ecran", "dep_prefecture": "x€"},

/* 7A*/ 
{"dep_code": "D_358", "reg_code": "R_179", "dep_nom": "Batterie", "dep_prefecture": " 55€"},
{"dep_code": "D_359", "reg_code": "R_179", "dep_nom": "Ecran", "dep_prefecture": "x€"},


/* pour creer un nouveau telephone */
/* {"dep_code": "D_358", "reg_code": "R_180", "dep_nom": "Batterie", "dep_prefecture": " x€"},
{"dep_code": "D_359", "reg_code": "R_180", "dep_nom": "Ecran", "dep_prefecture": "x€"},*/

];

// tri dans l'ordre alpha pour new rÃ©gion
tbl_region_2016.sort(function(a, b){
  if( a.reg_2016_nom < b.reg_2016_nom)
     return( -1);
  if( a.reg_2016_nom > b.reg_2016_nom)
     return( 1);
  return( 0);    
});
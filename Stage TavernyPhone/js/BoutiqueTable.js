/**
* Fonction de rÃ©cupÃ©ration des donnÃ©es correspondant au critÃ¨re de recherche
* @param   {String} condition - Chaine indiquant la condition Ã  remplir
* @param   {Array}  table - Tableau contenant les donnÃ©es Ã  extraire
* @returns {Array}  result - Tableau contenant les donnÃ©es extraites
*/
function getDataFromTable( condition, table) {
  // rÃ©cupÃ©ration de la clÃ© et de la valeur
  var cde = condition.replace(/\s/g, '').split('='),
      key = cde[0],
      value = cde[1],
      result = [];
  
  // retour direct si *
  if (condition === '*') {
    return table.slice();
  }
  // retourne les Ã©lÃ©ments rÃ©pondant Ã  la condition
  result = table.filter( function(obj){
       return obj[key] === value;
    });
  return result;
}
/**
* Affichage du nombre d'<option> prÃ©sentes dans le <select>
* @param {Object} obj - <select> parent
* @param {Number} nb - nombre Ã  afficher
*/
function setNombre( obj, nb){
  var oElem = obj.parentNode.querySelector('.nombre');
  if( oElem){
    oElem.innerHTML = nb ? '(' +nb +')' :'';
  }
}
/**
* Fonction d'ajout des <option> Ã  un <select>
* @param   {String} id_select - ID du <select> Ã  mettre Ã  jour
* @param   {Array}  liste - Tableau contenant les donnÃ©es Ã  ajouter
* @param   {String} valeur - Champ pris en compte pour la value de l'<option>
* @param   {String} texte - Champ pris en compte pour le texte affichÃ© de l'<option>
* @returns {String} Valeur sÃ©lectionnÃ©e du <select> pour chainage
*/
function updateSelect( id_select, liste, valeur, texte){
  var oOption,
      oSelect = document.getElementById( id_select),
      i, nb = liste.length;
  // vide le select
  oSelect.options.length = 0;
  // dÃ©sactive si aucune option disponible
  oSelect.disabled = nb ? false : true;
  // affiche info nombre options, facultatif
  setNombre( oSelect, nb);
  // ajoute 1st option
  if( nb){
    oSelect.add( new Option( 'Choisir', ''));
    // focus sur le select
    oSelect.focus();
  }
  // crÃ©ation des options d'aprÃ¨s la liste
  for (i = 0; i < nb; i += 1) {
    // crÃ©ation option
    oOption = new Option( liste[i][texte], liste[i][valeur]);
    // ajout de l'option en fin
    oSelect.add( oOption);
  }
  // si une seule option on la sÃ©lectionne
  oSelect.selectedIndex = nb === 1 ? 1 : 0;
  // on retourne la valeur pour le select suivant
  return oSelect.value;
}
/**
* fonction de chainage des <select>
* @param {String|Object} ID du <select> Ã  traiter ou le <select> lui-mÃªme
*/
function chainSelect( param){
  // affectation par dÃ©faut
  param = param || 'init';
  var liste,
      id     = param.id || param,
      valeur = param.value || '';
      
  // test Ã  faire pour rÃ©cupÃ©ration de la value
  if( typeof id === 'string'){
     param = document.getElementById( id);
     valeur = param ? param.value : '';
   }

  switch (id){
    case 'init':
      // rÃ©cup. des donnÃ©es
      liste = getDataFromTable( '*', tbl_region_2016);
      // mise Ã  jour du select
      valeur = updateSelect( 'new_region', liste, 'reg_2016_code', 'reg_2016_nom');
      // chainage sur le select liÃ©
      chainSelect('new_region');
      break;
    case 'new_region':
      // rÃ©cup. des donnÃ©es
      liste = getDataFromTable( 'reg_2016_code=' +valeur, tbl_old_region);
      // mise Ã  jour du select
      valeur = updateSelect( 'departement', liste, 'reg_code', 'reg_nom');
      // chainage sur le select liÃ©
      chainSelect('departement');
      break;
    case 'departement':
      // affichage final
      document.getElementById('prefecture').value = valeur;
      break;
  }
}
// Initialisation après chargement du DOM
document.addEventListener("DOMContentLoaded", function(event) {
  var oSelects = document.querySelectorAll('#liste select'),
      i, nb = oSelects.length;
  // affectation de la fonction sur le onchange
  for( i = 0; i < nb; i += 1) {
    oSelects[i].onchange = function() {
        chainSelect(this);
      };
  }
  // init du 1st select
  if( nb){
    chainSelect('init');
  }
});
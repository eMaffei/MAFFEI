$( document ).ready(function() {
    $('#btn-name-flask').click(function(){
        let name = $('#name').val();
        $.ajax({
            type: "GET",
            url: "http://localhost:5000/personnage?name="+ name,
            success: function(data){
                $("#resultFlaskCHERCHER").html(data);
            }
        });
    });
    $('#btn-Flask_get').click(function() {
        let login = $('#login').val();
        let mdp = $('#mdp').val();
        $.ajax({
            type: "GET",
            url: "http://localhost:5000/user?login=" + login + "&mdp=" + mdp,
            success: function (data) {
                alert(data);
                $("#resultgetFlask").html(data);
            }
        });
    });
    $('#btn-Flask_post').click(function() {
        let login = $('#login').val();
        let mdp = $('#mdp').val();
        let email = $('#email').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:5000/user?login=" + login + "&email=" +email+ "&mdp=" + mdp,
            success: function (data) {
                alert(data);
                $("#resultpostFlask").html(data);
            }
        });
    });
});
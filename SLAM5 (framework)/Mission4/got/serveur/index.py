from flask import Flask
from flask_cors import CORS
from flask import Flask
from flask import request
from flask import jsonify
from flaskext.mysql import MySQL
import json

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
CORS(app)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'got'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_SOCKET'] = ''
mysql.init_app(app)

@app.route("/bonjour")
def hello():
    return "Bonjour"

@app.route('/personnage/<name>')
def success(name):
    return 'nom= %s' % name

@app.route('/personnage', methods=['GET'])
def getPersonnage():
    name = request.args.get('name')
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM characters WHERE name=%s ',(name))
    data = cursor.fetchall()

    if len(data) != 0:
      return jsonify(data)
    else:
      return json.dumps({'error':str(data[0])})
    conn.close()

@app.route('/user', methods=['GET'])
def getUser():
  login = request.args.get('login')
  mdp = request.args.get('mdp')
  conn = mysql.connect()
  cursor = conn.cursor()
  cursor.execute('SELECT * FROM utilisateur WHERE login=%s AND mdp=%s', (login, mdp))
  data = cursor.fetchall()
  if len(data) != 0:
    return jsonify(data)
  else:
    return json.dumps({'error': ''})
  conn.close()

@app.route('/user', methods=['POST'])
def PostUser():
  login = request.args.get('login')
  mdp = request.args.get('mdp')
  email = request.args.get('email')
  conn = mysql.connect()                           #changer cette fonction
  cursor = conn.cursor()
  cursor.execute('INSERT INTO utilisateur (login,mdp,email) VALUES (%s,%s,%s)'(login,mdp,email))
  data = cursor.fetchall()
  if len(data) is 0:
    return json.dumps({'message':'utilisateur ajouté !'})
  else:
    return json.dumps({'error':str(data[0])})
  conn.close()


if __name__ == '__main__':
    app.run(debug=True)
<?php
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app = new \Slim\App;
$app->get('/bonjour', function(Request $request, Response $response){
    return "wazaaaaaaaa";
});

function connexion()
{
    /*IBM Cloud
     * $vcap_services = json_decode($_ENV['VCAP_SERVICES'], true);
     * $uri = $vcap_services['compose-for-mysql'][0]['credentials']['uri'];
     * $db_creds = parse_url($uri);
     * $dbname = "got";
     * $dsn = "mysql:host=" . $db_creds['host'] . ";port=" . $db_creds['port'] . ";dbname=" . $dbname;
     * return $dbh = new PDO($dsn, $db_creds['user'], $db_creds['pass'],array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
     * */

    return $dbh = new PDO("mysql:host=localhost;dbname=got", 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}

$app->get('/characters', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $name = $tb["name"];
    return getPersonnage($name);
});

function getPersonnage($name)
{
    $sql = "SELECT * FROM characters WHERE name=:name";// name
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":name", $name);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

$app->get('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $login = $tb["login"];
    $mdp = $tb["mdp"];
    //fonction de vérification d'utilisateur
    return checkUser($login, $mdp);
});

function checkUser($login, $mdp)
{
    $sql = "SELECT * FROM utilisateur WHERE login=:login AND mdp=:mdp";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":login",$login);
        $statement->bindParam(":mdp",$mdp);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

$app->post('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $login = $tb["login"];
    $mdp = $tb["mdp"];
    $email = $tb["email"];
    return insertUser($login , $mdp , $email );
});

function insertUser($login, $mdp)
{
    $sql = "INSERT INTO utilisateur( login , mdp, email  ) VALUES ( :login , :mdp , :email )";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":login",$login);
        $statement->bindParam(":mdp",$mdp);
        $statement->bindParam(":email",$email);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
    }
}

$app->post('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $id = $tb["id"];
    return DeleteUser($id);
});

function DeleteUser($id)
{
    $sql = "DELETE FROM utilisateur WHERE id=:id";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id",$id);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}


$app->run();
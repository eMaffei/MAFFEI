var express = require('express');
var cors= require('cors');
var port = 8080;
var hostname = 'localhost';
var app = express();
var mysql      = require('mysql');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'got',
});
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
connection.connect();

var myRouter = express.Router();

myRouter.route('/bonjour').get(function(req,res){
    res.json({message : "Youhou", methode : req.method});
})

myRouter.route('/characters/:name').get(function(req,res,next){
    let name = req.params.name;
    console.log(name);
    connection.query('SELECT * from characters WHERE name=?', [name],function (error, results, fields) {
        if (error) throw error;
        res.json({results});
    });
})

myRouter.route('/user').get(function(req,res){

    let token = req.query.token;
    if (verifToken(token)) {
        let login= req.query.login;
        let mdp= req.query.mdp;
        let result = CheckUser(login, mdp).then(function (value) {
            res.json({result : value});
        });
    }

    else {
        res.sendStatus(401);
    }
});


myRouter.route('/user').post(function(req,res){
    let login= req.query.login;
    let mdp= req.query.mdp;
    let email= req.query.email;
    console.log(login,mdp,email);
    connection.query('insert into utilisateur (login, mdp, email) values(?,?,?)', [login, mdp, email], function (error, results, fields) {
        if (error) throw error;
        return "ça n'as pas fonctionné";});
    res.json({ message : "utilisateur ajouté",});
})


myRouter.route('/user1').post(function(req,res){

    let token = req.query.token;
    if (verifToken(token)) {
        let id = req.params.id;
        let result = deleteUser(id).then(function (value) {
            res.json({result : value});
        });
    }

    else {
        res.sendStatus(401);
    }
});


myRouter.route('/userUpdate').post(function (req,res) {

    let token = req.query.token;
    if (verifToken(token)) {
        let id = req.params.id;
        let email = req.params.email;

        updateEmail(id, email).then(function (value) {
            res.json({updated : value});
        });
    }
    else {
        res.sendStatus(401);
    }
});

myRouter.route('/Personnage100').get(function (req,res) {
    connection.query("select NAME, culture from characters limit 100", function (error, results, fields) {
        if (error) throw error;
        res.json({results});

    })
});

myRouter.route('/obtentionToken').get(function(req,res) {

    let login = req.query.login;
    let mdp = req.query.mdp;

    CheckUser(login, mdp).then(function (allowed) {
        if (allowed) {
            let token = getToken(login, mdp);
            res.json({ success: true, message: 'je veux jouer à lol laisser moi trkl',token : token });
        }
        else {
            res.sendStatus(401);
        }
    });
});


function CheckUser(login, mdp) {
    let result;
    connection.query("SELECT * FROM utilisateur WHERE login = ? AND mdp = ?", [login, mdp], function (error, results) {
        if (error) throw error;

        result = results.length > 0;
    });

    return new Promise(resolve => {
        setTimeout(() => {
            resolve(result);
        }, 100);
    });
}

function deleteUser(id) {
    let result;
    connection.query("DELETE FROM utilisateur WHERE id=?", [id], function (error, results) {
        if (error) throw error;
        nbrows = results.affectedRows;

        result = nbrows > 0;
    });

    return new Promise(resolve => {
        setTimeout(() => {
            resolve(result);
        }, 100);
    });
}

function updateEmail(id, email) {
    let result;
    connection.query("UPDATE utilisateur SET email=? WHERE id=?", [email, id], function (error, results) {
        if (error) throw error;
        nbrows = results.affectedRows;

        result = nbrows > 0;
    });

    return new Promise(resolve => {
        setTimeout(() => {
            resolve(result);
        }, 100);
    });
}

function getToken(login, mdp) {
    return jwt.sign({ login : login, mdp : mdp }, 'Oskour', { expiresIn: 60 * 30, algorithm: 'HS256' });
}

function verifToken(token) {
    try {
        let decoded = jwt.verify(token, 'Oskour', { algorithm: 'HS256' });
        return CheckUser(decoded.login, decoded.mdp);
    }
    catch(err) {
        return false;
    }
}


app.use(myRouter);

// Démarrer le serveur

app.listen(port, hostname, function() {
    console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
    let token = getToken("Oskour", "Oskour");
    console.log(token);

    console.log(jwt.verify(token, 'Oskour', { algorithm: 'HS256' }));

    verifToken(token).then(function (result) {
        console.log("token : " + result);
    });

});

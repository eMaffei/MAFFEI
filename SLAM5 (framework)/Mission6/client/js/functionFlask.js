$( document ).ready(function() {
    $('#btn-name-flask').click(function(){
        let name = $('#name').val();
        $.ajax({
            type: "GET",
            url: "http://localhost:5000/personnage?name="+ name,
            success: function(data){
                $("#resultFlaskCHERCHER").html(data);
            }
        });
    });
    $('#btn-Flask_get').click(function() {
        let login = $('#login').val();
        let mdp = $('#mdp').val();
        $.ajax({
            type: "GET",
            url: "http://localhost:5000/user?login=" + login + "&mdp=" + mdp,
            success: function (data) {
                alert(data);
                $("#resultgetFlask").html(data);
            }
        });
    });
    $('#btn-Flask_post').click(function() {
        let login = $('#login').val();
        let mdp = $('#mdp').val();
        let email = $('#email').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:5000/user?login=" + login + "&email=" +email+ "&mdp=" + mdp,
            success: function (data) {
                console.log(data)
                $("#resultpostFlask").html(JSON.stringify(data));
            }
        });
    });
    $('#btndeleteFlask').click(function(){
        let id=$('#id').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:5000/user1?id=" +id ,
            success: function(data){
                console.log(data)
                $('#resultFlaskdelete').html(JSON.stringify(data));

            }
        });
    });
    $('#btn-UpdateUserFlask').click(function(){
        let id =$('#id').val();
        let email=$('#email').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:5000/userUpdate?id=" +id+ "&email=" +email,
            success: function(data){
                $("#resultflaskUpdate").html(data);
            }
        });
    });
    $('#btnPersonnagesFlask').click(function(){
        $.ajax({
            type: "GET",
            url: "http://localhost:5000/Personnage100",
            success: function(data){
                $('#resultPersonnages').html(JSON.stringify(data, null, 4));
            }
        });
    });
});
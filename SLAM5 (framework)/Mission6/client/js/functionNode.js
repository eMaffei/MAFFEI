$( document ).ready(function () {
    $( "#btnGetSessionTokenNode").click(function(event) {
        let ndc = $('#ndcGetUser').val();
        let mdp = $('#mdpGetUser').val();
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/obtentionToken",
            data: "login=" + ndc + "&mdp="+ mdp,
            success: function(data){
                $("#ndcGetUser").removeClass("is-invalid")
                    .addClass("is-valid");

                $('#mdpGetUser').removeClass("is-invalid")
                    .addClass("is-valid");

                console.log(data);
                localStorage.setItem('token', data.token);
                $("#result").html(JSON.stringify(data, undefined, 2));
                Prism.highlightElement($("#result")[0]);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $("#ndcGetUser").addClass("is-invalid");
                $('#mdpGetUser').addClass("is-invalid");
            }
        });
    });

    $('#btn-NodeSearch').click(function () {
        let name = $('#name').val();
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/characters/" + name,
            success: function (data) {
                console.log(data)
                $('#resultSearch').html(JSON.stringify(data));
            }
        });
    });
    $('#btnNode').click(function () {
        let login = $('#login').val();
        let mdp = $('#mdp').val();
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/user?login=" + login + "&mdp=" + mdp,
            success: function (data) {
                console.log(data)
                $('#resultCon').html(JSON.stringify(data));
            }
        });
    });

    $('#btn2Node').click(function () {
        let login = $('#login').val();
        let email = $('#email').val();
        let mdp = $('#mdp').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/user?login=" + login + "&email=" + email + "&mdp=" + mdp,
            success: function (data) {
                console.log(data)
                $('#resultNodepost').html(JSON.stringify(data));

            }
        });
    });
    $('#btndeleteNode').click(function () {
        let id = $('#id').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/user1?id=" + id,
            success: function (data) {
                console.log(data)
                $('#resultNodedelete').html(JSON.stringify(data));

            }
        });
    });
    $('#btn-UpdateUserNode').click(function () {
        let id = $('#id').val();
        let email = $('#email').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/userUpdate?id=" + id + "&email=" + email,
            success: function (data) {
                $("#resultNodeUpdate").html(data);
            }
        });
    });
    $('#btnPersonnagesNode').click(function () {
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/Personnage100",
            success: function (data) {
                console.log(data)
                $('#resultPersonnages').html(JSON.stringify(data, null, 4));
            }
        });
    });
})



<?php

include_once "Characters.php";
include_once "Maison.php";

class Noble extends Characters
{
    private $epoux;
    private $pere;
    private $mere;
    private $maMaison;

    /**
     * Noble constructor.
     * @param $epoux
     * @param $pere
     * @param $mere
     */
    public function __construct($nom, $id, $datedenaissance, $datedemort, $culture, $genre , $epoux, $pere, $mere, $maMaison)
    {
        parent::__construct($nom, $id, $datedenaissance, $datedemort ,$culture , $genre);
        $this->epoux = $epoux;
        $this->pere = $pere;
        $this->mere = $mere;
        $this->maMaison = $maMaison;
    }
    /**
     * @return mixed
     */
    public function getEpoux()
    {
        return $this->epoux;
    }

    /**
     * @param mixed $epoux
     */
    public function setEpoux($epoux)
    {
        $this->epoux = $epoux;
    }

    /**
     * @return mixed
     */
    public function getPere()
    {
        return $this->pere;
    }

    /**
     * @param mixed $pere
     */
    public function setPere($pere)
    {
        $this->pere = $pere;
    }

    /**
     * @return mixed
     */
    public function getMere()
    {
        return $this->mere;
    }

    /**
     * @param mixed $mere
     */
    public function setMere($mere)
    {
        $this->mere = $mere;
    }
}

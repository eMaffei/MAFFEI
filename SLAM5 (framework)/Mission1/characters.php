<?php
abstract class Characters
{
    private $nom;
    private $id;
    private $datedenaissance;
    private $datedemort;
    private $culture;
    private $genre;
    private static $nombreperso;

    /**
     * Characters constructor.
     * @param $nom
     * @param $id
     * @param $datedenaissance
     * @param $datedemort
     * @param $culture
     * @param $genre
     */
    public function __construct($nom, $id, $datedenaissance, $datedemort, $culture, $genre)
    {
        $this->nom = $nom;
        $this->id = $id;
        $this->datedenaissance = $datedenaissance;
        $this->datedemort = $datedemort;
        $this->culture = $culture;
        $this->genre = $genre;
        self::$nombreperso++;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDatedenaissance()
    {
        return $this->datedenaissance;
    }

    /**
     * @param mixed $datedenaissance
     */
    public function setDatedenaissance($datedenaissance)
    {
        $this->datedenaissance = $datedenaissance;
    }

    /**
     * @return mixed
     */
    public function getDatedemort()
    {
        return $this->datedemort;
    }

    /**
     * @param mixed $datedemort
     */
    public function setDatedemort($datedemort)
    {
        $this->datedemort = $datedemort;
    }

    /**
     * @return mixed
     */
    public function getCulture()
    {
        return $this->culture;
    }

    /**
     * @param mixed $culture
     */
    public function setCulture($culture)
    {
        $this->culture = $culture;
    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return mixed
     */
    public static function getNombreperso()
    {
        return self::$nombreperso;
    }

    /**
     * @param mixed $nombreperso
     */
    public static function setNombreperso($nombreperso)
    {
        self::$nombreperso = $nombreperso;
    }

    public function __toString()
    {
        return "MyClass{" . "identifiant='"  .$this->getIdentifiant().  '\'' .
            ", nom='" . $this->getNom() . '\'' .
            ", dateNais='" . $this->getDatedenaissance(). '\'' .
            ", dateMort='" . $this->getDatedemort(). '\'' .
            ", culture='" . $this->getCulture() . '\'' .
            ", genre='" . $this->getGenre() .'\'}';
    }




}

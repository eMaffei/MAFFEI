<?php
class Maison
{
    private $nom;
    private $devise;
    private $armoiries;
    private $datedefondation;
    private $region;

    /**
     * Maison constructor.
     * @param $nom
     * @param $devise
     * @param $armoiries
     * @param $datedefondation
     */
    public function __construct($nom, $devise, $armoiries, $datedefondation ,$region)
    {
        $this->nom = $nom;
        $this->devise = $devise;
        $this->armoiries = $armoiries;
        $this->datedefondation = $datedefondation;
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getDevise()
    {
        return $this->devise;
    }

    /**
     * @param mixed $devise
     */
    public function setDevise($devise)
    {
        $this->devise = $devise;
    }

    /**
     * @return mixed
     */
    public function getArmoiries()
    {
        return $this->armoiries;
    }

    /**
     * @param mixed $armoiries
     */
    public function setArmoiries($armoiries)
    {
        $this->armoiries = $armoiries;
    }

    /**
     * @return mixed
     */
    public function getDatedefondation()
    {
        return $this->datedefondation;
    }

    /**
     * @param mixed $datedefondation
     */
    public function setDatedefondation($datedefondation)
    {
        $this->datedefondation = $datedefondation;
    }
    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

}


<?php
class Hero extends Characters
{
    private $nomActeur;

    /**
     * Hero constructor.
     * @param $nomActeur
     */
    public function __construct($nom, $id, $datedenaissance, $datedemort, $nomActeur ,$culture ,$genre)
    {
        parent::__construct($nom, $id, $datedenaissance, $datedemort ,$culture ,$genre);
        $this->nomActeur = $nomActeur;
    }


    /**
     * @return mixed
     */
    public function getNomActeur()
    {
        return $this->nomActeur;
    }

    /**
     * @param mixed $nomActeur
     */
    public function setNomActeur($nomActeur)
    {
        $this->nomActeur = $nomActeur;
    }

}
$(document).ready(function() {
    $('#submit').click(function () {
        let email = document.getElementById("email").value;
        let mdp = document.getElementById("mdp").value;

        console.log(email+" - "+mdp);
        $.ajax({
            url: "connexion.php",
            type: "POST",
            data: {
                email: email,
                mdp: mdp
            },
            success: function (r) {
                console.log(r);
                $("#debug").html(""+r+"");
            }
        });
    });

    $('#tablehouse').click(function () {
        $( document ).ready(function() {
            $('#table').bootstrapTable({
                url: 'https://api.got.show/api/show/houses',
                columns: [{
                    field: 'seat',
                    title: 'GOT'
                }, {
                    field: 'allegiance',
                    title: ''
                }, {
                    field: 'region',
                    title: 'region'
                }, {
                    field: 'location',
                    title: 'localisation'
                }, {
                    field: 'name',
                    title: 'nom'
                }, {
                    field: 'worlds',
                    title: 'monde'

                },]
            });
        });
    });

    $('#tablepersonnage').click(function ()
    {
        let personnageName = document.getElementById("personnageName").value;
        let data;
        let json;

        if(personnageName != "")
        {
            $.ajax(
                {
                    url: "https://api.got.show/api/show/characters/" + personnageName,
                    type: "GET",

                    success : function (resultat) {

                        data = "[" + JSON.stringify(resultat) + "]";
                        json = JSON.parse(data);

                        if(json[0]['house'] != "" || json[0]['house'] != null)
                        {
                            let house = json[0]['house'];

                            $.ajax({
                                url: 'https://api.got.show/api/show/houses/' + house,
                                type: 'GET',
                                succes:function (houseParams)
                                {
                                    console.log(house);
                                    console.log(houseParams[0]);
                                }


                            })

                        }
                        $('#table1').bootstrapTable({

                            data: json,
                            columns: [
                                {
                                    field: '_id',
                                    title: 'ID'
                                }, {
                                    field: 'name',
                                    title: 'Name'
                                }, {
                                    field: 'titles',
                                    title: 'Titles'
                                }, {
                                    field: 'house',
                                    title: 'Maison'
                                }]

                        });

                    }


                });
        }
        else
        {
            $('#table1').bootstrapTable({
                url: "https://api.got.show/api/show/characters/" + personnageName,
                columns: [
                    {
                        field: '_id',
                        title: 'ID'
                    }, {
                        field: 'name',
                        title: 'Name'
                    }, {
                        field: 'titles',
                        title: 'Titles'
                    }, {
                        field: 'house',
                        title: 'Maison'
                    }]

            });
        }


    });



    $('#tableville').click(function () {
        $( document ).ready(function() {
            $('#table').bootstrapTable({
                url: 'https://api.got.show/api/show/cities',
                columns: [{
                    field: 'rulers',
                    title: 'GOT'
                }, {
                    field: 'name',
                    title: 'Item Name'
                }, {
                    field: 'founder',
                    title: ''
                }, {
                    field: 'location',
                    title: 'localisation'
                }, {
                    field: 'type',
                    title: 'le type'
                }, {
                    field: 'name',
                    title: 'Item Name'

                },]
            });
         });
    });
});

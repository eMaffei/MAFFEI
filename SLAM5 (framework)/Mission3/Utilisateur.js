class Utilisateur {
	email;
	nom;
	constructor(email, nom) {
		this.email = email;
		this.nom = nom;
	}


	get email() {
		return this._email;
	}

	set email(value) {
		this._email = value;
	}

	get nom() {
		return this._nom;
	}

	set nom(value) {
		this._nom = value;
	}
}

let user = new Utilisateur("enzomaffei8@gmail.com","maffei");
console.log(user.email + " et " + user.nom);

var express = require('express');
var cors= require('cors');
var port = 8080;
var hostname = 'localhost';
var app = express();

app.use(cors());

var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'got',
});
connection.connect();

var myRouter = express.Router();


myRouter.route('/bonjour').get(function(req,res){
    res.json({message : "Youhou", methode : req.method});
})

myRouter.route('/characters/:name').get(function(req,res,next){
    let name = req.params.name;
    console.log(name);
    connection.query('SELECT * from characters WHERE name=?', [name],function (error, results, fields) {
        if (error) throw error;
        res.json({results});
    });
})

myRouter.route('/user').get(function(req,res){
    let login= req.query.login;
    let mdp= req.query.mdp;
    console.log( login,mdp);
    connection.query('SELECT * from utilisateur WHERE login=? AND mdp = ?', [login,mdp], function (error, results, fields) {
        if (error) throw error;
        res.json({results});
    });

})

myRouter.route('/user').post(function(req,res){
    let login= req.query.login;
    let mdp= req.query.mdp;
    let email= req.query.email;
    console.log(login,mdp,email);
    connection.query('insert into utilisateur (login, mdp, email) values(?,?,?)', [login, mdp, email], function (error, results, fields) {
        if (error) throw error;
        return "ça n'as pas fonctionné";});
    res.json({ message : "utilisateur ajouté",});
})


myRouter.route('/user1').post(function(req,res){
    let id= req.query.id;
    console.log(id);
    connection.query('delete from utilisateur WHERE id=?', [id], function (error, results, fields) {
        if (error) throw error;
        return "ça n'as pas fonctionné";
    });
    res.json({ message : "utilisateur supprimé",});
})


myRouter.route('/userUpdate').post(function (req,res) {
    let id = req.query.id;
    let email = req.query.email
    console.log(id, email);
    connection.query("UPDATE utilisateur SET email =? WHERE id =?", [email,id], function (error, results, fields) {
        if (error) throw error;
        return "ça n'as pas fonctionné";
    });
    res.json({message : 'utilisateur Update'});
})

myRouter.route('/Personnage100').get(function (req,res) {
    connection.query("select NAME, culture from characters limit 100", function (error, results, fields) {
        if (error) throw error;
        res.json({results});

    })
});

app.use(myRouter);

// Démarrer le serveur

app.listen(port, hostname, function() {
    console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});
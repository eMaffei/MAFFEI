#crible d'erathostène
#Enzo maffei

#taille de la liste
taille = 20

#initialisation de L par 1
L = [1]*taille

#0 et 1 ne sont pas premiers : 
L[0] =0
L[1] =0

#suprression des 1 d'indices pair
indice =2
for i in range(3) :
    indice +=2
    L[indice] = 0


#affichage de la liste L
print(L)

#Extraction des indices dont la valeur est égale a 1
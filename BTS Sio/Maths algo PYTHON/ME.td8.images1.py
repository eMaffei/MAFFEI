#les dessins avec les 1 et 0 td8
#Maffei Enzo

            
E=[] #la liste E
for i in range (5): #pour chaque ligne i
    for j in range (5): #pour chaque colonne j
        if i==2 or j==2: #ce sont les valeurs du 1er carré
            E.append(1) #on mets les 1
        else:
            E.append(0)#on mets les 0


O=[] #la liste O
for i in range (5): #pour chaque ligne i
    for j in range (5): #pour chaque colonne j
        if i==j or j+i==4   : #ce sont les valeurs du 2eme carré
            O.append(1) #on mets les 1
        else:
            O.append(0)#on mets les 0
            
L=[] #la liste L
for i in range (5): #pour chaque ligne i
    for j in range (5): #pour chaque colonne j
        if i==0 or i==4 or j==0 or j==4: #ce sont les valeurs du 3eme carré
            L.append(1) #on mets les 1
        else:
            L.append(0)#on mets les 0

#-----main------#
for i in range(5):
    print(E[5*i:5*(i+1)]) #afficher le 1er carré
    
print()#pour faire un espace entre les 2 figures

for i in range(5):
    print(O[5*i:5*(i+1)]) #afficher le 2eme carré
    
print()#pour faire un espace entre les 2 figures

for i in range(5): #afficher le 3eme carré
    print(L[5*i:5*(i+1)]) 


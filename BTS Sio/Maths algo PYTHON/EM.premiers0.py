#Nombres premiers 0
#Maffei enzo

#-----Fonctions-----#
def primalite(n):
    rep = True # quand la reponse est vraie
    for d in range (2,n):
        if n% d==0:
            rep = False #quand la reponse est faux et donc pas un nombre entier
    return rep

#----MAIN----#
N=int (input("veuillez saisir votre entier"))
if primalite(N):
    print("L'entier" ,N, "est premier")
else:
    print("L'entier" ,N, "n'est pas premier")

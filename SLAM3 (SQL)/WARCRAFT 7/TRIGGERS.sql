1) 
DELIMITER //
CREATE TRIGGER reni_lvl BEFORE UPDATE ON members
FOR EACH ROW
IF NEW.members_character_id <> OLD.members_character_id THEN
SET NEW.members_character_level=1;
END IF;
END //  

UPDATE members SET members_character_name = 'florian'
WHERE members_character_id = 1;

2)
DELIMITER //
CREATE TRIGGER add_level_mounts AFTER INSERT ON mounts_members
FOR EACH ROW
BEGIN
UPDATE members SET members_character_level = members.members_character_level=1 
WHERE members.members_character_id = NEW.name;
END; //  

3)
DELIMITER //
CREATE TRIGGER death_knight BEFORE INSERT ON members FOR EACH ROW
BEGIN
IF NEW.members_character_class = 6 THEN
SET NEW.members_character_level = 70;
END IF;
END //

4.
DELIMITER //
CREATE TRIGGER stop_del BEFORE DELETE ON members 
FOR EACH NOW 
BEGIN 
IF OLD.members_characters_level >= 100 THEN
DELETE FROM oui;
END IF;
END;
//;

5)
DELIMITER //
CREATE TRIGGER interdit_choisi BEFORE INSERT ON
members
FOR EACH ROW
BEGIN
  SELECT races.races_side INTO @raceUser FROM races WHERE races.races_id = NEW.members_character_race;

  CASE NEW.members_character_class
    WHEN 2 THEN
      IF @raceUser = 1 THEN
        CALL cannot_insert_error;
      END IF;
    WHEN 11 THEN
      IF @raceUser = 2 THEN
        CALL cannot_insert_error;
      END IF;
    END CASE;
END //

6)
DELIMITER //
CREATE TRIGGER mounts_rand AFTER INSERT ON
members
FOR EACH ROW
BEGIN
  SELECT mounts.mounts_creature_id INTO @mounts FROM mounts ORDER BY RAND() LIMIT 1;
   INSERT INTO mounts_members(mounts_collected_id, name) VALUES (@mounts,NEW.members_character_id);
END //

PARTIE 2

1)
DELIMITER //
CREATE PROCEDURE pet_rand(IN pets_member INT)
BEGIN
DECLARE V1 INT UNSIGNED DEFAULT 0;
WHILE V1 < pets_member DO
SELECT pets.pets_id INTO @petsrand FROM pets ORDER BY RAND() LIMIT 1;
SELECT members.members_character_id INTO @mounts_rand FROM memners ORDER BY RAND() LIMIT 1;
INSERT INTO pets_members VALUES(@pets_rand, @members_rand);
SET V1 = V1 + 1;
END WHILE;
END //

DELIMITER // 
CREATE FUNCTION pet_x_joueurs(randid int) RETURNS int
BEGIN
DECLARE petgib int;

SELECT pets_id INTO petgib FROM pets ORDER BY RAND() LIMIT 1;
INSERT INTO pets_members VALUES (petgib,randid);
RETURN petgib;
END;

//;


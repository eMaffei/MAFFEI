from tkinter import *
from math import *
from random import *


#x = random.randint(0,Largeur)
#y = random.randint(0,longueur)
#Canevas.create_oval(x, y, x, y, fill='orange')


def deplacement():
    global dx, dy

    if Canevas.coords(snake)[3] > 800:
        dy = -1 * dy

    # On deplace le snake
    Canevas.move(snake, dx, dy)

    # On repete pour le deplacement continue
    root.after(20, deplacement)


def Droite(event):
    global dx, dy
    dx = 5
    dy = 0

def Gauche(event):
    global dx, dy
    dx = -5
    dy = 0

# en haut
def Haut(event):
    global dx, dy
    dx = 0
    dy = -5

# Et en bas
def Bas(event):
    global dx, dy
    dx = 0
    dy = 5

#deplacement du snake au depart donc neutre
dx = 0
dy = 0

# position de depart du snake
PosY=20
PosX=20

# taille du jeu fenetre
Largeur = 800
longueur = 800

# création de l'instance
root = Tk()

#le nom du jeu en titre
root.title("Snake")

# creation de LA FENETRE
Canevas = Canvas(root, width=Largeur, height=longueur, bg="grey")
Canevas.pack(padx=10, pady=10)

# creation du snake
snake = Canevas.create_rectangle(PosX, PosX, PosY+10, PosX+10, width=10, fill='red')

# affectation a la touche de droite, gauche, haut et bas
Canevas.bind_all('<Right>', Droite)
Canevas.bind_all('<Left>', Gauche)
Canevas.bind_all('<Up>', Haut)
Canevas.bind_all('<Down>', Bas)


#lancement de la fonction
deplacement()

#ouvrir l appli
root.mainloop()
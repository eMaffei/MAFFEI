-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `classes_id` int(11) NOT NULL,
  `classes_name` varchar(20) DEFAULT NULL,
  `classes_power_type` varchar(11) DEFAULT NULL,
  `races_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`classes_id`),
  KEY `races_id` (`races_id`),
  CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`races_id`) REFERENCES `races` (`races_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `classes` (`classes_id`, `classes_name`, `classes_power_type`, `races_id`) VALUES
(1,	'Guerrier',	'rage',	1),
(2,	'Paladin',	'mana',	1),
(3,	'Chasseur',	'focus',	5),
(4,	'Voleur',	'energy',	6),
(5,	'Prêtre',	'mana',	8),
(7,	'Chaman',	'mana',	3),
(8,	'Mage',	'mana',	4),
(9,	'Démoniste',	'mana',	5),
(10,	'Moine',	'energy',	2),
(11,	'Druide',	'mana',	9);

DROP TABLE IF EXISTS `mounts`;
CREATE TABLE `mounts` (
  `mounts_creature_id` int(11) NOT NULL,
  `mounts_icon` varchar(80) DEFAULT NULL,
  `mounts_item_id` int(11) DEFAULT NULL,
  `mounts_name` varchar(60) DEFAULT NULL,
  `mounts_quality_id` int(11) DEFAULT NULL,
  `mounts_spell_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mounts_creature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mounts` (`mounts_creature_id`, `mounts_icon`, `mounts_item_id`, `mounts_name`, `mounts_quality_id`, `mounts_spell_id`) VALUES
(-81772,	'inv_dogmount',	0,	'Shu Zen, la sentinelle divine',	1,	259395),
(-80358,	'inv_armoredraptor',	0,	'Ravasaure doré',	1,	255696),
(-80357,	'inv_dressedhorse',	0,	'Étalon crin-de-mer',	1,	255695),
(-79790,	'inv_lightforgedelekk_blue',	153044,	'Gangrebroyeur vengeur',	4,	254259),
(-79789,	'inv_lightforgedelekk_amethyst',	153043,	'Gangrebroyeur béni',	4,	254258),
(-79613,	'inv_ammo_bullet_07',	152912,	'Ortie d’eau douce',	4,	253711),
(-79595,	'inv_argusfelstalkermountred',	152905,	'Gueule-écumante cramoisie',	4,	253661),
(-79593,	'inv_argusfelstalkermountgrey',	152904,	'Crache-acide',	4,	253662),
(-79592,	'inv_argusfelstalkermountblue',	152903,	'Grince-dents caustique',	4,	253660),
(-79583,	'inv_mount_arcaneraven',	152901,	'Aile-ensorcelée pourpre',	4,	253639),
(-79487,	'inv_manaraymount_redfel',	152841,	'Raie de mana gangreluisante',	4,	253108),
(-79486,	'inv_manaraymount_orange',	152840,	'Raie de mana scintillante',	4,	253109),
(-79485,	'inv_manaraymount_purple',	152842,	'Raie de mana éclatante',	4,	253106),
(-79484,	'inv_manaraymount_blue',	152844,	'Raie de mana diaprée',	4,	253107),
(-79480,	'inv_felhound3_shadow_mount',	152815,	'Molosse ombreux antoréen',	4,	253087),
(-79479,	'inv_felhound3_shadow_fire',	152816,	'Molosse embrasé antoréen',	4,	253088),
(-79444,	'inv_argustalbukmount_red',	152793,	'Foule-ruines brun-roux',	4,	253006),
(-79443,	'inv_argustalbukmount_purple',	152794,	'Foule-ruines améthyste',	4,	253004),
(-79441,	'inv_argustalbukmount_green',	152795,	'Foule-ruines béryl',	4,	253005),
(-79440,	'inv_argustalbukmount_felred',	153041,	'Foule-ruines sabot-morne',	4,	254260),
(-79438,	'inv_argustalbukmount_brown',	152796,	'Foule-ruines acajou',	4,	253008),
(-79437,	'inv_argustalbukmount_blue',	152797,	'Foule-ruines céruléen',	4,	253007),
(-79436,	'inv_soulhoundmount_blue',	152789,	'Ur’zul entravé',	4,	243651),
(-78105,	'inv_zeppelinmount',	153485,	'Dirigeable de Sombrelune',	4,	247448),
(-78092,	'inv_horse2purple',	151623,	'Cauchemar lucide',	4,	247402),
(-77298,	'inv_hordezeppelinmount',	151617,	'Intercepteur d’Orgrimmar',	4,	245725),
(-77297,	'inv_allianceshipmount',	151618,	'Traque-ciel de Hurlevent',	4,	245723),
(-76646,	'inv_argusfelstalkermount',	152790,	'Vil indomptable',	4,	243652),
(-76586,	'inv_shadowstalkerpanthermount',	147901,	'Cherchétoile lumineux',	4,	243512),
(-76533,	'inv_stormdragonmount2_fel',	153493,	'Dragon des tempêtes du gladiateur démoniaque',	4,	243201),
(29046,	'inv_misc_key_14',	44413,	'Bécane de mekgénieur',	4,	60424);

DROP TABLE IF EXISTS `nombre_de_mounts`;
CREATE TABLE `nombre_de_mounts` (
  `nombre_mounts_id` int(11) NOT NULL,
  `personnages_id` int(11) DEFAULT NULL,
  `mounts_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`nombre_mounts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `personnages`;
CREATE TABLE `personnages` (
  `personnages_id` int(11) NOT NULL,
  `personnages_names` varchar(20) DEFAULT NULL,
  `personnages_classes` varchar(20) DEFAULT NULL,
  `personnages_races` varchar(20) DEFAULT NULL,
  `personnages_niveau` varchar(20) DEFAULT NULL,
  `personnages_inscription` varchar(20) DEFAULT NULL,
  `personnages_montures` int(11) DEFAULT NULL,
  PRIMARY KEY (`personnages_id`),
  KEY `personnages_montures` (`personnages_montures`),
  CONSTRAINT `personnages_ibfk_1` FOREIGN KEY (`personnages_montures`) REFERENCES `mounts` (`mounts_creature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `personnages` (`personnages_id`, `personnages_names`, `personnages_classes`, `personnages_races`, `personnages_niveau`, `personnages_inscription`, `personnages_montures`) VALUES
(1,	'Daryl',	'Paladin',	'Humain',	'60',	'20-02-2019',	NULL),
(2,	'Jaco',	'Moine',	'Orc',	'100',	'15-08-2005',	-79583),
(3,	'Dunbar',	'Prêtre',	'Gnome',	'15',	'17-11-2017',	-76533),
(4,	'Zakyku',	'Druide',	'Gobelin',	'115',	'18-10-2018',	29046),
(5,	'Saperlipopette',	'Prêtre',	'Tauren',	'50',	NULL,	NULL),
(6,	'Zigomar',	'Mage',	'Elfe de sang ',	'10',	NULL,	NULL),
(7,	'Sperpinette',	'Prêtre',	'Troll',	'105',	NULL,	-78092);

DROP TABLE IF EXISTS `races`;
CREATE TABLE `races` (
  `races_id` int(11) NOT NULL,
  `races_name` varchar(20) NOT NULL,
  `races_side` varchar(8) DEFAULT NULL,
  `classes_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`races_id`),
  KEY `classes_id` (`classes_id`),
  CONSTRAINT `races_ibfk_1` FOREIGN KEY (`classes_id`) REFERENCES `classes` (`classes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `races` (`races_id`, `races_name`, `races_side`, `classes_id`) VALUES
(1,	'Humain',	'alliance',	NULL),
(2,	'Orc',	'horde',	NULL),
(3,	'Nain',	'alliance',	NULL),
(4,	'Elfe de la nuit',	'alliance',	NULL),
(5,	'Mort-vivant',	'horde',	NULL),
(6,	'Tauren',	'horde',	NULL),
(7,	'Gnome',	'alliance',	NULL),
(8,	'Troll',	'horde',	NULL),
(9,	'Gobelin',	'horde',	NULL),
(10,	'Elfe de sang',	'horde',	NULL);

-- 2018-10-12 09:23:35
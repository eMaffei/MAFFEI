1.
classes (classes_id,classes_mask,classes_name,classes_power_type,races_id)
clef primaire : classes_id
races (races_id,races_mask,races_name,races_side)
clef primaire : races_id

2.
CREATE TABLE IF NOT EXISTS races (
races_id INT primary key,
races_mask INT NULL,
races_name VARCHAR(20) NULL,
races_side VARCHAR(8) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 
INSERT INTO races VALUES
(1,1,"Humain","alliance"),
(2,2,"Orc","horde"),
(3,4,"Nain","alliance"),
(4,8,"Elfe de la nuit","alliance"),
(5,16,"Mort-vivant","horde"),
(6,32,"Tauren","horde"),
(7,64,"Gnome","alliance"),
(8,128,"Troll","horde"),
(9,256,"Gobelin","horde"),
(10,512,"Elfe de sang","horde");
c est la virgule l erreur il faut un point virgule

3. pour les supprimer il faut faire les alter table
ALTER TABLE classes DROP classes_mask
ALTER TABLE races DROP races_mask

4.
CREATE TABLE IF NOT EXISTS personnages (
personnages_id INT primary key,
personnages_names VARCHAR(20) NULL,
personnages_classes VARCHAR(20) NULL,
personnages_races VARCHAR(20) NULL,
personnages_niveau VARCHAR(20) NULL,
personnages_inscription VARCHAR(20) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

5.cela est pour la creation des personnages
INSERT INTO personnages VALUES
(1,"Daryl","Paladin","Humain",60,"20-02-2019"),
(2,"Jaco","Moine","Orc",100,"15-08-2005"),
(3,"Dunbar","Prêtre","Gnome",15,"17-11-2017"),
(4,"Zakyku","Druide","Gobelin",115,"18-10-2018");

6.
DELETE FROM races WHERE races_name="Humain";
Le problème non visible c est que le personnage daryl ne pourra pas jouer son personnage puisque qu il joue un humain et que l humain est supprimer

7.
ALTER TABLE classes ADD FOREIGN KEY (races_id) REFERENCES races (races_id);

Erreur dans la requête (1452): Cannot add or update a child row: a foreign key constraint fails (`maffee`.`#sql-1de_4cc6d`, CONSTRAINT `#sql-1de_4cc6d_ibfk_1` FOREIGN KEY (`races_id`) REFERENCES `races` (`races_id`))
l erreur est qu on ne peut pas mettre de cle etrangere puisque la race humain est manquante a la classe paladin donc impossible

8.
CREATE TABLE IF NOT EXISTS races (
races_id INT primary key,
races_mask INT NULL,
races_name VARCHAR(20) NULL,
races_side VARCHAR(8) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 
INSERT INTO races VALUES
(1,"Humain","alliance");

9.
CREATE TABLE IF NOT EXISTS races (
races_id INT primary key,
races_mask INT NULL,
races_name VARCHAR(20) NULL,
races_side VARCHAR(8) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 
INSERT INTO races VALUES
(9,256,"Gobelin","horde"),

DELETE FROM races WHERE races_name="Gobelin";
la race gobelin ne peut être supprimer puisque elle est proteger par la clé étrangere

Part2:
1.
INSERT INTO personnages VALUES
(5,"Saperlipopette","Prêtre","Tauren",50,NULL),
(6,"Zigomar","Mage","Elfe de sang ",10,NULL),
(7,"Sperpinette","Prêtre","Troll",105,NULL);

2.
races(races_id,classes_mask,races_name,races_side,classes_id)
clef primaire : races_id

3-
ALTER TABLE races ADD (classes_id INT);
ALTER TABLE races ADD FOREIGN KEY (classes_id) REFERENCES classes (classes_id);
cela permets de mettre une clé etrangere classes dans races 

4-
ALTER TABLE personnages ADD CONSTRAINT FOREIGN KEY (personnages_classes) REFERENCES classes(classes_id);
ALTER TABLE personnages ADD CONSTRAINT FOREIGN KEY (personnages_race) REFERENCES races(races_id);
j ai essaye cela ne marche pas 

Part3:

1-
mounts (mounts_creature_id, mounts_icon, mounts_item_id, mounts_name, mounts_quality_id, mounts_spell_id)
clé primaire : mounts_creature_id

2-
CREATE TABLE mounts (
  mounts_creature_id int(11) NOT NULL,
  mounts_icon varchar(80) DEFAULT NULL,
  mounts_item_id int(11) DEFAULT NULL,
  mounts_name varchar(60) DEFAULT NULL,
  mounts_quality_id int(11) DEFAULT NULL,
  mounts_spell_id int(11) DEFAULT NULL,
  PRIMARY KEY (mounts_creature_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

Part monture-

1-
on est obliger d ajouter une clé primaire pour integrer toutes les montures
ALTER TABLE personnages ADD (personnages_montures INT);
ALTER TABLE personnages ADD FOREIGN KEY (personnages_montures) REFERENCES mounts (mounts_creature_id);

2-
du coup on ajoute les montures
INSERT INTO `mounts` (`mounts_creature_id`, `mounts_icon`, `mounts_item_id`, `mounts_name`, `mounts_quality_id`, `mounts_spell_id`) VALUES
(-81772,	'inv_dogmount',	0,	'Shu Zen, la sentinelle divine',	1,	259395),
(-80358,	'inv_armoredraptor',	0,	'Ravasaure doré',	1,	255696),
(-80357,	'inv_dressedhorse',	0,	'Étalon crin-de-mer',	1,	255695),
(-79790,	'inv_lightforgedelekk_blue',	153044,	'Gangrebroyeur vengeur',	4,	254259),
(-79789,	'inv_lightforgedelekk_amethyst',	153043,	'Gangrebroyeur béni',	4,	254258),
(-79613,	'inv_ammo_bullet_07',	152912,	'Ortie d’eau douce',	4,	253711),
(-79595,	'inv_argusfelstalkermountred',	152905,	'Gueule-écumante cramoisie',	4,	253661),
(-79593,	'inv_argusfelstalkermountgrey',	152904,	'Crache-acide',	4,	253662),
(-79592,	'inv_argusfelstalkermountblue',	152903,	'Grince-dents caustique',	4,	253660),
(-79583,	'inv_mount_arcaneraven',	152901,	'Aile-ensorcelée pourpre',	4,	253639),
(-79487,	'inv_manaraymount_redfel',	152841,	'Raie de mana gangreluisante',	4,	253108),
(-79486,	'inv_manaraymount_orange',	152840,	'Raie de mana scintillante',	4,	253109),
(-79485,	'inv_manaraymount_purple',	152842,	'Raie de mana éclatante',	4,	253106),
(-79484,	'inv_manaraymount_blue',	152844,	'Raie de mana diaprée',	4,	253107),
(-79480,	'inv_felhound3_shadow_mount',	152815,	'Molosse ombreux antoréen',	4,	253087),
(-79479,	'inv_felhound3_shadow_fire',	152816,	'Molosse embrasé antoréen',	4,	253088),
(-79444,	'inv_argustalbukmount_red',	152793,	'Foule-ruines brun-roux',	4,	253006),
(-79443,	'inv_argustalbukmount_purple',	152794,	'Foule-ruines améthyste',	4,	253004),
(-79441,	'inv_argustalbukmount_green',	152795,	'Foule-ruines béryl',	4,	253005),
(-79440,	'inv_argustalbukmount_felred',	153041,	'Foule-ruines sabot-morne',	4,	254260),
(-79438,	'inv_argustalbukmount_brown',	152796,	'Foule-ruines acajou',	4,	253008),
(-79437,	'inv_argustalbukmount_blue',	152797,	'Foule-ruines céruléen',	4,	253007),
(-79436,	'inv_soulhoundmount_blue',	152789,	'Ur’zul entravé',	4,	243651),
(-78105,	'inv_zeppelinmount',	153485,	'Dirigeable de Sombrelune',	4,	247448),
(-78092,	'inv_horse2purple',	151623,	'Cauchemar lucide',	4,	247402),
(-77298,	'inv_hordezeppelinmount',	151617,	'Intercepteur d’Orgrimmar',	4,	245725),
(-77297,	'inv_allianceshipmount',	151618,	'Traque-ciel de Hurlevent',	4,	245723),
(-76646,	'inv_argusfelstalkermount',	152790,	'Vil indomptable',	4,	243652),
(-76586,	'inv_shadowstalkerpanthermount',	147901,	'Cherchétoile lumineux',	4,	243512),
(-76533,	'inv_stormdragonmount2_fel',	153493,	'Dragon des tempêtes du gladiateur démoniaque',	4,	243201);
on peut pas mettre toutes les montures d un coup puisque trop de données
puis on fait une mise a jour pour le personnage et on insert sa monture
INSERT INTO `mounts` (`mounts_creature_id`, `mounts_icon`, `mounts_item_id`, `mounts_name`, `mounts_quality_id`, `mounts_spell_id`) VALUES
(29046,	'inv_misc_key_14',	44413,	'Bécane de mekgénieur',	4,	60424);
UPDATE personnages SET personnages_montures="29046" WHERE personnages_names="Zakyku";


3-
DELETE FROM mounts WHERE mounts_creature_id="29046";
la contrainte est bien verifié puisque on ne peut pas la supprimer 
Erreur dans la requête (1451): Cannot delete or update a parent row: a foreign key constraint fails (`maffee`.`personnages`, CONSTRAINT `personnages_ibfk_1` FOREIGN KEY (`personnages_montures`) REFERENCES `mounts` (`mounts_creature_id`))

les personnages puissent posseder plusieurs montures -

1-
nombre de mounts(nombre_mounts_id,personnages_id,mounts_id)
clef primaire:nombre_mounts_id

2-
CREATE TABLE IF NOT EXISTS nombre_de_mounts (
nombre_mounts_id INT NULL,
personnages_id INT NULL,
mounts_id INT NULL,
PRIMARY KEY (nombre_mounts_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

3-
ALTER TABLE mounts ADD CONSTRAINT FOREIGN KEY (mounts_name) REFERENCES mounts(mounts_creature_id);
ALTER TABLE nombre_de_mounts ADD CONSTRAINT FOREIGN KEY (mounts_id) REFERENCES nombre_de_mouts(nombre_mounts_id);
mais cela ne marche pas

4-
UPDATE personnages SET personnages_montures="-76646" WHERE personnages_names="Daril";
UPDATE personnages SET personnages_montures="-76533" WHERE personnages_names="Dunbar"; 
UPDATE personnages SET personnages_montures="-79583" WHERE personnages_names="Jaco";

5-
UPDATE personnages SET personnages_montures="-76646" OR personnages_montures="-79613" WHERE personnages_names="Saperlipopette";
UPDATE personnages SET personnages_montures="-79441" OR personnages_montures="-79479" WHERE personnages_names="Sperpinette";
mais cela ne marche pas a cause de la clé etrangere


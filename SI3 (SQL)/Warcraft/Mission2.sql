                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       1-
races (races_id,races_mask,races_name,races_side)
clef primaire : races_side

2-
CREATE TABLE IF NOT EXISTS Factions (
Factions_id INT primary key,
Factions_nom VARCHAR (20) 
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

3-INSERT INTO Factions VALUES
(1,"horde"),
(2,"alliance"),
(3,"neutre");

4-ALTER TABLE races ADD Factions_id INT (11);
UPDATE races SET Factions_id = 2 WHERE races_id = 1;
UPDATE races SET Factions_id = 1 WHERE races_id = 2;
UPDATE races SET Factions_id = 2 WHERE races_id = 3;
UPDATE races SET Factions_id = 2 WHERE races_id = 4;
UPDATE races SET Factions_id = 1 WHERE races_id = 5;
UPDATE races SET Factions_id = 1 WHERE races_id = 6;
UPDATE races SET Factions_id = 2 WHERE races_id = 7;
UPDATE races SET Factions_id = 1 WHERE races_id = 8;
UPDATE races SET Factions_id = 1 WHERE races_id = 9;
UPDATE races SET Factions_id = 1 WHERE races_id = 10;

5-ALTER TABLE races ADD FOREIGN KEY (Factions_id) REFERENCES Factions(Factions_id);

6-DELETE FROM Factions WHERE Factions_id="1";

7-INSERT INTO races VALUES
(11,"Draeneï","alliance",NULL,"2"), 
(12,"Worgen","alliance",NULL,"2"),
(14,"Pandaren","neutral",NULL,"3"), 
(15,"Pandaren","alliance",NULL,"2"), 
(16,"Pandaren","horde",NULL,"1"), 
(17,"Sacrenuit","horde",NULL,"1"),
(18,"Tauren de Haut-Roc","horde",NULL,"1"), 
(19,"Elfe du Vide","alliance",NULL,"2"),
(20,"Draeneï sancteforge","alliance",NULL,"2");

8-UPDATE personnages 
SET personnages_niveau = '100'
WHERE ='1'

9-DELETE FROM personnages WHERE personnages_id = 1

10-INSERT INTO classes VALUES
(6,"Chevalier de la mort","runic-power",10),
(12,"Chasseur de démons","fury",9);

part 2

CREATE TABLE `members` (
  `members_character_spec_description` varchar(176) DEFAULT NULL,
  `members_character_name` varchar(17) DEFAULT NULL,
  `members_character_achievement_points` int(11) DEFAULT NULL,
  `members_character_spec_role` varchar(7) DEFAULT NULL,
  `members_character_realm` varchar(5) DEFAULT NULL,
  `members_character_guild_realm` varchar(5) DEFAULT NULL,
  `members_rank` int(11) DEFAULT NULL,
  `members_character_battlegroup` varchar(6) DEFAULT NULL,
  `members_character_spec_order` int(11) DEFAULT NULL,
  `members_character_spec_name` varchar(20) DEFAULT NULL,
  `members_character_gender` int(11) DEFAULT NULL,
  `members_character_class` int(11) DEFAULT NULL,
  `members_character_spec_icon` varchar(34) DEFAULT NULL,
  `members_character_spec_background_image` varchar(22) DEFAULT NULL,
  `members_character_level` int(11) DEFAULT NULL,
  `members_character_guild` varchar(13) DEFAULT NULL,
  `members_character_thumbnail` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `members` (`members_character_race`, `members_character_spec_description`, `members_character_name`, `members_character_achievement_points`, `members_character_spec_role`, `members_character_realm`, `members_character_guild_realm`, `members_rank`, `members_character_battlegroup`, `members_character_spec_order`, `members_character_spec_name`, `members_character_gender`, `members_character_class`, `members_character_spec_icon`, `members_character_spec_background_image`, `members_character_level`, `members_character_guild`, `members_character_thumbnail`) VALUES
(10,	'Invoque la puissance de la Lumière pour protéger et soigner.',	'Bàllôu',	13780,	'HEALING',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Sacré',	1,	2,	'spell_holy_holybolt',	'bg-paladin-holy',	110,	'Kitten Cannon',	'hyjal/120/114312056-avatar.jpg'),
(8,	'Une maîtresse de la magie de l’Ombre qui se spécialise dans les drains et les sorts de dégâts sur la durée.',	'Reactorslock',	16165,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Affliction',	1,	9,	'spell_shadow_deathcoil',	'bg-warlock-affliction',	110,	'Kitten Cannon',	'hyjal/207/114332111-avatar.jpg'),
(10,	'Enflamme ses ennemis à l’aide de boules de feu et du souffle des dragons.',	'Ballöu',	13760,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Feu',	1,	8,	'spell_fire_firebolt02',	'bg-mage-fire',	110,	'Kitten Cannon',	'hyjal/216/114350808-avatar.jpg'),
(10,	'Invoque la puissance de la Lumière pour protéger et soigner.',	'Alynornor',	12270,	'HEALING',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Sacré',	1,	2,	'spell_holy_holybolt',	'bg-paladin-holy',	110,	'Kitten Cannon',	'hyjal/184/114422200-avatar.jpg'),
(10,	'Enflamme ses ennemis à l’aide de boules de feu et du souffle des dragons.',	'Nydra',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Feu',	1,	8,	'spell_fire_firebolt02',	'bg-mage-fire',	110,	'Kitten Cannon',	'hyjal/89/114422617-avatar.jpg'),
(8,	'Une maîtresse de la magie de l’Ombre qui se spécialise dans les drains et les sorts de dégâts sur la durée.',	'Rønak',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Affliction',	0,	9,	'spell_shadow_deathcoil',	'bg-warlock-affliction',	110,	'Kitten Cannon',	'hyjal/131/114422659-avatar.jpg'),
(10,	'Un pratiquant des arts martiaux hors pair qui roue de coups ses ennemis à l’aide de ses mains et de ses poings.',	'Birana',	2750,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	2,	'Marche-vent',	1,	10,	'spell_monk_windwalker_spec',	'bg-monk-battledancer',	110,	'Kitten Cannon',	'hyjal/11/114422795-avatar.jpg'),
(2,	'Un funeste messager de glace, qui canalise sa puissance runique et frappe rapidement avec son arme.',	'Ragnrôk',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Givre',	0,	6,	'spell_deathknight_frostpresence',	'bg-deathknight-frost',	110,	'Kitten Cannon',	'hyjal/206/114423502-avatar.jpg'),
(8,	'Une maîtresse de la magie de l’Ombre qui se spécialise dans les drains et les sorts de dégâts sur la durée.',	'Xiâ',	19945,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Affliction',	1,	9,	'spell_shadow_deathcoil',	'bg-warlock-affliction',	110,	'Kitten Cannon',	'hyjal/220/114424284-avatar.jpg'),
(28,	'Utilise la magie de la Nature pour lancer des sorts de soins sur la durée visant à maintenir ses alliés en vie.',	'Balloou',	13780,	'HEALING',	'Hyjal',	'Hyjal',	5,	'Misery',	3,	'Restauration',	0,	11,	'spell_nature_healingtouch',	'bg-druid-restoration',	110,	'Kitten Cannon',	'hyjal/85/114424661-avatar.jpg'),
(10,	'Une maîtresse endurcie dans l’art du combat avec des armes à deux mains, qui associe mobilité et frappes surpuissantes pour mettre ses adversaires au tapis.',	'Melïndra',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Armes',	1,	1,	'ability_warrior_savageblow',	'bg-warrior-arms',	110,	'Kitten Cannon',	'hyjal/248/114489592-avatar.jpg'),
(8,	'Enflamme ses ennemis à l’aide de boules de feu et du souffle des dragons.',	'Ellînne',	15220,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Feu',	1,	8,	'spell_fire_firebolt02',	'bg-mage-fire',	110,	'Kitten Cannon',	'hyjal/23/114553367-avatar.jpg'),
(2,	'Un maître-archer ou tireur d’élite, qui excelle dans l’exécution de cibles éloignées.',	'Dharaa',	14020,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Précision',	0,	3,	'ability_hunter_focusedaim',	'bg-hunter-marksman',	110,	'Kitten Cannon',	'hyjal/224/114620128-avatar.jpg'),
(10,	'Enflamme ses ennemis à l’aide de boules de feu et du souffle des dragons.',	'Linactors',	16165,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Feu',	1,	8,	'spell_fire_firebolt02',	'bg-mage-fire',	110,	'Kitten Cannon',	'hyjal/51/114744371-avatar.jpg'),
(26,	'Une fugitive impitoyable utilisant agilité et ruse pour affronter ses ennemis.',	'Shenlock',	26030,	'DPS',	'Hyjal',	'Hyjal',	3,	'Misery',	1,	'Hors-la-loi',	0,	4,	'inv_sword_30',	'bg-rogue-combat',	110,	'Kitten Cannon',	'hyjal/179/114880435-avatar.jpg'),
(10,	'Utilise la magie du Sacré pour se protéger et défendre ses alliés.',	'Shyñe',	18125,	'TANK',	'Hyjal',	'Hyjal',	4,	'Misery',	1,	'Protection',	1,	2,	'ability_paladin_shieldofthetemplar',	'bg-paladin-protection',	110,	'Kitten Cannon',	'hyjal/125/114941821-avatar.jpg'),
(2,	'Un maître de la faune, capable de dompter une grande variété de bêtes qui l’assisteront au combat.',	'Arvyre',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Maîtrise des bêtes',	0,	3,	'ability_hunter_bestialdiscipline',	'bg-hunter-beastmaster',	110,	'Kitten Cannon',	'hyjal/3/114967811-avatar.jpg'),
(10,	'Une soigneuse qui maîtrise l’art mystérieux de la manipulation des énergies vitales, avec l’aide de la sagesse du Serpent de jade et des techniques médicales pandarènes.',	'Shynmonk',	18125,	'HEALING',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Tisse-brume',	1,	10,	'spell_monk_mistweaver_spec',	'bg-monk-mistweaver',	110,	'Kitten Cannon',	'hyjal/231/114968551-avatar.jpg'),
(10,	'Un sombre traqueur qui surgit de l’ombre pour tendre une embuscade à sa proie sans méfiance.',	'Reacto
...

2)ALTER TABLE members ADD (members_character INT primary key AUTO_INCREMENT);

3)ALTER TABLE members ADD FOREIGN KEY (members_character_race) REFERENCES races(races_id);

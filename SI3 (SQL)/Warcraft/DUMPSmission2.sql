SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `classes_id` int(22) NOT NULL,
  `classes_name` varchar(20) DEFAULT NULL,
  `classes_power_type` varchar(11) DEFAULT NULL,
  `races_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`classes_id`),
  KEY `races_id` (`races_id`),
  CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`races_id`) REFERENCES `races` (`races_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `classes` (`classes_id`, `classes_name`, `classes_power_type`, `races_id`) VALUES
(1,	'Guerrier',	'rage',	1),
(2,	'Paladin',	'mana',	1),
(3,	'Chasseur',	'focus',	5),
(4,	'Voleur',	'energy',	6),
(5,	'Prêtre',	'mana',	8),
(6,	'Chevalier de la mort',	'runic-power',	10),
(7,	'Chaman',	'mana',	3),
(8,	'Mage',	'mana',	4),
(9,	'Démoniste',	'mana',	5),
(10,	'Moine',	'energy',	2),
(11,	'Druide',	'mana',	9),
(12,	'Chasseur de démons',	'fury',	9)
ON DUPLICATE KEY UPDATE `classes_id` = VALUES(`classes_id`), `classes_name` = VALUES(`classes_name`), `classes_power_type` = VALUES(`classes_power_type`), `races_id` = VALUES(`races_id`);

DROP TABLE IF EXISTS `Factions`;
CREATE TABLE `Factions` (
  `Factions_id` int(11) NOT NULL,
  `Factions_nom` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Factions_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Factions` (`Factions_id`, `Factions_nom`) VALUES
(1,	'horde'),
(2,	'alliance'),
(3,	'neutre')
ON DUPLICATE KEY UPDATE `Factions_id` = VALUES(`Factions_id`), `Factions_nom` = VALUES(`Factions_nom`);

DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `members_character_race` int(11) NOT NULL,
  `members_character_spec_description` varchar(178) DEFAULT NULL,
  `members_character_name` varchar(17) DEFAULT NULL,
  `members_character_achievement_points` int(11) DEFAULT NULL,
  `members_character_spec_role` varchar(7) DEFAULT NULL,
  `members_character_realm` varchar(5) DEFAULT NULL,
  `members_character_guild_realm` varchar(5) DEFAULT NULL,
  `members_rank` int(11) DEFAULT NULL,
  `members_character_battlegroup` varchar(6) DEFAULT NULL,
  `members_character_spec_order` int(11) DEFAULT NULL,
  `members_character_spec_name` varchar(20) DEFAULT NULL,
  `members_character_gender` int(11) DEFAULT NULL,
  `members_character_class` int(11) DEFAULT NULL,
  `members_character_spec_icon` varchar(34) DEFAULT NULL,
  `members_character_spec_background_image` varchar(22) DEFAULT NULL,
  `members_character_level` int(11) DEFAULT NULL,
  `members_character_guild` varchar(13) DEFAULT NULL,
  `members_character_thumbnail` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`members_character_race`),
  CONSTRAINT `members_ibfk_1` FOREIGN KEY (`members_character_race`) REFERENCES `races` (`races_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `members` (`members_character_race`, `members_character_spec_description`, `members_character_name`, `members_character_achievement_points`, `members_character_spec_role`, `members_character_realm`, `members_character_guild_realm`, `members_rank`, `members_character_battlegroup`, `members_character_spec_order`, `members_character_spec_name`, `members_character_gender`, `members_character_class`, `members_character_spec_icon`, `members_character_spec_background_image`, `members_character_level`, `members_character_guild`, `members_character_thumbnail`) VALUES
(2,	'Une maîtresse de la magie de l’Ombre qui se spécialise dans les drains et les sorts de dégâts sur la durée.',	'Reactorslock',	16165,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Affliction',	1,	9,	'spell_shadow_deathcoil',	'bg-warlock-affliction',	110,	'Kitten Cannon',	'hyjal/207/114332111-avatar.jpg'),
(3,	'Enflamme ses ennemis à l’aide de boules de feu et du souffle des dragons.',	'Ballöu',	13760,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Feu',	1,	8,	'spell_fire_firebolt02',	'bg-mage-fire',	110,	'Kitten Cannon',	'hyjal/216/114350808-avatar.jpg'),
(4,	'Invoque la puissance de la Lumière pour protéger et soigner.',	'Alynornor',	12270,	'HEALING',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Sacré',	1,	2,	'spell_holy_holybolt',	'bg-paladin-holy',	110,	'Kitten Cannon',	'hyjal/184/114422200-avatar.jpg'),
(5,	'Enflamme ses ennemis à l’aide de boules de feu et du souffle des dragons.',	'Nydra',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Feu',	1,	8,	'spell_fire_firebolt02',	'bg-mage-fire',	110,	'Kitten Cannon',	'hyjal/89/114422617-avatar.jpg'),
(6,	'Une maîtresse de la magie de l’Ombre qui se spécialise dans les drains et les sorts de dégâts sur la durée.',	'Rønak',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Affliction',	0,	9,	'spell_shadow_deathcoil',	'bg-warlock-affliction',	110,	'Kitten Cannon',	'hyjal/131/114422659-avatar.jpg'),
(7,	'Un pratiquant des arts martiaux hors pair qui roue de coups ses ennemis à l’aide de ses mains et de ses poings.',	'Birana',	2750,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	2,	'Marche-vent',	1,	10,	'spell_monk_windwalker_spec',	'bg-monk-battledancer',	110,	'Kitten Cannon',	'hyjal/11/114422795-avatar.jpg'),
(8,	'Un funeste messager de glace, qui canalise sa puissance runique et frappe rapidement avec son arme.',	'Ragnrôk',	12300,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	1,	'Givre',	0,	6,	'spell_deathknight_frostpresence',	'bg-deathknight-frost',	110,	'Kitten Cannon',	'hyjal/206/114423502-avatar.jpg'),
(9,	'Une maîtresse de la magie de l’Ombre qui se spécialise dans les drains et les sorts de dégâts sur la durée.',	'Xiâ',	19945,	'DPS',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Affliction',	1,	9,	'spell_shadow_deathcoil',	'bg-warlock-affliction',	110,	'Kitten Cannon',	'hyjal/220/114424284-avatar.jpg'),
(10,	'Invoque la puissance de la Lumière pour protéger et soigner.',	'Bàllôu',	13780,	'HEALING',	'Hyjal',	'Hyjal',	5,	'Misery',	0,	'Sacré',	1,	2,	'spell_holy_holybolt',	'bg-paladin-holy',	110,	'Kitten Cannon',	'hyjal/120/114312056-avatar.jpg')
ON DUPLICATE KEY UPDATE `members_character_race` = VALUES(`members_character_race`), `members_character_spec_description` = VALUES(`members_character_spec_description`), `members_character_name` = VALUES(`members_character_name`), `members_character_achievement_points` = VALUES(`members_character_achievement_points`), `members_character_spec_role` = VALUES(`members_character_spec_role`), `members_character_realm` = VALUES(`members_character_realm`), `members_character_guild_realm` = VALUES(`members_character_guild_realm`), `members_rank` = VALUES(`members_rank`), `members_character_battlegroup` = VALUES(`members_character_battlegroup`), `members_character_spec_order` = VALUES(`members_character_spec_order`), `members_character_spec_name` = VALUES(`members_character_spec_name`), `members_character_gender` = VALUES(`members_character_gender`), `members_character_class` = VALUES(`members_character_class`), `members_character_spec_icon` = VALUES(`members_character_spec_icon`), `members_character_spec_background_image` = VALUES(`members_character_spec_background_image`), `members_character_level` = VALUES(`members_character_level`), `members_character_guild` = VALUES(`members_character_guild`), `members_character_thumbnail` = VALUES(`members_character_thumbnail`);

DROP TABLE IF EXISTS `mounts`;
CREATE TABLE `mounts` (
  `mounts_creature_id` int(11) NOT NULL,
  `mounts_icon` varchar(80) DEFAULT NULL,
  `mounts_item_id` int(11) DEFAULT NULL,
  `mounts_name` varchar(60) DEFAULT NULL,
  `mounts_quality_id` int(11) DEFAULT NULL,
  `mounts_spell_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`mounts_creature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mounts` (`mounts_creature_id`, `mounts_icon`, `mounts_item_id`, `mounts_name`, `mounts_quality_id`, `mounts_spell_id`) VALUES
(-81772,	'inv_dogmount',	0,	'Shu Zen, la sentinelle divine',	1,	259395),
(-80358,	'inv_armoredraptor',	0,	'Ravasaure doré',	1,	255696),
(-80357,	'inv_dressedhorse',	0,	'Étalon crin-de-mer',	1,	255695),
(-79790,	'inv_lightforgedelekk_blue',	153044,	'Gangrebroyeur vengeur',	4,	254259),
(-79789,	'inv_lightforgedelekk_amethyst',	153043,	'Gangrebroyeur béni',	4,	254258),
(-79613,	'inv_ammo_bullet_07',	152912,	'Ortie d’eau douce',	4,	253711),
(-79595,	'inv_argusfelstalkermountred',	152905,	'Gueule-écumante cramoisie',	4,	253661),
(-79593,	'inv_argusfelstalkermountgrey',	152904,	'Crache-acide',	4,	253662),
(-79592,	'inv_argusfelstalkermountblue',	152903,	'Grince-dents caustique',	4,	253660),
(-79583,	'inv_mount_arcaneraven',	152901,	'Aile-ensorcelée pourpre',	4,	253639),
(-79487,	'inv_manaraymount_redfel',	152841,	'Raie de mana gangreluisante',	4,	253108),
(-79486,	'inv_manaraymount_orange',	152840,	'Raie de mana scintillante',	4,	253109),
(-79485,	'inv_manaraymount_purple',	152842,	'Raie de mana éclatante',	4,	253106),
(-79484,	'inv_manaraymount_blue',	152844,	'Raie de mana diaprée',	4,	253107),
(-79480,	'inv_felhound3_shadow_mount',	152815,	'Molosse ombreux antoréen',	4,	253087),
(-79479,	'inv_felhound3_shadow_fire',	152816,	'Molosse embrasé antoréen',	4,	253088),
(-79444,	'inv_argustalbukmount_red',	152793,	'Foule-ruines brun-roux',	4,	253006),
(-79443,	'inv_argustalbukmount_purple',	152794,	'Foule-ruines améthyste',	4,	253004),
(-79441,	'inv_argustalbukmount_green',	152795,	'Foule-ruines béryl',	4,	253005),
(-79440,	'inv_argustalbukmount_felred',	153041,	'Foule-ruines sabot-morne',	4,	254260),
(-79438,	'inv_argustalbukmount_brown',	152796,	'Foule-ruines acajou',	4,	253008),
(-79437,	'inv_argustalbukmount_blue',	152797,	'Foule-ruines céruléen',	4,	253007),
(-79436,	'inv_soulhoundmount_blue',	152789,	'Ur’zul entravé',	4,	243651),
(-78105,	'inv_zeppelinmount',	153485,	'Dirigeable de Sombrelune',	4,	247448),
(-78092,	'inv_horse2purple',	151623,	'Cauchemar lucide',	4,	247402),
(-77298,	'inv_hordezeppelinmount',	151617,	'Intercepteur d’Orgrimmar',	4,	245725),
(-77297,	'inv_allianceshipmount',	151618,	'Traque-ciel de Hurlevent',	4,	245723),
(-76646,	'inv_argusfelstalkermount',	152790,	'Vil indomptable',	4,	243652),
(-76586,	'inv_shadowstalkerpanthermount',	147901,	'Cherchétoile lumineux',	4,	243512),
(-76533,	'inv_stormdragonmount2_fel',	153493,	'Dragon des tempêtes du gladiateur démoniaque',	4,	243201),
(29046,	'inv_misc_key_14',	44413,	'Bécane de mekgénieur',	4,	60424)
ON DUPLICATE KEY UPDATE `mounts_creature_id` = VALUES(`mounts_creature_id`), `mounts_icon` = VALUES(`mounts_icon`), `mounts_item_id` = VALUES(`mounts_item_id`), `mounts_name` = VALUES(`mounts_name`), `mounts_quality_id` = VALUES(`mounts_quality_id`), `mounts_spell_id` = VALUES(`mounts_spell_id`);

DROP TABLE IF EXISTS `nombre_de_mounts`;
CREATE TABLE `nombre_de_mounts` (
  `nombre_mounts_id` int(11) NOT NULL,
  `personnages_id` int(11) DEFAULT NULL,
  `mounts_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`nombre_mounts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `personnages`;
CREATE TABLE `personnages` (
  `personnages_id` int(11) NOT NULL,
  `personnages_names` varchar(20) DEFAULT NULL,
  `personnages_classes` varchar(20) DEFAULT NULL,
  `personnages_races` varchar(20) DEFAULT NULL,
  `personnages_niveau` varchar(20) DEFAULT NULL,
  `personnages_inscription` varchar(20) DEFAULT NULL,
  `personnages_montures` int(11) DEFAULT NULL,
  PRIMARY KEY (`personnages_id`),
  KEY `personnages_montures` (`personnages_montures`),
  CONSTRAINT `personnages_ibfk_1` FOREIGN KEY (`personnages_montures`) REFERENCES `mounts` (`mounts_creature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `personnages` (`personnages_id`, `personnages_names`, `personnages_classes`, `personnages_races`, `personnages_niveau`, `personnages_inscription`, `personnages_montures`) VALUES
(2,	'Jaco',	'Moine',	'Orc',	'100',	'15-08-2005',	-79583),
(3,	'Dunbar',	'Prêtre',	'Gnome',	'15',	'17-11-2017',	-76533),
(4,	'Zakyku',	'Druide',	'Gobelin',	'115',	'18-10-2018',	29046),
(5,	'Saperlipopette',	'Prêtre',	'Tauren',	'50',	NULL,	NULL),
(6,	'Zigomar',	'Mage',	'Elfe de sang ',	'10',	NULL,	NULL),
(7,	'Sperpinette',	'Prêtre',	'Troll',	'105',	NULL,	-78092)
ON DUPLICATE KEY UPDATE `personnages_id` = VALUES(`personnages_id`), `personnages_names` = VALUES(`personnages_names`), `personnages_classes` = VALUES(`personnages_classes`), `personnages_races` = VALUES(`personnages_races`), `personnages_niveau` = VALUES(`personnages_niveau`), `personnages_inscription` = VALUES(`personnages_inscription`), `personnages_montures` = VALUES(`personnages_montures`);

DROP TABLE IF EXISTS `races`;
CREATE TABLE `races` (
  `races_id` int(22) NOT NULL,
  `races_name` varchar(30) NOT NULL,
  `races_side` varchar(8) DEFAULT NULL,
  `classes_id` int(22) DEFAULT NULL,
  `Factions_id` int(22) DEFAULT NULL,
  PRIMARY KEY (`races_id`),
  KEY `classes_id` (`classes_id`),
  KEY `Factions_id` (`Factions_id`),
  CONSTRAINT `races_ibfk_1` FOREIGN KEY (`classes_id`) REFERENCES `classes` (`classes_id`),
  CONSTRAINT `races_ibfk_3` FOREIGN KEY (`Factions_id`) REFERENCES `Factions` (`Factions_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `races` (`races_id`, `races_name`, `races_side`, `classes_id`, `Factions_id`) VALUES
(1,	'Humain',	'alliance',	NULL,	2),
(2,	'Orc',	'horde',	NULL,	1),
(3,	'Nain',	'alliance',	NULL,	2),
(4,	'Elfe de la nuit',	'alliance',	NULL,	2),
(5,	'Mort-vivant',	'horde',	NULL,	1),
(6,	'Tauren',	'horde',	NULL,	1),
(7,	'Gnome',	'alliance',	NULL,	2),
(8,	'Troll',	'horde',	NULL,	1),
(9,	'Gobelin',	'horde',	NULL,	1),
(10,	'Elfe de sang',	'horde',	NULL,	1),
(11,	'Draeneï',	'alliance',	NULL,	2),
(12,	'Worgen',	'alliance',	NULL,	2),
(14,	'Pandaren',	'neutral',	NULL,	3),
(15,	'Pandaren',	'alliance',	NULL,	2),
(16,	'Pandaren',	'horde',	NULL,	1),
(17,	'Sacrenuit',	'horde',	NULL,	1),
(18,	'Tauren de Haut-Roc',	'horde',	NULL,	1),
(19,	'Elfe du Vide',	'alliance',	NULL,	2),
(20,	'Draeneï sancteforge',	'alliance',	NULL,	2)
ON DUPLICATE KEY UPDATE `races_id` = VALUES(`races_id`), `races_name` = VALUES(`races_name`), `races_side` = VALUES(`races_side`), `classes_id` = VALUES(`classes_id`), `Factions_id` = VALUES(`Factions_id`);

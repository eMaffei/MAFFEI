<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Inscription</title>
    </head>
    <body>

      <form action="RecupInscri.php" style="margin: 5%" method="post">
        <div class="form-group">
          <label for="login">Login</label>
          <input class="form-control" type="text" name="login" required>
        </div

        <div class="form-group">
          <label for="">mdp</label>
          <input class="form-control" type="password" name="password" required>
        </div>

      <div class="form-group">
        <label for="">Email</label>
        <input class="form-control" type="email" name="email" pattern="*@*" required>
      </div>
      <center><button type="submit" class="btn btn-success">S'inscrire</button></center>
    </form>
    </body>
    </html>

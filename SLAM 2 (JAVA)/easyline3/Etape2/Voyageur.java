package classes;

import java.util.Scanner;

public class Voyageur {
	protected int age ;
	protected String nom ;
	protected String categorie ;
	protected AdressePostale adresse ; // ca sert a relier en gros adressepostale devient un objet
	protected Bagage mesbagages ; // same 

	//constructeur vide
	public Voyageur() {

	}


	public Voyageur(int age, String nom) {

		this.age=age;
		this.nom=nom;
	}

	// GET /////////////////////////////////////

	// renvoie la donnée age "return"
	public int getage() {
		return this.age;
	}

	// renvoie la donnée nom "return"
	public String getnom() { //get = obtenir la donnée
		return this.nom ;
	}

	// renvoie la donnée categorie "return"
	public String getcategorie() {
		return this.categorie;
	}

	// renvoie la donnée adresse "return"
	private AdressePostale getAdresse() {
		return adresse;
	}

	// SET //////////////////////////////////////////


	//si age superieur a 0
	//	alors age = age 
	//sinon
	//	afficher erreur sur l'age  
	public void setage(int age) { //modifie age pour que la valeur ne soit pas en dessous de 0
		if(age>0) {
			this.age=age; // this ca sert a indiquer que t es dans ta fonction
		}else {
			System.out.println("erreur sur l'age");//sinon il affiche erreur sur l age

		}
		//this.setcategorie(); //pour changer la categorie en fonction de l'age

	}


	public void setnom(String n) {
		nom=n; // n pour avoir une autre variable nom
	}

	// toString transformer toutes les valeurs dans un seul string
	public String ToString() {
		return this.getnom()+" "+getage() ;
	}

	//si age <= 1
	//  alors vous appartenez a la classe nourrison
	//sinon age <18
	//  alors vous appartenez a la classe enfant
	//...
	public void setcategorie() {
		if(this.age<=1) {
			this.categorie="nourrisson";
			System.out.println("vous êtes un nourrisson"+ this.age);
		}
		else if(this.age<18) {
			this.categorie="enfant";
			System.out.println("vous êtes un enfant");
		}
		else if(this.age<60) {
			this.categorie="adulte";
			System.out.println("vous êtes un adulte");
		}
		else if(this.age>60) {
			this.categorie="senior";
			System.out.println("vous êtes un senior");
		}
	}

	//adresse correspond a la valeur adressepostale adresse
	private void setAdresse(AdressePostale adresse) {
		this.adresse = adresse;
	}

	//de meme avec mes bagages
	private void setMesbagages(Bagage mesbagages) {
		this.mesbagages = mesbagages;
	}

	//METHODE AFFICHER ////////////////////


	//cela verifie la validité du nom en bref si le nom possède moins de 2 lettre celui ci
	//ne sera pas accepté
	// si celui ci est valide ca affichera les gets du nom, age et categorie
	//sinon "vous n'etes pas humain"
	public void affichage()
	{
		if(nom != null && age > 0)
		{
			if(nom.length() >= 2)
			{
				System.out.println("Vous etes " + getnom() + " et vous avez " + getage() + " ans, vous etes un " + getcategorie());
			}
		}
		else
		{
			System.out.println("Vous n'etes pas humain !");
		}
	}
}

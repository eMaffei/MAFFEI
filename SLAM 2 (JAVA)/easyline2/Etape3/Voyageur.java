package classes;

import java.util.Scanner;

public class Voyageur {
	private int age ;
	private String nom ;
	private String categorie ;
	AdressePostale adresse ; // generer un get et un set a faire chez soi
	Bagage mesbagages ;

	public Voyageur() {}

	public Voyageur(int age, String nom) {

		this.age=age;
		this.nom=nom;
	}

	public int getage() {
		return this.age;
	}

	public String getnom() { //get = obtenir la donnée
		return this.nom ;
	}

	public String getcategorie() {
		return this.categorie;
	}

	public void setage(int age) { //modifie age pour que la valeur ne soit pas en dessous de 0
		if(age>0) {
			this.age=age; // this ca sert a indiquer que t es dans ta fonction
		}else {
			System.out.println("erreur sur l'age");//sinon il affiche erreur sur l age

		}
		this.setcategorie(); //pour changer la categorie en fonction de l'age

	}

	public void setnom(String n) {
		nom=n; // n pour avoir une autre variable nom
	}

	public void setcategorie() {
		if(this.age<=1) {
			this.categorie="nourrisson";
			System.out.println("vous êtes un nourrisson"+ this.age);
		}
		else if(this.age<18) {
			this.categorie="enfant";
			System.out.println("vous êtes un enfant");
		}
		else if(this.age<60) {
			this.categorie="adulte";
			System.out.println("vous êtes un adulte");
		}
		else if(this.age>60) {
			this.categorie="senior";
			System.out.println("vous êtes un senior");
		}
	}

	private AdressePostale getAdresse() {
		return adresse;
	}

	private void setAdresse(AdressePostale adresse) {
		this.adresse = adresse;
	}


	private void setMesbagages(Bagage mesbagages) {
		this.mesbagages = mesbagages;
	}

	public void affichage()
	{
		if(nom != null && age > 0)
		{
			if(nom.length() >= 2)
			{
				System.out.println("Vous etes " + getnom() + " et vous avez " + getage() + " ans, vous etes un " + getcategorie());
			}
		}
		else
		{
			System.out.println("Vous n'etes pas humain !");
		}
	}
}

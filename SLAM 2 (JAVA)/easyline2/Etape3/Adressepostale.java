package classes;

import java.util.Scanner;

public class AdressePostale {
	private int libelle ;
	private String ville;
	private String codepostale;

	public AdressePostale(int libelle, String adresse, String ville){
		this.libelle=libelle;
		this.codepostale=adresse;
		this.ville=ville;
	}

	public String getcodepostale() {
		return this.codepostale;
	}
	public int getlibelle() {
		return this.libelle;
	}
	public String getville() {
		return this.ville;
	}
	public void setcodepostal(String codepostal) {
		this.codepostale=codepostal;
	}

	public void setlibelle(int libelle) {
		this.libelle=libelle;

	}
	public void setville(String ville){
		this.ville=ville;

	}
	public void afficher(){
		System.out.println( this.getlibelle()+this.getville()+this.getcodepostale());
	}
}

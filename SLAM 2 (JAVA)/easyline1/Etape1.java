a)

package classes;

import java.util.Scanner;

public class Voyageur {
public String nom ; 
public int age ; // public sert a la voir partout
public Voyageur(String nomvoyageur,int agevoyageur) {
	this.nom = nomvoyageur ; //this permet de se reservir de la variable
	this.age = agevoyageur ; // on nous permet de prendre la variable agevoyageur
}
}
--------------------------------------------------------------------------
package classes;
import java.util.Scanner;
public class Classes2 {
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		Voyageur voyageur1 =  new Voyageur("italie", 123);
		System.out.println(voyageur1.nom);//affiche italie
		voyageur1.nom = "bonjour";
		System.out.println(voyageur1.nom);//affiche bonjour
		                
		Voyageur voyageur2 = new Voyageur("italie",15);
		System.out.println("Veuillez saisir votre age voyageur");
		voyageur2.age = sc.nextInt();
		System.out.println(voyageur2.age);//affiche la valeur saisie par l'utilisateur
	}
}

b)
package classes;

import java.util.Scanner;

public class Voyageur {
public int age ;
public String nom ;
public Voyageur() { // on mets par default le constructeur du coup on ne mets rien comme variable dans le constructeur
}
}
-----------------------------------------------------------------------
package classes;
import java.util.Scanner;
public class Classes2 {
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		Voyageur voyageur1 =  new Voyageur();
		System.out.println(voyageur1.nom);
		voyageur1.nom = "bonjour";
		System.out.println(voyageur1.nom);
		                
		Voyageur voyageur2 = new Voyageur();
		System.out.println("Veuillez saisir votre age voyageur");
		voyageur2.age = sc.nextInt();
		System.out.println(voyageur2.age);
	}
}

avec if ------------------------------------------------ pour age 0

package classes;
import java.util.Scanner;
public class Classes2 {
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		Voyageur voyageur1 =  new Voyageur();
		voyageur1.setage(-5);// modifier = set en gros cela modifie la valeur a -5
		String J = 2 ;
		if (J < voyageur1.nom) {
		System.out.println(voyageur1.nom);
		voyageur1.nom = "bonjour";
		System.out.println(voyageur1.nom);
		}
		Voyageur voyageur2 = new Voyageur();
		if (0 < voyageur2.age) {
			System.out.println("Veuillez saisir votre age voyageur");
			voyageur2.age = sc.nextInt();
			System.out.println(voyageur2.age);
	}
}
}

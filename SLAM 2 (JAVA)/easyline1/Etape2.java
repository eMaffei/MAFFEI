--------constructeur------------

package classes;

import java.util.Scanner;

public class Voyageur {
private int age ;
private String nom ;

public Voyageur(int age, String nom) {}

public Voyageur(int age, String nom) {
	
	this.age=age;
	this.nom=nom;
}

public int getage() {
	return this.age;
}

public String getnom() { //get = obtenir la donnée
	return this.nom ;
}

public void setage(int age) { //modifie age pour que la valeur ne soit pas en dessous de 0
	if(age>0) {
		this.age=age; // this ca sert a indiquer que t es dans ta fonction
	}else {
		System.out.println("erreur sur l'age");//sinon il affiche erreur sur l age
	}
}

public void setnom(String n) {
	nom=n; // n pour avoir une autre variable nom
}

public void afficher() {//peut aussi mettre une fonction afficher mais alors mettre tout en public cette fonction ne sert pas
//si tout est en privéé
    System.out.println(this.getnom() +" à "+ this.getage() +"ans de categorie" +this.getcategorie())
    
}
}


-------------main-------------

package classes;
import java.util.Scanner;
public class Classes2 {
	public static void main(String[] args){
		
		Scanner sc= new Scanner(System.in);
		
		Voyageur voyageur1 =  new Voyageur();//appel construcetur 1
		Voyageur voyageur1 = new Voyageur();//appel constructeur 2
		//avec la methode afficher c est si dessous
		voyageur1.afficher();// cela affichera ce qu il y a dans la methode afficher
		voyageur1.setage(20);//on modifie la valeur du setage
		voyageur1.afficher();// cela va donc changer la valeur du set age et va afficher 20 et donc afficher adulte au mieux de l anncienne donnéé
		//avec la methode sans le afficher 
		voyageur1.setnom("skate");
		voyageur1.setage(-5);// modifier = set en gros cela modifie la valeur a -5
	
		System.out.println(voyageur1.getage());
		System.out.println(voyageur1.getnom());
		}
}

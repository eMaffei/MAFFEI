-----------constructeur----------
package classes;

import java.util.Scanner;

public class Voyageur {
private int age ;
private String nom ;
private String categorie ;

public Voyageur() {}

public Voyageur(int age, String nom) {
	
	this.age=age;
	this.nom=nom;
}

public int getage() {
	return this.age;
}

public String getnom() { //get = obtenir la donnée
	return this.nom ;
}

public String getcategorie() {
	return this.categorie;
}

public void setage(int age) { //modifie age pour que la valeur ne soit pas en dessous de 0
	if(age>0) {
		this.age=age; // this ca sert a indiquer que t es dans ta fonction
	}else {
		System.out.println("erreur sur l'age");//sinon il affiche erreur sur l age
	}
}

public void setnom(String n) {
	nom=n; // n pour avoir une autre variable nom
}

public void setcategorie() {
	if(this.age<=1) {
	    this.categorie="nourrisson";
		System.out.println("vous êtes un nourrisson"+ this.age);
	}
	else if(this.age<18) {
	    this.categorie="enfant";
		System.out.println("vous êtes un enfant");
	}
	else if(this.age<60) {
	    this.categorie="adulte";
		System.out.println("vous êtes un adulte");
	}
	else if(this.age>60) {
	    this.categorie="senior";
		System.out.println("vous êtes un senior");
	}
}
}


--------------main------------------

package classes;
import java.util.Scanner;
public class Classes2 {
	public static void main(String[] args){
		
		Scanner sc= new Scanner(System.in);
		
		Voyageur voyageur1 = new Voyageur();//appel constructeur 1
		System.out.println("Saissisez votre age");
		int age = sc.nextInt();
		
		
		voyageur1.setnom("skate");
		voyageur1.setage(age);//veux mettre un scanner);// modifier = set en gros cela modifie la valeur a -5
		voyageur1.setcategorie();
		
	    System.out.println(voyageur1.getnom());
		System.out.println(voyageur1.getage());
		System.out.println(voyageur1.getcategorie());
		}
}





